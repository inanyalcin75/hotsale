﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace HotSale.Infrastructure.Extension
{
    public static class TypeExtension
    {

        public static T To<T>(this object input)
        {
            try
            {

                var t = typeof(T);

                if (t.IsGenericType && t.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
                {
                    if (input == null)
                    {
                        return default(T);
                    }

                    t = Nullable.GetUnderlyingType(t);
                }

                return (T)Convert.ChangeType(input, t, CultureInfo.InvariantCulture);
            }
            catch (Exception e)
            {
                throw new Exception("Variable is not converting");
            }
        }
    }
}
