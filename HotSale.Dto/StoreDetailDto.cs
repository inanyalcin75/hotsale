﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HotSale.Dto
{
    public class StoreDetailDto
    {
        public int StoreId { get; set; }
        public int FisNo { get; set; }
        public string Stok_Kodu { get; set; }
        public string Adi_1 { get; set; }
        public decimal Miktari { get; set; }
        public decimal Tutari { get; set; }
    }
}
