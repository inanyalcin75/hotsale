﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HotSale.Dto
{
    public class ProductGroupDto
    {
        public string Id { get; set; }

        public string Name { get; set; }
    }
}
