﻿using HotSale.Infrastructure.Extension;
using System;
using System.Collections.Generic;
using System.Text;

namespace HotSale.Dto.Helper
{
    public class ServiceResponse<T>
    {
        public bool Status { get; set; }
        public string Message { get; set; }
        public string Term { get; set; }
        public int DataTotalCount { get; set; }
        public Pager Page { get; set; }
        public T Data { get; set; }
    }
}
