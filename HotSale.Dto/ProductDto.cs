﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HotSale.Dto
{
    public class ProductDto
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Unit { get; set; }
        public decimal UnitRate { get; set; }
        public decimal Price { get; set; }
        public decimal Tax { get; set; }

    }
}
