﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HotSale.Dto
{
    public class CompanyDto
    {
        public int? Id { get; set; }
        public string Name { get; set; }
    }
}
