﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HotSale.Dto
{
    public class OrderDto
    {
        public int Id { get; set; }
        public int FisNo { get; set; }
        public string Unvani { get; set; }
        public decimal Tutari { get; set; }
    }
}
