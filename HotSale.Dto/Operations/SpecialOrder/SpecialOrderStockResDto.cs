﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HotSale.Dto.Operations.SpecialOrder
{
    public class SpecialOrderStockResDto
    {
        public string StockCode { get; set; }
        public string StockName { get; set; }
    }
}
