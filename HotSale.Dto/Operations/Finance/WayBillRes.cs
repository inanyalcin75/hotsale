﻿using HotSale.Dto.Helper;
using System;
using System.Collections.Generic;
using System.Text;

namespace HotSale.Dto.Operations.Finance
{
    public class WayBillRes
    {
        public DateTime StartDate { get; set; }
        public DateTime FinishDate { get; set; }
        public string Term { get; set; }
        public decimal Total { get; set; }
        public List<WayBillDto> WayBillDtos { get; set; }
        public List<WayBillDetailDto> WayBillDetailsDto { get; set; }
    }

    public class WayBillDto
    {
        public int Id { get; set; }
        public string IsyeriKodu { get; set; }
        public int FisNo { get; set; }
        public DateTime FisTarihi { get; set; }
        public int SipNo { get; set; }
        public string Belge_No { get; set; }
    }


    public class WayBillDetailDto
    {
        public int FisNo { get; set; }
        public int FisSira { get; set; }
        public string IsyeriKodu { get; set; }
        public string Stok_Kodu { get; set; }
        public string Stok_Adi { get; set; }
        public string BirimTipi { get; set; }
        public decimal Miktari { get; set; }
        public decimal Fiyat { get; set; }
        public decimal Tutari { get; set; }
    }
}
