﻿using HotSale.Dto.Helper;
using System;
using System.Collections.Generic;
using System.Text;

namespace HotSale.Dto.Operations.Finance
{

    public class CurrentAccountRes
    {
        public DateTime StartDate { get; set; }
        public DateTime FinishDate { get; set; }
        public Decimal TotalBorcTutari { get; set; }
        public Decimal TotalAlckTutari { get; set; }
        public ServiceResponse<List<CurrentAccountDto>> CurrentAccountsDto { get; set; }
    }

    public class CurrentAccountDto
    {
        public int FisSira { get; set; }
        public string FisTipi { get; set; }
        public int IsyeriKodu { get; set; }
        public int FisNo { get; set; }
        public DateTime FisTarihi { get; set; }
        public string BelgeNo { get; set; }
        public decimal BorcTutari { get; set; }
        public decimal AlckTutari { get; set; }
    }
}
