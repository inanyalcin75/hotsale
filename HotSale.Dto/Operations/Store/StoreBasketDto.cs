﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HotSale.Dto.Operations.Store
{
    public class StoreBasketDto
    {
        public string UserId { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public decimal Quantity { get; set; }
        public decimal Price { get; set; }
        public decimal Tax { get; set; }
        public string ProductCode { get; set; }
        public string ProductUnit { get; set; }
        public decimal ProductUnitRate { get; set; }
    }
}
