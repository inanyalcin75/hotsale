﻿using HotSale.Dto.Helper;
using System;
using System.Collections.Generic;
using System.Text;

namespace HotSale.Dto.Operations.Store
{
    public class StoreRes
    {
        public string Term { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime FinishDate { get; set; }
        public ServiceResponse<List<StoreDto>> StoreDto { get; set; }
        public List<StoreDetailDto> StoreDetailsDto { get; set; }
    }
}
