﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HotSale.Dto.Operations
{
    public class DashboardRes
    {
        public int? CompanyId { get; set; }
        public int? CompanySectionsId { get; set; }
        public string CompanyTypeId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime FinishDate { get; set; }

        public List<CompanyDto> CompanyDto { get; set; }
        public List<CompanySectionDto> CompanySectionsDto { get; set; }
        public List<CompanyTypeDto> CompanyTypeDto { get; set; }
        public List<ProductsSaleReportDto> ProductsSaleReportDto { get; set; }
        public List<ProductGroupsReportDto> ProductGroupsReportDto { get; set; }
        public AccountReportDto AccountReportDto { get; set; }
    }


    public class CompanySectionDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class CompanyTypeDto
    {
        public string  Id { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
    }


    public class ProductsSaleReportDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Count { get; set; }
        public decimal Total { get; set; }
    }


    public class ProductGroupsReportDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Count { get; set; }
    }

    public class AccountReportDto
    {
        public string Assignor { get; set; } //Devreden Bakiye
        public List<AccountReportDetailDto> AccountReportDetailsDto { get; set; }
    }



    public class AccountReportDetailDto
    {
        public int Id { get; set; }
        public string ProcessName { get; set; }
        public string ProcessType { get; set; }
        public decimal Total { get; set; }
        public DateTime ProcessDate { get; set; }
    }

}
