﻿using HotSale.Dto.Helper;
using System;
using System.Collections.Generic;
using System.Text;

namespace HotSale.Dto.Operations.Order
{
    public class ProductRes
    {
        public string ProductGroupId { get; set; }
        public string Term { get; set; }
        public int PageNo { get; set; }
        public List<ProductGroupDto> ProductGroupsDto { get; set; }
        public ServiceResponse<List<ProductDto>> ProductsDto { get; set; }
    }
}
