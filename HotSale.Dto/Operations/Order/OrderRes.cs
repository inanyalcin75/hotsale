﻿using HotSale.Dto.Helper;
using System;
using System.Collections.Generic;
using System.Text;

namespace HotSale.Dto.Operations.Order
{
    public class OrderRes
    {
        public DateTime StartDate { get; set; }
        public DateTime FinishDate { get; set; }
        public string Term { get; set; }
        public ServiceResponse<List<OrderDto>> OrdersDto { get; set; }
        public List<OrderDetailDto> OrderDetailsDto { get; set; }
    }
}
