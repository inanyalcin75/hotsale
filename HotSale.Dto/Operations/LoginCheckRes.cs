﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HotSale.Dto.Operations
{
    public class LoginCheckRes
    {
        public string UserId { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }

        //Sefa ekletti
        public string FirmCode { get; set; }
        //Cari No
        public int FirmId { get; set; }
        public int FirmAddressId { get; set; }
        //İşyeri Kodu
        public int BranchId { get; set; }
        public string KeysoftUserId { get; set; }
        //Fotoğraf yükleme yetkisi
        public bool IsPhotoAccess { get; set; }
        //Müşteri girişi yetkisi
        public bool IsCustomer { get; set; }
        //Firma Merkez kullanıcısı mı
        public bool IsOtherBranch { get; set; }
    }
}
