﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HotSale.Dto
{
    //Cari Kartı
    public class AccountCardDto
    {
        //CariNo
        public int AccountCardNo { get; set; }
        //CariKodu
        public string AccountCardCode { get; set; }
        //Unvani
        public string AccountCardTitle { get; set; }
    }
}
