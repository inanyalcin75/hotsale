﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HotSale.Dto
{
    public class StoreDto
    {
        public int Id { get; set; }
        public int FisNo { get; set; }
        public string DepoAdi { get; set; }
    }
}
