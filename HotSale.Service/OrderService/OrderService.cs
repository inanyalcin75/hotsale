﻿using System;
using System.Collections.Generic;
using System.Text;
using HotSale.Dto;
using HotSale.Dto.Helper;
using System.Linq;
using HotSale.Dto.Operations.Order;
using System.Data.SqlClient;
using HotSale.Service.SqlDataService;
using HotSale.Dto.Operations;
using HotSale.Infrastructure.Extension;

namespace HotSale.Service.OrderService
{
    public class OrderService : IOrderService
    {
        private readonly ISqlDataService _sqlDataService;

        public OrderService(ISqlDataService sqlDataService)
        {
            _sqlDataService = sqlDataService;
        }

        public List<OrderDetailDto> GetOrderDetailList(int[] orderIds)
        {
            var orderDetailList = new List<OrderDetailDto>();
            var listParamater = new List<SqlParameter>();

            var sqlParam = string.Join(",", orderIds);


            var reader = _sqlDataService.GetDataList($@"SELECT SFK.Fis_No, SK.Stok_Kodu, SK.Adi_1 AS Stok_Adi, SFK.Miktari, SFK.Tutari
                                                       FROM SIPR_Fis_Kalem AS SFK
                                                       INNER JOIN STOK_Karti AS SK ON SK.Stok_No = SFK.Stok_No
                                                       WHERE SFK.Fis_Durum = 0 AND SFK.Fis_Tipi = 36 
                                                             AND SFK.Fis_No IN ({sqlParam})", listParamater);

            while (reader.Read())
            {
                orderDetailList.Add(new OrderDetailDto { FisNo = reader["Fis_No"].To<int>(), Stok_Kodu = reader["Stok_Kodu"].ToString(), Adi_1 = reader["Stok_Adi"].ToString(), Miktari = reader["Miktari"].To<decimal>(), Tutari = reader["Tutari"].To<decimal>() });
            }

            reader.Close();


            //orderDetailList.Add(new OrderDetailDto { ProductId = 15, ProductName = "Çilek", OrderId = 1, Count = 30 });
            //orderDetailList.Add(new OrderDetailDto { ProductId = 15, ProductName = "Armut", OrderId = 1, Count = 5 });
            //orderDetailList.Add(new OrderDetailDto { ProductId = 15, ProductName = "Kavun", OrderId = 1, Count = 32 });
            //orderDetailList.Add(new OrderDetailDto { ProductId = 15, ProductName = "Ekmek", OrderId = 1, Count = 18 });
            return orderDetailList;
        }

        public ServiceResponse<List<OrderDto>> GetOrderList(int companyId, int firmId, bool isCustomer, string term, int pageNo, int listCount, DateTime startDate, DateTime finishDate)
        {

            var counter = listCount * (pageNo - 1);
            var totalcount = 0;

            var orderList = new List<OrderDto>();

            var listParamater = new List<SqlParameter>();
            listParamater.Add(new SqlParameter { ParameterName = "@PageNo", Value = pageNo });
            listParamater.Add(new SqlParameter { ParameterName = "@ListCount", Value = listCount });
            listParamater.Add(new SqlParameter() { ParameterName = "@StardDate", Value = startDate });
            listParamater.Add(new SqlParameter() { ParameterName = "@FinishDate", Value = finishDate });
            listParamater.Add(new SqlParameter() { ParameterName = "@Isyeri_Kodu", Value = companyId });

            if (isCustomer)
                listParamater.Add(new SqlParameter() { ParameterName = "@Cari_No", Value = firmId });

            if (!string.IsNullOrEmpty(term))
                listParamater.Add(new SqlParameter() { ParameterName = "@Term", Value = "%" + term + "%" });

            var reader = _sqlDataService.GetDataList($@"Select * From (SELECT Top (@PageNo * @ListCount) row_number() OVER(ORDER BY B.Fis_No DESC) As RowNew,* FROM (SELECT SFB.Fis_No, CK.Unvani, SFB.NetTutar1 --Duruma göre alanları eklersin
                                                       FROM SIPR_Fis_Baslik AS SFB
                                                       INNER JOIN CARI_Karti AS CK ON SFB.Cari_No = CK.Cari_No AND CK.IptDrm = 0
                                                       WHERE SFB.Fis_Durum = 0 AND SFB.Fis_Tipi = 36 
                                                             AND SFB.Isyeri_Kodu = @Isyeri_Kodu  
                                                           { (!isCustomer ? " " : " AND SFB.Cari_No = @Cari_No ") }
                                                             AND SFB.Fis_Tarihi >= @StardDate AND SFB.Fis_Tarihi <= @FinishDate
                                                             { (string.IsNullOrEmpty(term) ? "" : "AND CK.Unvani Like @Term") }
                                                    ) As B) As A Where A.RowNew BETWEEN ((@PageNo-1)*@ListCount)+1 AND (@PageNo * @ListCount)", listParamater);
            var ider = 1;

            while (reader.Read())
            {
                orderList.Add(new OrderDto { Id = ider++, FisNo = reader["Fis_No"].To<int>(), Unvani = reader["Unvani"].ToString(), Tutari = reader["NetTutar1"].To<decimal>() });
            }

            reader.Close();

            listParamater = new List<SqlParameter>();
            listParamater.Add(new SqlParameter { ParameterName = "@PageNo", Value = pageNo });
            listParamater.Add(new SqlParameter { ParameterName = "@ListCount", Value = listCount });
            listParamater.Add(new SqlParameter() { ParameterName = "@StardDate", Value = startDate });
            listParamater.Add(new SqlParameter() { ParameterName = "@FinishDate", Value = finishDate });
            listParamater.Add(new SqlParameter() { ParameterName = "@Isyeri_Kodu", Value = companyId });

            if (isCustomer)
                listParamater.Add(new SqlParameter() { ParameterName = "@Cari_No", Value = firmId });

            if (!string.IsNullOrEmpty(term))
                listParamater.Add(new SqlParameter() { ParameterName = "@Term", Value = "%" + term + "%" });

            reader = _sqlDataService.GetDataList($@"SELECT Count(0) As Count
                                                   FROM SIPR_Fis_Baslik AS SFB
                                                   INNER JOIN CARI_Karti AS CK ON SFB.Cari_No = CK.Cari_No AND CK.IptDrm = 0
                                                   WHERE SFB.Fis_Durum = 0 AND SFB.Fis_Tipi = 36 
                                                             AND SFB.Isyeri_Kodu = @Isyeri_Kodu 
                                                           { (!isCustomer ? " " : " AND SFB.Cari_No = @Cari_No ") }
                                                             { (string.IsNullOrEmpty(term) ? "" : "AND CK.Unvani Like @Term ") }
                                                             AND SFB.Fis_Tarihi >= @StardDate AND SFB.Fis_Tarihi <= @FinishDate", listParamater);

            while (reader.Read())
            { totalcount = reader["Count"].To<int>(); }

            reader.Close();

            return new ServiceResponse<List<OrderDto>> { Data = orderList, DataTotalCount = totalcount, Status = true };
        }

        public bool SaveOrder(int storeCode, int? accountNo, List<BasketDto> baskets, LoginCheckRes loginCheckRes)
        {

            var listParamCount = new List<SqlParameter>();


            int newFisNo = 1;
            var reader = _sqlDataService.GetDataList("SELECT ISNULL(MAX(Fis_No), CAST(0 AS INT)) + CAST(1 AS INT) AS NewFisNo FROM SIPR_Fis_Baslik WHERE Fis_Tipi = 36 AND Isyeri_Kodu = " + loginCheckRes.BranchId, listParamCount);
            if (reader.Read())
                newFisNo = reader["NewFisNo"].To<int>();
            reader.Close();

            listParamCount.Add(new SqlParameter { ParameterName = "@Fis_Tipi", Value = 36 });
            listParamCount.Add(new SqlParameter { ParameterName = "@Isyeri_Kodu", Value = loginCheckRes.BranchId });
            listParamCount.Add(new SqlParameter { ParameterName = "@Fis_No", Value = newFisNo });
            listParamCount.Add(new SqlParameter { ParameterName = "@Fis_Tarihi", Value = DateTime.Now });
            listParamCount.Add(new SqlParameter { ParameterName = "@Depo_Kodu", Value = 1 }); //Değişebilir
            listParamCount.Add(new SqlParameter { ParameterName = "@Cari_No", Value = accountNo ?? loginCheckRes.FirmId });
            listParamCount.Add(new SqlParameter { ParameterName = "@Belge_Tarihi", Value = DateTime.Now.Date });
            listParamCount.Add(new SqlParameter { ParameterName = "@Belge_Tipi", Value = 36 });
            listParamCount.Add(new SqlParameter { ParameterName = "@Belge_FisNo", Value = newFisNo });
            listParamCount.Add(new SqlParameter { ParameterName = "@Teslim_Tarihi", Value = DateTime.Now });
            listParamCount.Add(new SqlParameter { ParameterName = "@Kdv_Tipi1", Value = 1 });
            listParamCount.Add(new SqlParameter { ParameterName = "@Kdv_Tipi2", Value = 1 });
            listParamCount.Add(new SqlParameter { ParameterName = "@Para_Birimi", Value = "TL" });
            listParamCount.Add(new SqlParameter { ParameterName = "@Fiyat_Tipi", Value = "SFYT" }); //Değişebilir
            listParamCount.Add(new SqlParameter { ParameterName = "@Kur_Degeri", Value = 1 });
            listParamCount.Add(new SqlParameter { ParameterName = "@TopDeger1", Value = baskets.Sum(p => (p.Quantity * p.Price) / (1 + (p.Tax / 100))) }); //Değişebilir
            listParamCount.Add(new SqlParameter { ParameterName = "@TopDeger2", Value = baskets.Sum(p => (p.Quantity * p.Price) / (1 + (p.Tax / 100))) }); //Değişebilir
            listParamCount.Add(new SqlParameter { ParameterName = "@ToplamKdv", Value = baskets.Sum(p => (p.Quantity * p.Price) - ((p.Quantity * p.Price) / (1 + (p.Tax / 100)))) }); //Değişebilir
            listParamCount.Add(new SqlParameter { ParameterName = "@NetTutar1", Value = baskets.Sum(p => p.Quantity * p.Price) }); //Değişebilir
            listParamCount.Add(new SqlParameter { ParameterName = "@NetTutar2", Value = baskets.Sum(p => p.Quantity * p.Price) }); //Değişebilir
            listParamCount.Add(new SqlParameter { ParameterName = "@Odeme_Plani", Value = "WEB" });
            listParamCount.Add(new SqlParameter { ParameterName = "@Acilis_User", Value = loginCheckRes.KeysoftUserId }); // loginCheckRes.UserId + "_" + kaldırıldı Uzun geliyor varchar(10)
            listParamCount.Add(new SqlParameter { ParameterName = "@Acilis_Zaman", Value = DateTime.Now });
            listParamCount.Add(new SqlParameter { ParameterName = "@Adres_SiraNo", Value = loginCheckRes.FirmAddressId });
            _sqlDataService.GetExecute(@"INSERT INTO SIPR_Fis_Baslik (Fis_Tipi, Isyeri_Kodu, Fis_No, Fis_Tarihi, Depo_Kodu, Cari_No, Belge_Tarihi, Belge_Tipi, Belge_FisNo, Teslim_Tarihi, Kdv_Tipi1, Kdv_Tipi2, Para_Birimi, Fiyat_Tipi, Kur_Degeri, TopDeger1, TopDeger2, ToplamKdv, NetTutar1, NetTutar2, Odeme_Plani, Acilis_User, Acilis_Zaman, Adres_SiraNo) " +
                                        "VALUES (@Fis_Tipi, @Isyeri_Kodu, @Fis_No, @Fis_Tarihi, @Depo_Kodu, @Cari_No, @Belge_Tarihi, @Belge_Tipi, @Belge_FisNo, @Teslim_Tarihi, @Kdv_Tipi1, @Kdv_Tipi2, @Para_Birimi, @Fiyat_Tipi, @Kur_Degeri, @TopDeger1, @TopDeger2, @ToplamKdv, @NetTutar1, @NetTutar2, @Odeme_Plani, @Acilis_User, @Acilis_Zaman, @Adres_SiraNo)", listParamCount);

            for (int i = 0; i < baskets.Count; i++)
            {
                listParamCount.Clear();
                listParamCount.Add(new SqlParameter { ParameterName = "@Fis_Tipi", Value = 36 });
                listParamCount.Add(new SqlParameter { ParameterName = "@Isyeri_Kodu", Value = loginCheckRes.BranchId });
                listParamCount.Add(new SqlParameter { ParameterName = "@Fis_No", Value = newFisNo });
                listParamCount.Add(new SqlParameter { ParameterName = "@Fis_Sira", Value = i + 1 });
                listParamCount.Add(new SqlParameter { ParameterName = "@Fis_Tarihi", Value = DateTime.Now });
                listParamCount.Add(new SqlParameter { ParameterName = "@Islem_Depo", Value = storeCode });
                listParamCount.Add(new SqlParameter { ParameterName = "@Stok_No", Value = baskets[i].ProductId });
                listParamCount.Add(new SqlParameter { ParameterName = "@Stok_Kodu", Value = baskets[i].ProductCode });
                listParamCount.Add(new SqlParameter { ParameterName = "@Birim_Tipi", Value = baskets[i].ProductUnit });
                listParamCount.Add(new SqlParameter { ParameterName = "@Birim_Orani", Value = baskets[i].ProductUnitRate });
                listParamCount.Add(new SqlParameter { ParameterName = "@Miktari", Value = baskets[i].Quantity });
                listParamCount.Add(new SqlParameter { ParameterName = "@Para_Birimi", Value = "TL" });
                listParamCount.Add(new SqlParameter { ParameterName = "@Birim_Fiyati", Value = baskets[i].Price });
                listParamCount.Add(new SqlParameter { ParameterName = "@KDV_Tipi", Value = 1 });
                listParamCount.Add(new SqlParameter { ParameterName = "@KDV_Orani", Value = baskets[i].Tax });
                listParamCount.Add(new SqlParameter { ParameterName = "@Tutari", Value = baskets[i].Quantity * baskets[i].Price });
                listParamCount.Add(new SqlParameter { ParameterName = "@Tutari1", Value = baskets[i].Quantity * baskets[i].Price });
                listParamCount.Add(new SqlParameter { ParameterName = "@Tutari2", Value = baskets[i].Quantity * baskets[i].Price });
                listParamCount.Add(new SqlParameter { ParameterName = "@TutariP", Value = baskets[i].Quantity * baskets[i].Price });
                listParamCount.Add(new SqlParameter { ParameterName = "@TesTarihi", Value = DateTime.Now });
                listParamCount.Add(new SqlParameter { ParameterName = "@Kur_DegeriP", Value = 1 });
                listParamCount.Add(new SqlParameter { ParameterName = "@BMaliyet", Value = 0 }); //DİKKAT! Değişebilir
                _sqlDataService.GetExecute(@"INSERT INTO SIPR_Fis_Kalem (Fis_Tipi, Isyeri_Kodu, Fis_No, Fis_Sira, Fis_Tarihi, Islem_Depo, Stok_No, Stok_Kodu, Birim_Tipi, Birim_Orani, Miktari, Para_Birimi, Birim_Fiyati, KDV_Tipi, KDV_Orani, Tutari, Tutari1, Tutari2, TutariP, TesTarihi, Kur_DegeriP, BMaliyet) " +
                                            "VALUES (@Fis_Tipi, @Isyeri_Kodu, @Fis_No, @Fis_Sira, @Fis_Tarihi, @Islem_Depo, @Stok_No, @Stok_Kodu, @Birim_Tipi, @Birim_Orani, @Miktari, @Para_Birimi, @Birim_Fiyati, @KDV_Tipi, @KDV_Orani, @Tutari, @Tutari1, @Tutari2, @TutariP, @TesTarihi, @Kur_DegeriP, @BMaliyet)", listParamCount);
            }

            return true;
        }
    }
}

