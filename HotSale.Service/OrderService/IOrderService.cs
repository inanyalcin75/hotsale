﻿using HotSale.Dto;
using HotSale.Dto.Helper;
using HotSale.Dto.Operations;
using HotSale.Dto.Operations.Order;
using System;
using System.Collections.Generic;
using System.Text;

namespace HotSale.Service.OrderService
{
    public interface IOrderService
    {
        ServiceResponse<List<OrderDto>> GetOrderList(int companyId, int firmId, bool isCustomer, string term, int pageNo, int listCount, DateTime startDate, DateTime finishDate);
        List<OrderDetailDto> GetOrderDetailList(int[] orderIds);
        bool SaveOrder(int storeCode, int? accountNo, List<BasketDto> baskets, LoginCheckRes loginCheckRes);
    }
}
