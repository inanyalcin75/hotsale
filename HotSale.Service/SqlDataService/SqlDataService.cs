﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace HotSale.Service.SqlDataService
{
    public class SqlDataService : ISqlDataService
    {
        private IConfiguration _configuration { get; }

        public SqlDataService(IConfiguration configuration)
        {
            _configuration = configuration;
        }


        public SqlDataReader GetDataList(string sql, List<SqlParameter> ListSqlParameters)
        {
            var connectionString = _configuration.GetSection("AppParamConfig")["SqlConnection"];

            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand(sql, con);
            cmd.Parameters.AddRange(ListSqlParameters.ToArray());
            con.Open();
            SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            return rdr;
        }


        public int GetExecute(string sql, List<SqlParameter> ListSqlParameters)
        {
            var connectionString = _configuration.GetSection("AppParamConfig")["SqlConnection"];

            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand(sql, con);
            cmd.Parameters.AddRange(ListSqlParameters.ToArray());
            con.Open();
            var result = cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
            con.Dispose();
            return result;
        }
    }
}
