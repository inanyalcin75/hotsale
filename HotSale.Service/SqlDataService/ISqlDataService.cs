﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace HotSale.Service.SqlDataService
{
    public interface ISqlDataService
    {
        SqlDataReader GetDataList(string sql, List<SqlParameter> ListSqlParameters);

        int GetExecute(string sql, List<SqlParameter> ListSqlParameters);
    }
}
