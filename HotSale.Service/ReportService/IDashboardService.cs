﻿using HotSale.Dto.Operations;
using System;
using System.Collections.Generic;
using System.Text;

namespace HotSale.Service.ReportService
{
    public interface IDashboardService
    {
        List<ProductGroupsReportDto> GetProductsReport(int? companyId, int? companysectionId, string companyTypeId, string firmCode, DateTime startDate, DateTime finishDate);
        List<ProductsSaleReportDto> GetProductsSaleReportDto(int? companyId, int? companysectionId, string companyTypeId, string firmCode, DateTime startDate, DateTime finishDate);
        AccountReportDto GetAccountsReport(int? companyId, int? companysectionId, string companyTypeId, DateTime startDate, DateTime finishDate);
    }
}
