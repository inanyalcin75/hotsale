﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using HotSale.Dto.Operations;
using HotSale.Infrastructure.Extension;
using HotSale.Service.SqlDataService;
using Microsoft.Extensions.Configuration;

namespace HotSale.Service.ReportService
{
    public class DashboardService : IDashboardService
    {
        private readonly ISqlDataService _sqlDataService;

        public DashboardService(ISqlDataService sqlDataService)
        {
            _sqlDataService = sqlDataService;
        }

        public List<ProductGroupsReportDto> GetProductsReport(int? companyId, int? companysectionId, string companyTypeId, string firmCode, DateTime startDate, DateTime finishDate)
        {
            var list = new List<ProductGroupsReportDto>();

            switch (companysectionId)
            {
                case 1: list = CiroGroupSql(companyTypeId, startDate, finishDate); break;
                case 2: list = UretimGroupSql(companyId, companyTypeId, startDate, finishDate); break;
                case 3: list = DepoGroupSql(companyId, companyTypeId, firmCode, startDate, finishDate); break;
            }
            return list;
        }

        public List<ProductsSaleReportDto> GetProductsSaleReportDto(int? companyId, int? companysectionId, string companyTypeId, string firmCode, DateTime startDate, DateTime finishDate)
        {

            var list = new List<ProductsSaleReportDto>();


            switch (companysectionId)
            {
                case 1: list = CiroSql(companyTypeId, startDate, finishDate); break;
                case 2: list = UretimSql(companyId, companyTypeId, startDate, finishDate); break;
                case 3: list = DepoSql(companyId, companyTypeId, firmCode, startDate, finishDate); break;
            }
            return list;
        }

        private List<ProductsSaleReportDto> DepoSql(int? companyId, string companyTypeId, string firmCode, DateTime startDate, DateTime finishDate)
        {

            var list = new List<ProductsSaleReportDto>();

            var listParamater = new List<SqlParameter>();

            if (!string.IsNullOrEmpty(companyTypeId))
                listParamater.Add(new SqlParameter() { ParameterName = "@Ozel_Kodu3", Value = companyTypeId });

            if (companyId != null)
            {
                listParamater.Add(new SqlParameter() { ParameterName = "@Isyeri_Kodu", Value = companyId });
                listParamater.Add(new SqlParameter() { ParameterName = "@Firma_Kodu", Value = firmCode });
            }



            var readers = _sqlDataService.GetDataList($@"SELECT SK.Adi_1 AS Stok_Adi, SUM(TSGT.GMiktari - TSGT.CMiktari) AS Kalan
                                                         FROM T_Stok_GunlukToplam AS TSGT 
                                                         INNER JOIN STOK_Karti AS SK ON TSGT.Stok_No = SK.Stok_No AND SK.IptDrm = 0
                                                         WHERE 1=1 {(companyId == null ? "" : " AND TSGT.Islem_Depo IN (SELECT KODU FROM KEY_SYSTEM.dbo.DEPO_Karti WHERE ISYERI_KODU = @Isyeri_Kodu AND FIRMA_KODU = @Firma_Kodu)")}
                                                               {(string.IsNullOrEmpty(companyTypeId) == true ? "" : "AND SK.Ozel_Kodu3= @Ozel_Kodu3 ")} --AND TSGT.Tarih >= ''
                                                         GROUP BY SK.Adi_1", listParamater);
            var ider = 1;

            while (readers.Read())
            {
                list.Add(new ProductsSaleReportDto { Id = ider++, Name = readers["Stok_Adi"].ToString(), Count = readers["Kalan"].To<Decimal>() });
            }
            readers.Close();

            //list.Add(new ProductsSaleReportDto { Id = 1, Name = "Limonata", Count = 15, Total = 10 });
            //list.Add(new ProductsSaleReportDto { Id = 2, Name = "Alman Pastası", Count = 8, Total = 7 });
            //list.Add(new ProductsSaleReportDto { Id = 3, Name = "Kadayıf", Count = 8, Total = 4 });
            //list.Add(new ProductsSaleReportDto { Id = 4, Name = "Tulumba tatlısı", Count = 8, Total = 19 });
            //list.Add(new ProductsSaleReportDto { Id = 4, Name = "Kek", Count = 8, Total = 53 });
            return list;

        }

        private List<ProductGroupsReportDto> DepoGroupSql(int? companyId, string companyTypeId, string firmCode, DateTime startDate, DateTime finishDate)
        {
            var list = new List<ProductGroupsReportDto>();


            var listParamater = new List<SqlParameter>();

            if (!string.IsNullOrEmpty(companyTypeId))
                listParamater.Add(new SqlParameter() { ParameterName = "@Ozel_Kodu3", Value = companyTypeId });

            if (companyId != null)
            {
                listParamater.Add(new SqlParameter() { ParameterName = "@Isyeri_Kodu", Value = companyId });
                listParamater.Add(new SqlParameter() { ParameterName = "@Firma_Kodu", Value = firmCode });
            }



            var readers = _sqlDataService.GetDataList($@"SELECT SK.Ozel_Kodu3, SUM(TSGT.GMiktari - TSGT.CMiktari) AS Kalan
                                                         FROM T_Stok_GunlukToplam AS TSGT 
                                                         INNER JOIN STOK_Karti AS SK ON TSGT.Stok_No = SK.Stok_No AND SK.IptDrm = 0
                                                         WHERE 1=1 {(companyId == null ? "" : " AND TSGT.Islem_Depo IN (SELECT KODU FROM KEY_SYSTEM.dbo.DEPO_Karti WHERE ISYERI_KODU = @Isyeri_Kodu AND FIRMA_KODU = @Firma_Kodu)")}
                                                               {(string.IsNullOrEmpty(companyTypeId) == true ? "" : "AND SK.Ozel_Kodu3= @Ozel_Kodu3 ")}  --AND TSGT.Tarih >= ''
                                                         GROUP BY SK.Ozel_Kodu3", listParamater);
            var ider = 1;

            while (readers.Read())
            {
                list.Add(new ProductGroupsReportDto { Id = ider++, Name = readers["Ozel_Kodu3"].ToString(), Count = readers["Kalan"].To<Decimal>() });
            }
            readers.Close();




            //list.Add(new ProductGroupsReportDto { Id = 1, Name = "Pasta", Count = 10 });
            //list.Add(new ProductGroupsReportDto { Id = 2, Name = "Tatlı", Count = 104 });
            //list.Add(new ProductGroupsReportDto { Id = 3, Name = "Meşrubat", Count = 85 });
            return list;

        }

        private List<ProductsSaleReportDto> CiroSql(string companyTypeId, DateTime startDate, DateTime finishDate)
        {

            var list = new List<ProductsSaleReportDto>();
            list.Add(new ProductsSaleReportDto { Id = 1, Name = "Limonata", Count = 15, Total = 10 });
            list.Add(new ProductsSaleReportDto { Id = 2, Name = "Alman Pastası", Count = 8, Total = 7 });
            list.Add(new ProductsSaleReportDto { Id = 3, Name = "Kadayıf", Count = 8, Total = 4 });
            list.Add(new ProductsSaleReportDto { Id = 4, Name = "Tulumba tatlısı", Count = 8, Total = 19 });
            list.Add(new ProductsSaleReportDto { Id = 4, Name = "Kek", Count = 8, Total = 53 });
            return list;

        }

        private List<ProductGroupsReportDto> CiroGroupSql(string companyTypeId, DateTime startDate, DateTime finishDate)
        {

            var list = new List<ProductGroupsReportDto>();
            list.Add(new ProductGroupsReportDto { Id = 1, Name = "Pasta", Count = 10 });
            list.Add(new ProductGroupsReportDto { Id = 2, Name = "Tatlı", Count = 104 });
            list.Add(new ProductGroupsReportDto { Id = 3, Name = "Meşrubat", Count = 85 });
            return list;

        }

        private List<ProductsSaleReportDto> UretimSql(int? companyId, string companyTypeId, DateTime startDate, DateTime finishDate)
        {

            var list = new List<ProductsSaleReportDto>();


            //Üretim seçildiğinde Tipi bölümüne stok kodu sayı ile başlayanlar özel kodlar çekilecek örnek: ADET PASTALAR,SütLÜ tatlı 

            var listParamater = new List<SqlParameter>();
            listParamater.Add(new SqlParameter() { ParameterName = "@StardDate", Value = startDate });
            listParamater.Add(new SqlParameter() { ParameterName = "@FinishDate", Value = finishDate });

            if (!string.IsNullOrEmpty(companyTypeId))
                listParamater.Add(new SqlParameter() { ParameterName = "@Ozel_Kodu3", Value = companyTypeId });

            if (companyId != null)
            {
                listParamater.Add(new SqlParameter() { ParameterName = "@Isyeri_Kodu", Value = companyId });
            }



            var readers = _sqlDataService.GetDataList($@"SELECT SK.Adi_1 AS Stok_Adi, SUM(UIEPD.Miktari) AS Uretim_Miktari, UR.Birimi
                                                        FROM URET_IsEmri_Plan AS UIEP
                                                        INNER JOIN URET_IsEmri_Plan_Detay AS UIEPD ON UIEP.IsYeri_Kodu = UIEPD.IsYeri_Kodu AND UIEP.Fis_Tipi = UIEPD.Fis_Tipi AND UIEP.Fis_No = UIEPD.Fis_No
                                                        INNER JOIN URET_Recete AS UR ON UIEPD.Recete_Kodu = UR.Recete_Kodu
                                                        INNER JOIN STOK_Karti AS SK ON UR.Stok_No = SK.Stok_No AND SK.IptDrm = 0
                                                        WHERE UIEP.Fis_Durum = 0 AND 
                                                              {(string.IsNullOrEmpty(companyTypeId) == true ? "" : " SK.Ozel_Kodu3= @Ozel_Kodu3 AND ")} 
                                                              {(companyId == null ? "" : " UIEP.IsYeri_Kodu = @Isyeri_Kodu AND ")} 
                                                              UIEP.Fis_Tarihi >= @StardDate AND UIEP.Fis_Tarihi <= @FinishDate
                                                        GROUP BY SK.Adi_1, UR.Birimi", listParamater);
            var ider = 1;

            while (readers.Read())
            {
                list.Add(new ProductsSaleReportDto { Id = ider++, Name = readers["Stok_Adi"].ToString(), Count = readers["Uretim_Miktari"].To<Decimal>() });
            }
            readers.Close();


            //list.Add(new ProductsSaleReportDto { Id = 1, Name = "Limonata", Count = 15, Total = 10 });
            //list.Add(new ProductsSaleReportDto { Id = 2, Name = "Alman Pastası", Count = 8, Total = 7 });
            //list.Add(new ProductsSaleReportDto { Id = 3, Name = "Kadayıf", Count = 8, Total = 4 });
            //list.Add(new ProductsSaleReportDto { Id = 4, Name = "Tulumba tatlısı", Count = 8, Total = 19 });
            //list.Add(new ProductsSaleReportDto { Id = 4, Name = "Kek", Count = 8, Total = 53 });

            return list;

        }

        private List<ProductGroupsReportDto> UretimGroupSql(int? companyId, string companyTypeId, DateTime startDate, DateTime finishDate)
        {

            var list = new List<ProductGroupsReportDto>();

            //Üretim seçildiğinde Tipi bölümüne stok kodu sayı ile başlayanlar özel kodlar çekilecek örnek: ADET PASTALAR,SütLÜ tatlı 

            var listParamater = new List<SqlParameter>();
            listParamater.Add(new SqlParameter() { ParameterName = "@StardDate", Value = startDate });
            listParamater.Add(new SqlParameter() { ParameterName = "@FinishDate", Value = finishDate });

            if (!string.IsNullOrEmpty(companyTypeId))
                listParamater.Add(new SqlParameter() { ParameterName = "@Ozel_Kodu3", Value = companyTypeId });

            if (companyId != null)
            {
                listParamater.Add(new SqlParameter() { ParameterName = "@Isyeri_Kodu", Value = companyId });
            }


            var readers = _sqlDataService.GetDataList($@"SELECT SK.Ozel_Kodu3, SUM(UIEPD.Miktari) AS Uretim_Miktari, UR.Birimi
                                                        FROM URET_IsEmri_Plan AS UIEP
                                                        INNER JOIN URET_IsEmri_Plan_Detay AS UIEPD ON UIEP.IsYeri_Kodu = UIEPD.IsYeri_Kodu AND UIEP.Fis_Tipi = UIEPD.Fis_Tipi AND UIEP.Fis_No = UIEPD.Fis_No
                                                        INNER JOIN URET_Recete AS UR ON UIEPD.Recete_Kodu = UR.Recete_Kodu
                                                        INNER JOIN STOK_Karti AS SK ON UR.Stok_No = SK.Stok_No AND SK.IptDrm = 0
                                                        WHERE UIEP.Fis_Durum = 0 AND 
                                                              {(string.IsNullOrEmpty(companyTypeId) == true ? "" : " SK.Ozel_Kodu3= @Ozel_Kodu3 AND ")} 
                                                              {(companyId == null ? "" : " UIEP.IsYeri_Kodu = @Isyeri_Kodu AND ")} 
                                                              UIEP.Fis_Tarihi >= @StardDate AND UIEP.Fis_Tarihi <= @FinishDate
                                                        GROUP BY SK.Ozel_Kodu3, UR.Birimi", listParamater);
            var ider = 1;

            while (readers.Read())
            {
                list.Add(new ProductGroupsReportDto { Id = ider++, Name = readers["Ozel_Kodu3"].ToString(), Count = readers["Uretim_Miktari"].To<Decimal>() });
            }
            readers.Close();


            //list.Add(new ProductGroupsReportDto { Id = 1, Name = "Pasta", Count = 10 });
            //list.Add(new ProductGroupsReportDto { Id = 2, Name = "Tatlı", Count = 104 });
            //list.Add(new ProductGroupsReportDto { Id = 3, Name = "Meşrubat", Count = 85 });
            return list;

        }

        public AccountReportDto GetAccountsReport(int? companyId, int? companysectionId, string companyTypeId, DateTime startDate, DateTime finishDate)
        {
            var account = new AccountReportDto();

            switch (companyTypeId)
            {
                case "10": account = KasaDurumSql(companyId, startDate, finishDate); break;
                case "11": account = CariBorcAlacakSql(companyId, startDate, finishDate); break;
                case "12": account = BankaDurumSql(companyId, startDate, finishDate); break;
                case "13": account = CekSenetOdemeSql(companyId, startDate, finishDate); break;
                case "14": account = CekSenetTahsilatSql(companyId, startDate, finishDate); break;
            }

            return account;
        }

        private AccountReportDto KasaDurumSql(int? companyId, DateTime startDate, DateTime finishDate)
        {
            var account = new AccountReportDto();
            var list = new List<AccountReportDetailDto>();

            #region Kasa

            var listParamater = new List<SqlParameter>();
            listParamater.Add(new SqlParameter() { ParameterName = "@StardDate", Value = startDate });
            listParamater.Add(new SqlParameter() { ParameterName = "@FinishDate", Value = finishDate });


            if (companyId != null)
            {
                listParamater.Add(new SqlParameter() { ParameterName = "@Isyeri_Kodu", Value = companyId });
            }



            var readers = _sqlDataService.GetDataList($@"SELECT KFK.Kasa_Kodu + ' - ' + KFK.Aciklama AS Aciklama,KFK.Fis_Tarihi,
                                                                CASE KFK.Fis_Tipi WHEN 10 THEN 'Tahsil' WHEN 11 THEN 'Tediye' ELSE CONVERT(varchar(5), KFK.Fis_Tipi) END AS Fis_Tipi, 
                                                                (CASE WHEN KFK.BorcTutari > 0 THEN 'B' ELSE 'A' END) AS Tip, ABS(KFK.BorcTutari - KFK.AlckTutari) AS Tutar
                                                         FROM KASA_Fis_Kalem AS KFK 
                                                         INNER JOIN KASA_Karti AS KK ON KFK.Kasa_Kodu = KK.Kasa_Kodu
                                                         WHERE KFK.Fis_Durum = 0 
                                                               {(companyId == null ? "" : " AND KFK.Isyeri_Kodu = @Isyeri_Kodu ")} 
                                                               AND KFK.Fis_Tarihi >= @StardDate AND KFK.Fis_Tarihi <= @FinishDate", listParamater);
            var ider = 1;

            while (readers.Read())
            {
                list.Add(new AccountReportDetailDto { Id = ider++, ProcessDate = readers["Fis_Tarihi"].To<DateTime>(), ProcessName = readers["Aciklama"].ToString(), ProcessType = readers["Fis_Tipi"].ToString(), Total = readers["Tutar"].To<Decimal>() });
            }
            readers.Close();

            account.AccountReportDetailsDto = list;

            #endregion Kasa

            #region KasaDevir

            listParamater = new List<SqlParameter>();
            listParamater.Add(new SqlParameter() { ParameterName = "@StardDate", Value = startDate });


            if (companyId != null)
            {
                listParamater.Add(new SqlParameter() { ParameterName = "@Isyeri_Kodu", Value = companyId });
            }



            var readers2 = _sqlDataService.GetDataList($@"SELECT SUM(KFK.BorcTutari - KFK.AlckTutari) AS Bakiye
                                                          FROM KASA_Fis_Kalem AS KFK 
                                                          WHERE KFK.Fis_Durum = 0  
                                                                {(companyId == null ? "" : " AND KFK.Isyeri_Kodu = @Isyeri_Kodu ")} 
                                                                AND KFK.Fis_Tarihi < @StardDate", listParamater);

            if (readers2.Read())
            {
                var bakiye = readers2["Bakiye"].ToString().Replace(",", ".");

                if (bakiye == "")
                    bakiye = "0.00";

                account.Assignor = $"Kasa Devir : {string.Format("{0:C}", bakiye.To<decimal>())} Not : Bir önceki son kasa bakiyesi";
            }
            readers2.Close();


            #endregion KasaDevir

            return account;
        }

        private AccountReportDto CariBorcAlacakSql(int? companyId, DateTime startDate, DateTime finishDate)
        {
            var account = new AccountReportDto();
            var list = new List<AccountReportDetailDto>();

            #region Cari

            var listParamater = new List<SqlParameter>();
            listParamater.Add(new SqlParameter() { ParameterName = "@StardDate", Value = startDate });
            listParamater.Add(new SqlParameter() { ParameterName = "@FinishDate", Value = finishDate });


            if (companyId != null)
            {
                listParamater.Add(new SqlParameter() { ParameterName = "@Isyeri_Kodu", Value = companyId });
            }



            var readers = _sqlDataService.GetDataList($@"SELECT CFK.Aciklama + ' - ' + CFK.Belge_No AS Aciklama,CFK.Cari_Kodu,CFK.Fis_Tarihi,CK.Unvani,
                                                                CASE WHEN CFK.Fis_Tipi In(10,14) THEN 'Borç' 
                                                                     WHEN CFK.Fis_Tipi In(11,18,15) THEN 'Alacak'
                                                                     WHEN CFK.Fis_Tipi In(0) THEN 'Virman' 
                                                                     WHEN CFK.Fis_Tipi In(0) THEN 'Devir'
                                                                     WHEN CFK.Fis_Tipi In(50) THEN 'A.Fat.'
                                                                     WHEN CFK.Fis_Tipi In(51) THEN 'S.Fat.'
                                                                     WHEN CFK.Fis_Tipi In(70) THEN 'M.Çek'
                                                                     WHEN CFK.Fis_Tipi In(80) THEN 'F.Çek'
                                                                     ELSE CONVERT(varchar(5), CFK.Fis_Tipi) END AS Fis_Tipi, 
                                                                (CASE WHEN CFK.BorcTutari > 0 THEN 'B' ELSE 'A' END) AS Tip, ABS(CFK.BorcTutari - CFK.AlckTutari) AS Tutar
                                                         FROM CARI_Fis_Kalem AS CFK 
                                                         INNER JOIN CARI_Karti AS CK ON CFK.Cari_No = CK.Cari_No
                                                         WHERE CFK.Fis_Durum = 0 
                                                               {(companyId == null ? "" : " AND CFK.Isyeri_Kodu = @Isyeri_Kodu ")} 
                                                               AND CFK.Fis_Tarihi >= @StardDate AND CFK.Fis_Tarihi <= @FinishDate", listParamater);
            var ider = 1;

            while (readers.Read())
            {
                list.Add(new AccountReportDetailDto
                {
                    Id = ider++,
                    ProcessDate = readers["Fis_Tarihi"].To<DateTime>(),
                    ProcessName = $"<button type='button' class='btn btn-warning s-xs' data-toggle='tooltip' data-placement='top' title='{readers["Unvani"].ToString()}'>{readers["Cari_Kodu"].ToString()}</button>&nbsp;" + readers["Aciklama"].ToString() + $"<span style='display:none;'>{readers["Unvani"].ToString()}</span>",
                    ProcessType = readers["Fis_Tipi"].ToString(),
                    Total = readers["Tutar"].To<Decimal>()
                });
            }
            readers.Close();

            account.AccountReportDetailsDto = list;

            #endregion Cari

            #region CariDevir

            listParamater = new List<SqlParameter>();
            listParamater.Add(new SqlParameter() { ParameterName = "@StardDate", Value = startDate });


            if (companyId != null)
            {
                listParamater.Add(new SqlParameter() { ParameterName = "@Isyeri_Kodu", Value = companyId });
            }



            var readers2 = _sqlDataService.GetDataList($@"SELECT SUM(CFK.BorcTutari - CFK.AlckTutari) AS Bakiye
                                                          FROM CARI_Fis_Kalem AS CFK 
                                                          WHERE CFK.Fis_Durum = 0 
                                                                {(companyId == null ? "" : " AND CFK.Isyeri_Kodu = @Isyeri_Kodu ")} 
                                                                AND CFK.Fis_Tarihi < @StardDate", listParamater);

            if (readers2.Read())
            {
                var bakiye = readers2["Bakiye"].ToString().Replace(",", ".");

                if (bakiye == "")
                    bakiye = "0.00";

                account.Assignor = $"Cari Devir : {string.Format("{0:C}", bakiye.To<decimal>())} Not : Bir önceki son cari bakiyesi";
            }
            readers2.Close();


            #endregion CariDevir


            return account;
        }

        private AccountReportDto BankaDurumSql(int? companyId, DateTime startDate, DateTime finishDate)
        {
            var account = new AccountReportDto();
            var list = new List<AccountReportDetailDto>();

            #region Banka

            var listParamater = new List<SqlParameter>();
            listParamater.Add(new SqlParameter() { ParameterName = "@StardDate", Value = startDate });
            listParamater.Add(new SqlParameter() { ParameterName = "@FinishDate", Value = finishDate });


            if (companyId != null)
            {
                listParamater.Add(new SqlParameter() { ParameterName = "@Isyeri_Kodu", Value = companyId });
            }



            var readers = _sqlDataService.GetDataList($@"SELECT ISNULL((Select Top 1 CK.Unvani + ' - ' From CARI_Karti As CK Where CK.Cari_No = BFK.Cari_No),'') + BFK.Aciklama AS Aciklama,
                                                                BK.Banka_Adi,BFK.Fis_Tarihi,BFK.Banka_Kodu, 
                                                         CASE BFK.Fis_Tipi WHEN 12 THEN 'Para Çekme' WHEN 13 THEN 'Para Yatırma' WHEN 14 THEN 'Gelen Havale' WHEN 15 THEN 'Yapılan Havale' WHEN 16 THEN 'Virman' WHEN 18 THEN 'Masraf' ELSE  CONVERT(varchar(5), BFK.Fis_Tipi) END AS Fis_Tipi, 
                                                         (CASE WHEN BFK.BorcTutari > 0 THEN 'B' ELSE 'A' END) AS Tip, ABS(BFK.BorcTutari - BFK.AlckTutari) AS Tutar
                                                         FROM BANK_Fis_Kalem AS BFK 
                                                         INNER JOIN BANK_Karti AS BK ON BFK.Banka_Kodu = BK.Banka_Kodu
                                                         WHERE BFK.Fis_Durum = 0 
                                                               {(companyId == null ? "" : " AND BFK.Isyeri_Kodu = @Isyeri_Kodu ")}
                                                               AND BFK.Fis_Tarihi >= @StardDate AND BFK.Fis_Tarihi <= @FinishDate", listParamater);
            var ider = 1;

            while (readers.Read())
            {
                list.Add(new AccountReportDetailDto
                {
                    Id = ider++,
                    ProcessDate = readers["Fis_Tarihi"].To<DateTime>(),
                    ProcessName = $"<button type='button' class='btn btn-warning s-xs' data-toggle='tooltip' data-placement='top' title='{readers["Banka_Adi"].ToString()}'>{readers["Banka_Kodu"].ToString()}</button>&nbsp;" + readers["Aciklama"].ToString() + $"<span style='display:none;'>{readers["Banka_Adi"].ToString()}</span>",
                    ProcessType = readers["Fis_Tipi"].ToString(),
                    Total = readers["Tutar"].To<Decimal>()
                });
            }
            readers.Close();

            account.AccountReportDetailsDto = list;

            #endregion Banka

            #region BankaDevir

            listParamater = new List<SqlParameter>();
            listParamater.Add(new SqlParameter() { ParameterName = "@StardDate", Value = startDate });


            if (companyId != null)
            {
                listParamater.Add(new SqlParameter() { ParameterName = "@Isyeri_Kodu", Value = companyId });
            }



            var readers2 = _sqlDataService.GetDataList($@"SELECT SUM(BFK.BorcTutari - BFK.AlckTutari) AS Bakiye
                                                          FROM BANK_Fis_Kalem AS BFK 
                                                          WHERE BFK.Fis_Durum = 0 
                                                                {(companyId == null ? "" : " AND BFK.Isyeri_Kodu = @Isyeri_Kodu ")}
                                                                AND BFK.Fis_Tarihi < @StardDate", listParamater);

            if (readers2.Read())
            {
                var bakiye = readers2["Bakiye"].ToString().Replace(",", ".");

                if (bakiye == "")
                    bakiye = "0.00";

                account.Assignor = $"Banka Devir : {string.Format("{0:C}", bakiye.To<decimal>())} Not : Bir önceki son banka bakiyesi";
            }
            readers2.Close();


            #endregion BankaDevir



            return account;
        }

        private AccountReportDto CekSenetOdemeSql(int? companyId, DateTime startDate, DateTime finishDate)
        {
            var account = new AccountReportDto();
            var list = new List<AccountReportDetailDto>();

            var listParamater = new List<SqlParameter>();
            listParamater.Add(new SqlParameter() { ParameterName = "@StardDate", Value = startDate });
            listParamater.Add(new SqlParameter() { ParameterName = "@FinishDate", Value = finishDate });


            if (companyId != null)
            {
                listParamater.Add(new SqlParameter() { ParameterName = "@Isyeri_Kodu", Value = companyId });
            }



            var readers = _sqlDataService.GetDataList($@"SELECT CFK.CS_No + ' - ' + CK.Unvani AS Aciklama,CFK.Bordro_Tarihi,
                                                                CASE WHEN CFK.Fis_Tipi = 80 THEN 'Çek' ELSE 'Senet' END As Fis_Tipi,
                                                                CFK.Tutari
                                                         FROM CKSN_Fis_Kalem AS CFK
                                                         INNER JOIN CKSN_Fis_Baslik AS CFB ON CFK.Fis_Tipi = CFB.Fis_Tipi AND CFK.Isyeri_Kodu = CFB.Isyeri_Kodu AND CFK.Fis_No = CFB.Fis_No AND CFB.Fis_Durum = 0
                                                         INNER JOIN CARI_Karti AS CK ON CK.Cari_No = CFB.Cari_No
                                                         WHERE CFK.Fis_Durum = 0 AND CFK.Fis_Tipi IN (80, 81) 
                                                               {(companyId == null ? "" : " AND CFK.Isyeri_Kodu = @Isyeri_Kodu ")} 
                                                               AND CFK.Bordro_Tarihi >= @StardDate AND CFK.Bordro_Tarihi <= @FinishDate", listParamater);
            var ider = 1;

            while (readers.Read())
            {
                list.Add(new AccountReportDetailDto { Id = ider++, ProcessDate = readers["Bordro_Tarihi"].To<DateTime>(), ProcessName = readers["Aciklama"].ToString(), ProcessType = readers["Fis_Tipi"].ToString(), Total = readers["Tutari"].To<Decimal>() });
            }
            readers.Close();

            account.AccountReportDetailsDto = list;

            return account;
        }

        private AccountReportDto CekSenetTahsilatSql(int? companyId, DateTime startDate, DateTime finishDate)
        {
            var account = new AccountReportDto();
            var list = new List<AccountReportDetailDto>();

            var listParamater = new List<SqlParameter>();
            listParamater.Add(new SqlParameter() { ParameterName = "@StardDate", Value = startDate });
            listParamater.Add(new SqlParameter() { ParameterName = "@FinishDate", Value = finishDate });


            if (companyId != null)
            {
                listParamater.Add(new SqlParameter() { ParameterName = "@Isyeri_Kodu", Value = companyId });
            }



            var readers = _sqlDataService.GetDataList($@"SELECT CFK.CS_No + ' - ' + CK.Unvani AS Aciklama,CFK.Bordro_Tarihi,
                                                                CASE WHEN CFK.Fis_Tipi = 70 THEN 'Çek' ELSE 'Senet' END As Fis_Tipi,
                                                                CFK.Tutari
                                                         FROM CKSN_Fis_Kalem AS CFK
                                                         INNER JOIN CKSN_Fis_Baslik AS CFB ON CFK.Fis_Tipi = CFB.Fis_Tipi AND CFK.Isyeri_Kodu = CFB.Isyeri_Kodu AND CFK.Fis_No = CFB.Fis_No AND CFB.Fis_Durum = 0
                                                         INNER JOIN CARI_Karti AS CK ON CK.Cari_No = CFB.Cari_No
                                                         WHERE CFK.Fis_Durum = 0 AND CFK.Fis_Tipi IN (70, 71) 
                                                               {(companyId == null ? "" : " AND CFK.Isyeri_Kodu = @Isyeri_Kodu ")} 
                                                               AND CFK.Bordro_Tarihi >= @StardDate AND CFK.Bordro_Tarihi <= @FinishDate", listParamater);
            var ider = 1;

            while (readers.Read())
            {
                list.Add(new AccountReportDetailDto { Id = ider++, ProcessDate = readers["Bordro_Tarihi"].To<DateTime>(), ProcessName = readers["Aciklama"].ToString(), ProcessType = readers["Fis_Tipi"].ToString(), Total = readers["Tutari"].To<Decimal>() });
            }
            readers.Close();

            account.AccountReportDetailsDto = list;

            return account;
        }

    }
}
