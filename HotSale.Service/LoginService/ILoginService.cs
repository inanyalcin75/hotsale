﻿using HotSale.Dto.Operations;
using System;
using System.Collections.Generic;
using System.Text;

namespace HotSale.Service.LoginService
{
    public interface ILoginService
    {
        LoginCheckRes LoginCheck(string userName, string password);
    }
}
