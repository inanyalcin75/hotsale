﻿using HotSale.Dto.Operations;
using HotSale.Infrastructure.Extension;
using HotSale.Service.SqlDataService;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace HotSale.Service.LoginService
{
    public class LoginService : ILoginService
    {
        private readonly ISqlDataService _sqlDataService;

        public LoginService(ISqlDataService sqlDataService)
        {
            _sqlDataService = sqlDataService;
        }

        public LoginCheckRes LoginCheck(string userName, string password)
        {
            LoginCheckRes res = null;


            var readers = _sqlDataService.GetDataList(@"SELECT * FROM HotSale.dbo.UserSynchronize WHERE UserId = @UserId AND Password = @Password",
                new List<SqlParameter> {
                    new SqlParameter() { ParameterName = "@UserId", Value = userName },
                    new SqlParameter() { ParameterName = "@Password", Value = password }
                });

            if (readers.Read())
            {
                res = new LoginCheckRes { };
                res.BranchId = readers["BranchId"].To<int>();
                res.FirmId = readers["FirmId"].To<int>();
                res.FirmCode = readers["FirmCode"].ToString();
                res.UserId = readers["UserId"].ToString();
                res.KeysoftUserId = readers["KeysoftUserId"].ToString();
                res.FullName = readers["FullName"].ToString();
                res.FirmAddressId = readers["FirmAddressId"] is DBNull ? 1 : readers["FirmAddressId"].To<int>();
                res.IsPhotoAccess = readers["IsPhotoAccess"].To<bool>();
                res.IsCustomer = readers["IsCustomer"].To<bool>();
                res.IsOtherBranch = readers["IsOtherBranch"].To<bool>();
            }
            readers.Close();

            return res;
        }
    }
}
