﻿using HotSale.Dto;
using HotSale.Dto.Helper;
using System;
using System.Collections.Generic;
using System.Text;

namespace HotSale.Service.ProductService
{
    public interface IProductService
    {
        List<ProductGroupDto> GetProductGroupList(int firmId);
        ServiceResponse<List<ProductDto>> GetProductList(string productGroupId, int firmId, string term, int pageNo, int listCount);

        ServiceResponse<List<ProductDto>> GetStoreProductList(string term, int pageNo, int listCount);
    }
}
