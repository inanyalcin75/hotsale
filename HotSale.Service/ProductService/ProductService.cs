﻿using HotSale.Dto;
using HotSale.Dto.Helper;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using HotSale.Service.SqlDataService;
using System.Data.SqlClient;
using HotSale.Infrastructure.Extension;

namespace HotSale.Service.ProductService
{
    public class ProductService : IProductService
    {
        private readonly ISqlDataService _sqlDataService;

        public ProductService(ISqlDataService sqlDataService)
        {
            _sqlDataService = sqlDataService;
        }


        public List<ProductGroupDto> GetProductGroupList(int firmId)
        {
            var productGroupList = new List<ProductGroupDto>();

            var readers = _sqlDataService.GetDataList(@"SELECT DISTINCT SK.Ozel_Kodu3 FROM STOK_Karti AS SK
                                                        INNER JOIN STOK_Sirkuler_Kalem AS SSK ON SK.Stok_No = SSK.Stok_No AND SSK.Fis_Durum = 0 AND SSK.Fis_Tipi = 31 AND SSK.Cari_No = @Cari_No
                                                        WHERE SK.IptDrm = 0 AND LTRIM(RTRIM(ISNULL(SK.Ozel_Kodu3, ''))) <> '' ", new List<SqlParameter>() { new SqlParameter() { ParameterName = "@Cari_No", Value = firmId } });

            while (readers.Read())
            {
                productGroupList.Add(new ProductGroupDto { Id = readers["Ozel_Kodu3"].ToString(), Name = readers["Ozel_Kodu3"].ToString() });
            }
            readers.Close();


            return productGroupList;
        }

        public ServiceResponse<List<ProductDto>> GetProductList(string productGroupId, int firmId, string term, int pageNo, int listCount)
        {
            var totalcount = 0;
            #region ProductCount

            var listParamCount = new List<SqlParameter>();

            if (productGroupId != null)
                listParamCount.Add(new SqlParameter { ParameterName = "@Ozel_Kodu3", Value = productGroupId });

            listParamCount.Add(new SqlParameter { ParameterName = "@Cari_No", Value = firmId });
            listParamCount.Add(new SqlParameter { ParameterName = "@Term", Value = (term ?? "") });

            var readersCount = _sqlDataService.GetDataList(@"SELECT Count(0) As Count FROM
                                                        (SELECT DISTINCT SK.Stok_No
                                                         FROM STOK_Karti AS SK
                                                         INNER JOIN STOK_Sirkuler_Kalem AS SSK ON SK.Stok_No = SSK.Stok_No AND SSK.Fis_Durum = 0 AND SSK.Fis_Tipi = 31 AND SSK.Cari_No = @Cari_No  
                                                         WHERE SK.IptDrm = 0  AND (SK.Adi_1 Like '%' + @Term + '%')" + (productGroupId == null ? "" : "AND LTRIM(RTRIM(ISNULL(SK.Ozel_Kodu3, ''))) = @Ozel_Kodu3") + @") AS StockList
                                                        INNER JOIN STOK_Sirkuler_Kalem AS SSK ON StockList.Stok_No = SSK.Stok_No AND SSK.Fis_Durum = 0 AND SSK.Fis_Tipi = 31 AND SSK.Cari_No = @Cari_No 
                                                        WHERE SSK.Fis_No = (SELECT MAX(Fis_No) FROM STOK_Sirkuler_Kalem WHERE Fis_Durum = 0 AND Fis_Tipi = 31 AND Cari_No = @Cari_No AND Stok_No = StockList.Stok_No)", listParamCount);


            if (readersCount.Read())
            {
                totalcount = readersCount["Count"].To<int>();
            }
            readersCount.Close();

            #endregion ProductCount

            #region ProductList

            var listParam = new List<SqlParameter>();

            if (productGroupId != null)
                listParam.Add(new SqlParameter { ParameterName = "@Ozel_Kodu3", Value = productGroupId });

            listParam.Add(new SqlParameter { ParameterName = "@Cari_No", Value = firmId });
            listParam.Add(new SqlParameter { ParameterName = "@PageNo", Value = pageNo });
            listParam.Add(new SqlParameter { ParameterName = "@ListCount", Value = listCount });
            listParam.Add(new SqlParameter { ParameterName = "@Term", Value = (term ?? "") });




            var productList = new List<ProductDto>();
            var readers = _sqlDataService.GetDataList(@"Select * From (SELECT Top (@PageNo * @ListCount) row_number() OVER(ORDER BY B.Stok_Kodu) As RowNew,* FROM (SELECT StockList.Stok_No, SSK.Stok_Kodu, StockList.Adi_1, SSK.KDV, SSK.Brm AS Birim, SSK.Brm_Orani AS Birim_Orani, SSK.Fiyat FROM
                                                        (SELECT DISTINCT SK.Stok_No, SK.Adi_1
                                                         FROM STOK_Karti AS SK
                                                         INNER JOIN STOK_Sirkuler_Kalem AS SSK ON SK.Stok_No = SSK.Stok_No AND SSK.Fis_Durum = 0 AND SSK.Fis_Tipi = 31 AND SSK.Cari_No = @Cari_No  
                                                         WHERE SK.IptDrm = 0 AND (SK.Adi_1 Like '%' + @Term + '%') " + (productGroupId == null ? "" : "AND LTRIM(RTRIM(ISNULL(SK.Ozel_Kodu3, ''))) = @Ozel_Kodu3 ") + @") AS StockList
                                                        INNER JOIN STOK_Sirkuler_Kalem AS SSK ON StockList.Stok_No = SSK.Stok_No AND SSK.Fis_Durum = 0 AND SSK.Fis_Tipi = 31 AND SSK.Cari_No = @Cari_No 
                                                        WHERE SSK.Fis_No = (SELECT MAX(Fis_No) FROM STOK_Sirkuler_Kalem WHERE Fis_Durum = 0 AND Fis_Tipi = 31 AND Cari_No = @Cari_No AND Stok_No = StockList.Stok_No)
                                                        ) As B) As A Where A.RowNew BETWEEN ((@PageNo-1)*@ListCount)+1 AND (@PageNo * @ListCount)", listParam);

            while (readers.Read())
            {
                productList.Add(new ProductDto { Id = readers["Stok_No"].To<int>(), Name = readers["Adi_1"].ToString(), Price = readers["Fiyat"].To<decimal>(), Code = readers["Stok_Kodu"].ToString(), Tax = readers["KDV"].To<decimal>(), Unit = readers["Birim"].ToString(), UnitRate = readers["Birim_Orani"].To<decimal>() });
            }
            readers.Close();

            #endregion ProductList


            return new ServiceResponse<List<ProductDto>> { Data = productList, DataTotalCount = totalcount, Status = true };
        }

        public ServiceResponse<List<ProductDto>> GetStoreProductList(string term, int pageNo, int listCount)
        {
            var productList = new List<ProductDto>();

            var listParam = new List<SqlParameter>();
            listParam.Add(new SqlParameter { ParameterName = "@Term", Value = (term ?? "") });

            var readers = _sqlDataService.GetDataList(@"SELECT DISTINCT SK.Stok_No,SK.Adi_1,SK.Stok_Kodu, SB.Kodu AS Birim_Kodu, SB.Katsayi AS Birim_Orani, SF.Deger AS Birim_Fiyati,SK.KDV_T FROM STOK_Karti AS SK
                                                        INNER JOIN STOK_Birim AS SB ON SB.Stok_No = SK.Stok_No AND SB.IptDrm = 0 AND SB.Birim_Onceligi = 1
                                                        INNER JOIN STOK_Fiyat AS SF ON SF.Stok_No = SK.Stok_No AND SF.IptDrm = 0 AND SF.Tip = 'AFYT'
                                                        WHERE SK.IptDrm = 0 AND 
                                                              (SK.Adi_1 Like '%' + @Term + '%') AND
                                                              LTRIM(RTRIM(ISNULL(SK.Ozel_Kodu3, ''))) <> '' AND SK.Stok_Kodu >= 'a'", listParam);

            //"SELECT KODU, ADI FROM KEY_SYSTEM.dbo.DEPO_Karti WHERE FIRMA_KODU = 'KERKELI' AND ISYERI_KODU = @ISYERI_KODU "

            while (readers.Read())
            {
                productList.Add(new ProductDto { Id = readers["Stok_No"].To<int>(), Name = readers["Adi_1"].ToString(), Price = readers["Birim_Fiyati"].To<decimal>(), Code = readers["Stok_Kodu"].ToString(), Tax = readers["KDV_T"].To<decimal>(), Unit = readers["Birim_Kodu"].ToString(), UnitRate = readers["Birim_Orani"].To<decimal>() });
            }
            readers.Close();



            //productList.Add(new ProductDto { Id = 1, Name = "Zeytin", Price = 100, Unit = "AD", UnitRate = 10 });
            //productList.Add(new ProductDto { Id = 2, Name = "Peynir", Price = 100, Unit = "KG", UnitRate = 10 });
            //productList.Add(new ProductDto { Id = 3, Name = "Ekmek", Price = 100, Unit = "KG", UnitRate = 10 });
            return new ServiceResponse<List<ProductDto>> { Data = productList, Status = true };
        }
    }
}
