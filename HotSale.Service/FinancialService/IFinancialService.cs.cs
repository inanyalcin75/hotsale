﻿using System;
using System.Collections.Generic;
using System.Text;
using HotSale.Dto;
using HotSale.Dto.Helper;
using HotSale.Dto.Operations.Finance;
using HotSale.Infrastructure.Extension;
using HotSale.Service.SqlDataService;


namespace HotSale.Service.FinancialService
{
    public interface IFinancialService
    {
        ServiceResponse<CurrentAccountRes> GetCurrentAccountList(int firmId, bool isCustomer, int pageNo, int listCount, DateTime startDate, DateTime finishDate);
        ServiceResponse<List<BillDto>> GetBillList(int companyId, string term, int pageNo, int listCount, DateTime startDate, DateTime finishDate);
        ServiceResponse<WayBillRes> GetWayBillList(int companyId, int firmId, bool isCustomer, string term, int pageNo, int listCount, DateTime startDate, DateTime finishDate);
        List<WayBillDetailDto> GetWayBillDetailList(int[] orderIds);

    }
}
