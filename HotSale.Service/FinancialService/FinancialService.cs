﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using HotSale.Dto;
using HotSale.Dto.Helper;
using HotSale.Dto.Operations.Finance;
using HotSale.Infrastructure.Extension;
using HotSale.Service.SqlDataService;

namespace HotSale.Service.FinancialService
{
    public class FinancialService : IFinancialService
    {

        private readonly ISqlDataService _sqlDataService;

        public FinancialService(ISqlDataService sqlDataService)
        {
            _sqlDataService = sqlDataService;
        }

        public ServiceResponse<List<BillDto>> GetBillList(int companyId, string term, int pageNo, int listCount, DateTime startDate, DateTime finishDate)
        {

            var counter = listCount * (pageNo - 1);
            var totalcount = 0;

            var orderList = new List<OrderDto>();

            var listParamater = new List<SqlParameter>();
            listParamater.Add(new SqlParameter { ParameterName = "@PageNo", Value = pageNo });
            listParamater.Add(new SqlParameter { ParameterName = "@ListCount", Value = listCount });
            listParamater.Add(new SqlParameter() { ParameterName = "@StardDate", Value = startDate });
            listParamater.Add(new SqlParameter() { ParameterName = "@FinishDate", Value = finishDate });
            listParamater.Add(new SqlParameter() { ParameterName = "@Isyeri_Kodu", Value = companyId });

            if (!string.IsNullOrEmpty(term))
                listParamater.Add(new SqlParameter() { ParameterName = "@Term", Value = "%" + term + "%" });

            var reader = _sqlDataService.GetDataList($@"Select * From (SELECT Top (@PageNo * @ListCount) row_number() OVER(ORDER BY B.Fis_No DESC) As RowNew,* FROM (SELECT SFB.Fis_No, CK.Unvani, SFB.NetTutar1 --Duruma göre alanları eklersin
                                                       FROM SIPR_Fis_Baslik AS SFB
                                                       INNER JOIN CARI_Karti AS CK ON SFB.Cari_No = CK.Cari_No AND CK.IptDrm = 0
                                                       WHERE SFB.Fis_Durum = 0 AND SFB.Fis_Tipi = 36 
                                                             AND SFB.Isyeri_Kodu = @Isyeri_Kodu  
                                                             AND SFB.Fis_Tarihi >= @StardDate AND SFB.Fis_Tarihi <= @FinishDate
                                                             { (string.IsNullOrEmpty(term) ? "" : "AND CK.Unvani Like @Term") }
                                                    ) As B) As A Where A.RowNew BETWEEN ((@PageNo-1)*@ListCount)+1 AND (@PageNo * @ListCount)", listParamater);
            var ider = 1;

            while (reader.Read())
            {
                orderList.Add(new OrderDto { Id = ider++, FisNo = reader["Fis_No"].To<int>(), Unvani = reader["Unvani"].ToString(), Tutari = reader["NetTutar1"].To<decimal>() });
            }

            reader.Close();

            listParamater = new List<SqlParameter>();
            listParamater.Add(new SqlParameter { ParameterName = "@PageNo", Value = pageNo });
            listParamater.Add(new SqlParameter { ParameterName = "@ListCount", Value = listCount });
            listParamater.Add(new SqlParameter() { ParameterName = "@StardDate", Value = startDate });
            listParamater.Add(new SqlParameter() { ParameterName = "@FinishDate", Value = finishDate });
            listParamater.Add(new SqlParameter() { ParameterName = "@Isyeri_Kodu", Value = companyId });

            if (!string.IsNullOrEmpty(term))
                listParamater.Add(new SqlParameter() { ParameterName = "@Term", Value = "%" + term + "%" });

            reader = _sqlDataService.GetDataList($@"SELECT Count(0) As Count
                                                   FROM SIPR_Fis_Baslik AS SFB
                                                   INNER JOIN CARI_Karti AS CK ON SFB.Cari_No = CK.Cari_No AND CK.IptDrm = 0
                                                   WHERE SFB.Fis_Durum = 0 AND SFB.Fis_Tipi = 36 
                                                             AND SFB.Isyeri_Kodu = @Isyeri_Kodu 
                                                             { (string.IsNullOrEmpty(term) ? "" : "AND CK.Unvani Like @Term ") }
                                                             AND SFB.Fis_Tarihi >= @StardDate AND SFB.Fis_Tarihi <= @FinishDate", listParamater);

            while (reader.Read())
            { totalcount = reader["Count"].To<int>(); }

            reader.Close();

            return new ServiceResponse<List<BillDto>> { DataTotalCount = totalcount, Status = true };
        }


        public ServiceResponse<WayBillRes> GetWayBillList(int companyId, int firmId, bool isCustomer, string term, int pageNo, int listCount, DateTime startDate, DateTime finishDate)
        {

            var counter = listCount * (pageNo - 1);
            var totalcount = 0;
            decimal total = 0;

            var wayBillList = new List<WayBillDto>();

            var listParamater = new List<SqlParameter>();
            listParamater.Add(new SqlParameter { ParameterName = "@PageNo", Value = pageNo });
            listParamater.Add(new SqlParameter { ParameterName = "@ListCount", Value = listCount });
            listParamater.Add(new SqlParameter() { ParameterName = "@StardDate", Value = startDate });
            listParamater.Add(new SqlParameter() { ParameterName = "@FinishDate", Value = finishDate });

            if (isCustomer)
                listParamater.Add(new SqlParameter() { ParameterName = "@Cari_No", Value = firmId });

            if (!string.IsNullOrEmpty(term))
                listParamater.Add(new SqlParameter() { ParameterName = "@Term", Value = "%" + term + "%" });

            var reader = _sqlDataService.GetDataList($@"Select * From (SELECT Top (@PageNo * @ListCount) row_number() OVER(ORDER BY Fis_No DESC) As RowNew,* FROM (SELECT IB.Isyeri_Kodu, IB.Fis_No, IB.Fis_Tarihi, IB.Sip_No, IB.Belge_No
                                                             FROM IRSL_Baslik As IB
                                                             INNER JOIN CARI_Karti AS CK ON IB.Cari_No = CK.Cari_No AND CK.IptDrm = 0
                                                             WHERE IB.Fis_Durum = 0 AND IB.Fis_Tipi = 41 
                                                            { (!isCustomer ? " " : " AND IB.Cari_No = @Cari_No ") }
                                                             AND IB.Fis_Tarihi >= @StardDate AND IB.Fis_Tarihi <= @FinishDate
                                                             { (string.IsNullOrEmpty(term) ? "" : "AND CK.Unvani Like @Term") }
                                                    ) As B) As A Where A.RowNew BETWEEN ((@PageNo-1)*@ListCount)+1 AND (@PageNo * @ListCount)", listParamater);
            var ider = 1;

            while (reader.Read())
            {
                wayBillList.Add(new WayBillDto { Id = ider++, FisNo = reader["Fis_No"].To<int>(), Belge_No = reader["Belge_No"].ToString(), FisTarihi = reader["Fis_Tarihi"].To<DateTime>(), IsyeriKodu = reader["Isyeri_Kodu"].ToString(), SipNo = reader["Sip_No"].To<int>() });
            }

            reader.Close();

            listParamater = new List<SqlParameter>();
            listParamater.Add(new SqlParameter { ParameterName = "@PageNo", Value = pageNo });
            listParamater.Add(new SqlParameter { ParameterName = "@ListCount", Value = listCount });
            listParamater.Add(new SqlParameter() { ParameterName = "@StardDate", Value = startDate });
            listParamater.Add(new SqlParameter() { ParameterName = "@FinishDate", Value = finishDate });

            if (isCustomer)
                listParamater.Add(new SqlParameter() { ParameterName = "@Cari_No", Value = firmId });

            if (!string.IsNullOrEmpty(term))
                listParamater.Add(new SqlParameter() { ParameterName = "@Term", Value = "%" + term + "%" });

            reader = _sqlDataService.GetDataList($@"SELECT Count(0) As Count,(SELECT Sum(SFK.CTutari) FROM STOK_Fis_Kalem AS SFK INNER JOIN IRSL_Baslik As IBSub On IBSub.Fis_Tipi = SFK.Fis_Tipi AND IBSub.Fis_No = SFK.Fis_No AND IBSub.Isyeri_Kodu = SFK.Isyeri_Kodu  Where SFK.Fis_Durum = 0 AND SFK.Fis_Tipi = 41 { (!isCustomer ? " " : " AND IBSub.Cari_No = @Cari_No ") } AND SFK.Fis_Tarihi >= @StardDate AND SFK.Fis_Tarihi <= @FinishDate) As Tutar
                                                    FROM IRSL_Baslik As IB
                                                    INNER JOIN CARI_Karti AS CK ON IB.Cari_No = CK.Cari_No AND CK.IptDrm = 0
                                                    WHERE IB.Fis_Durum = 0 AND IB.Fis_Tipi = 41 
                                                            { (!isCustomer ? " " : " AND IB.Cari_No = @Cari_No ") }
                                                             AND IB.Fis_Tarihi >= @StardDate AND IB.Fis_Tarihi <= @FinishDate
                                                             { (string.IsNullOrEmpty(term) ? "" : "AND CK.Unvani Like @Term") }", listParamater);

            while (reader.Read())
            {
                totalcount = reader["Count"].To<int>();
                total = reader["Tutar"].To<decimal>();
            }

            reader.Close();



            return new ServiceResponse<WayBillRes> { Data = new WayBillRes { WayBillDtos = wayBillList, Total = total }, DataTotalCount = totalcount, Status = true };
        }

        public List<WayBillDetailDto> GetWayBillDetailList(int[] orderIds)
        {
            var orderDetailList = new List<WayBillDetailDto>();
            var listParamater = new List<SqlParameter>();

            var sqlParam = string.Join(",", orderIds);


            var reader = _sqlDataService.GetDataList($@"SELECT SFK.Fis_No,SFK.Isyeri_Kodu, SFK.Fis_Sira, SK.Stok_Kodu, SK.Adi_1 AS Stok_Adi, SFK.CMiktari AS Miktar, SFK.Birim_Fiyati AS Fiyat, SFK.CTutari AS Tutar,SFK.Birim_Tipi
                                                       FROM STOK_Fis_Kalem AS SFK
                                                       INNER JOIN STOK_Karti AS SK ON SK.IptDrm = 0 AND SK.Stok_No = SFK.Stok_No
                                                       WHERE SFK.Fis_Durum = 0 AND SFK.Fis_Tipi = 41 
                                                             AND SFK.Fis_No IN ({sqlParam})", listParamater);

            while (reader.Read())
            {
                orderDetailList.Add(new WayBillDetailDto { IsyeriKodu = reader["Isyeri_Kodu"].ToString(), FisNo = reader["Fis_No"].To<int>(), FisSira = reader["Fis_Sira"].To<int>(), Stok_Kodu = reader["Stok_Kodu"].ToString(), Stok_Adi = reader["Stok_Adi"].ToString(), Miktari = reader["Miktar"].To<decimal>(), Fiyat = reader["Fiyat"].To<decimal>(), Tutari = reader["Tutar"].To<decimal>(), BirimTipi = reader["Birim_Tipi"].ToString() });
            }

            reader.Close();


            return orderDetailList;
        }


        public ServiceResponse<CurrentAccountRes> GetCurrentAccountList(int firmId, bool isCustomer, int pageNo, int listCount, DateTime startDate, DateTime finishDate)
        {

            var counter = listCount * (pageNo - 1);
            var totalcount = 0;

            var currentAccount = new CurrentAccountRes();
            var currentAccountList = new List<CurrentAccountDto>();

            var listParamater = new List<SqlParameter>();
            listParamater.Add(new SqlParameter { ParameterName = "@PageNo", Value = pageNo });
            listParamater.Add(new SqlParameter { ParameterName = "@ListCount", Value = listCount });
            listParamater.Add(new SqlParameter() { ParameterName = "@StardDate", Value = startDate });
            listParamater.Add(new SqlParameter() { ParameterName = "@FinishDate", Value = finishDate });

            if (isCustomer)
                listParamater.Add(new SqlParameter() { ParameterName = "@Cari_No", Value = firmId });


            var reader = _sqlDataService.GetDataList($@"Select * From (SELECT Top (@PageNo * @ListCount) row_number() OVER(ORDER BY B.Fis_No DESC) As RowNew,* FROM (
                                                        SELECT Fis_Sira,
                                                               CASE WHEN Fis_Tipi In(10,14) THEN 'Borç' 
                                                                    WHEN Fis_Tipi In(11,18,15) THEN 'Alacak'
                                                                    WHEN Fis_Tipi In(0) THEN 'Virman' 
                                                                    WHEN Fis_Tipi In(0) THEN 'Devir'
                                                                    WHEN Fis_Tipi In(50) THEN 'A.Fat.'
                                                                    WHEN Fis_Tipi In(51) THEN 'S.Fat.'
                                                                    WHEN Fis_Tipi In(70) THEN 'M.Çek'
                                                                    WHEN Fis_Tipi In(80) THEN 'F.Çek'
                                                               ELSE CONVERT(varchar(5), Fis_Tipi) END AS Fis_Tipi, 
                                                               Isyeri_Kodu, Fis_No, Fis_Tarihi, Belge_No, BorcTutari, AlckTutari FROM CARI_Fis_Kalem 
                                                        WHERE Fis_Durum = 0 
                                                              { (!isCustomer ? " " : " AND Cari_No = @Cari_No ") } 
                                                              AND Fis_Tarihi >= @StardDate AND Fis_Tarihi <= @FinishDate) As B) As A Where A.RowNew BETWEEN ((@PageNo-1)*@ListCount)+1 AND (@PageNo * @ListCount)", listParamater);

            while (reader.Read())
            {
                currentAccountList.Add(new CurrentAccountDto
                {
                    FisSira = reader["Fis_Sira"].To<int>(),
                    FisTipi = reader["Fis_Tipi"].ToString(),
                    IsyeriKodu = reader["Isyeri_Kodu"].To<int>(),
                    FisNo = reader["Fis_No"].To<int>(),
                    FisTarihi = reader["Fis_Tarihi"].To<DateTime>(),
                    BelgeNo = reader["Belge_No"].ToString(),
                    BorcTutari = reader["BorcTutari"].To<decimal>(),
                    AlckTutari = reader["AlckTutari"].To<decimal>()
                });
            }

            reader.Close();

            listParamater = new List<SqlParameter>();
            listParamater.Add(new SqlParameter() { ParameterName = "@StardDate", Value = startDate });
            listParamater.Add(new SqlParameter() { ParameterName = "@FinishDate", Value = finishDate });

            if (isCustomer)
                listParamater.Add(new SqlParameter() { ParameterName = "@Cari_No", Value = firmId });



            reader = _sqlDataService.GetDataList($@"SELECT Count(0) As Count,Sum(AlckTutari) As AlckTutari,Sum(BorcTutari) As BorcTutari FROM CARI_Fis_Kalem 
                                                    WHERE Fis_Durum = 0 
                                                          { (!isCustomer ? " " : " AND Cari_No = @Cari_No ") } 
                                                          AND Fis_Tarihi >= @StardDate AND Fis_Tarihi <= @FinishDate", listParamater);

            while (reader.Read())
            {
                totalcount = reader["Count"].To<int>();
                currentAccount.TotalAlckTutari = reader["AlckTutari"].To<decimal>();
                currentAccount.TotalBorcTutari = reader["BorcTutari"].To<decimal>();
            }

            reader.Close();

            currentAccount.CurrentAccountsDto = new ServiceResponse<List<CurrentAccountDto>>() { Data = currentAccountList };

            return new ServiceResponse<CurrentAccountRes> { Data = currentAccount, Status = true, DataTotalCount = totalcount };
        }



    }
}
