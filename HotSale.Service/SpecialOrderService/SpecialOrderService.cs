﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using HotSale.Dto.Operations.SpecialOrder;
using HotSale.Infrastructure.Extension;
using HotSale.Service.SqlDataService;

namespace HotSale.Service.SpecialOrderService
{
    public class SpecialOrderService : ISpecialOrderService
    {
        private readonly ISqlDataService _sqlDataService;

        public SpecialOrderService(ISqlDataService sqlDataService)
        {
            _sqlDataService = sqlDataService;
        }

        public List<SpecialOrderStockResDto> GetStock(string term)
        {
            var productList = new List<SpecialOrderStockResDto>();

            var listParam = new List<SqlParameter>();
            listParam.Add(new SqlParameter { ParameterName = "@Term", Value = (term ?? "") });

            var readers = _sqlDataService.GetDataList(@"SELECT DISTINCT SK.Stok_No,SK.Adi_1,SK.Stok_Kodu, SB.Kodu AS Birim_Kodu, SB.Katsayi AS Birim_Orani, SF.Deger AS Birim_Fiyati,SK.KDV_T FROM STOK_Karti AS SK
                                                        INNER JOIN STOK_Birim AS SB ON SB.Stok_No = SK.Stok_No AND SB.IptDrm = 0 AND SB.Birim_Onceligi = 1
                                                        INNER JOIN STOK_Fiyat AS SF ON SF.Stok_No = SK.Stok_No AND SF.IptDrm = 0 AND SF.Tip = 'AFYT'
                                                        WHERE SK.IptDrm = 0 AND 
                                                              (SK.Adi_1 Like '%' + @Term + '%') AND
                                                              LTRIM(RTRIM(ISNULL(SK.Ozel_Kodu3, ''))) <> '' AND SK.Stok_Kodu >= 'a'", listParam);

            while (readers.Read())
            {
                productList.Add(new SpecialOrderStockResDto { StockCode = readers["Stok_Kodu"].ToString(), StockName = readers["Adi_1"].ToString() });
            }
            readers.Close();

            return productList;
        }

        public bool SaveOrder(int branchId, int depoKodu, string StockCode, string StockDetail, string Description, string DeliveryAddress, string DeliveryDate, string DeliveryTime)
        {
            var listParamCount = new List<SqlParameter>();

            int newFisNo = 1;
            var reader = _sqlDataService.GetDataList("SELECT ISNULL(MAX(Fis_No), CAST(0 AS INT)) + CAST(1 AS INT) AS NewFisNo FROM SIPR_Fis_Bilgi WHERE Fis_Tipi = 36 AND Isyeri_Kodu = " + branchId, listParamCount);
            if (reader.Read())
                newFisNo = reader["NewFisNo"].To<int>();
            reader.Close();



            listParamCount.Add(new SqlParameter { ParameterName = "@Isyeri_Kodu", Value = branchId });
            listParamCount.Add(new SqlParameter { ParameterName = "@Fis_Tipi", Value = 36 });
            listParamCount.Add(new SqlParameter { ParameterName = "@Fis_No", Value = newFisNo });
            listParamCount.Add(new SqlParameter { ParameterName = "@Fis_Durum", Value = 0 });
            listParamCount.Add(new SqlParameter { ParameterName = "@Fis_Tarihi", Value = DateTime.Now });
            listParamCount.Add(new SqlParameter { ParameterName = "@Teslimat_Saati", Value = DeliveryTime });
            listParamCount.Add(new SqlParameter { ParameterName = "@Fis_Saati", Value = DateTime.Now.Hour.ToString() + ":" + DateTime.Now.Second.ToString() });
            listParamCount.Add(new SqlParameter { ParameterName = "@Aciklama", Value = Description });
            listParamCount.Add(new SqlParameter { ParameterName = "@Teslimat", Value = Convert.ToDateTime(DeliveryDate) });


            _sqlDataService.GetExecute(@"INSERT INTO SIPR_Fis_Bilgi (Isyeri_Kodu,  Fis_Tipi,  Fis_No,  Fis_Durum,  Fis_Tarihi, Teslimat_Saati, Fis_Saati, Aciklama,  Teslimat_Tarihi) " +
                                                            "VALUES (@Isyeri_Kodu, @Fis_Tipi, @Fis_No, @Fis_Durum, @Fis_Tarihi,@Teslimat_Saati,@Fis_Saati,@Aciklama, @Teslimat_Tarihi)", listParamCount);


            listParamCount = new List<SqlParameter>();

            listParamCount.Add(new SqlParameter { ParameterName = "@Isyeri_Kodu", Value = branchId });
            listParamCount.Add(new SqlParameter { ParameterName = "@Fis_Tipi", Value = 36 });
            listParamCount.Add(new SqlParameter { ParameterName = "@Fis_No", Value = newFisNo });
            listParamCount.Add(new SqlParameter { ParameterName = "@Fis_Durum", Value = 0 });
            listParamCount.Add(new SqlParameter { ParameterName = "@Fis_Sira", Value = 1 });
            listParamCount.Add(new SqlParameter { ParameterName = "@Icerik", Value = StockDetail });
            listParamCount.Add(new SqlParameter { ParameterName = "@Aciklama", Value = Description });
            listParamCount.Add(new SqlParameter { ParameterName = "@StokNo", Value = StockCode });
            listParamCount.Add(new SqlParameter { ParameterName = "@Depo_Kodu", Value = depoKodu });
            listParamCount.Add(new SqlParameter { ParameterName = "@Stok_Birim", Value = "AD" });



            _sqlDataService.GetExecute(@"INSERT INTO SIPR_Fis_Bilgi_Detay (Isyeri_Kodu, Fis_Tipi, Fis_No, Fis_Durum, Fis_Sira, Icerik, Aciklama, StokNo, Depo_Kodu,Stok_Birim) " +
                                      "VALUES (@Isyeri_Kodu, @Fis_Tipi, @Fis_No, @Fis_Durum, @Fis_Sira, @Icerik, @Aciklama, @StokNo, @Depo_Kodu,@Stok_Birim)", listParamCount);

            return true;
        }
    }
}
