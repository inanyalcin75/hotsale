﻿using HotSale.Dto.Operations.SpecialOrder;
using System;
using System.Collections.Generic;
using System.Text;

namespace HotSale.Service.SpecialOrderService
{
    public interface ISpecialOrderService
    {
        bool SaveOrder(int branchId, int depoKodu, string StockCode, string StockDetail, string Description, string DeliveryAddress, string DeliveryDate, string DeliveryTime);
        List<SpecialOrderStockResDto> GetStock(string term);

    }
}
