﻿using HotSale.Dto;
using HotSale.Dto.Operations;
using System;
using System.Collections.Generic;
using System.Text;

namespace HotSale.Service.CompanyService
{
    public interface ICompanyService
    {
        List<CompanyDto> GetCompanyList(bool cache = true);
        List<CompanySectionDto> GetCompanySectionList();
        List<CompanyTypeDto> GetCompanyTypeList(int companysectionId);
        List<CompanyStoreDto> GetCompanyStoreList(bool cache, int isyerKodu, string firmCode);
    }
}
