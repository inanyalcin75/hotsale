﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using HotSale.Cache;
using HotSale.Dto;
using HotSale.Dto.Helper;
using HotSale.Dto.Operations;
using HotSale.Infrastructure.Extension;
using HotSale.Service.SqlDataService;
using Microsoft.Extensions.Configuration;

namespace HotSale.Service.CompanyService
{
    public class CompanyService : ICompanyService
    {
        private readonly ISqlDataService _sqlDataService;
        private readonly IConfiguration _configuration;
        private readonly ICacheProvider _cacheProvider;

        //========================================================================================================================
        // Constructor
        //========================================================================================================================
        public CompanyService(ICacheProvider cacheProvider, ISqlDataService sqlDataService, IConfiguration configuration)
        {
            //Injection
            _cacheProvider = cacheProvider;
            _sqlDataService = sqlDataService;
            _configuration = configuration;
        }




        public List<CompanyDto> GetCompanyList(bool cache = true)
        {
            var companyCode = _configuration.GetSection("AppParamConfig")["CompanyCode"];
            var readers = _sqlDataService.GetDataList("SELECT * FROM KEY_SYSTEM.dbo.ISYR_Karti WHERE FIRMA_KODU = @CompanyCode", new List<SqlParameter> { new SqlParameter { ParameterName = "@CompanyCode", Value = companyCode } });

            var list = new List<CompanyDto>();

            while (readers.Read())
            {
                list.Add(new CompanyDto { Id = readers["KODU"].To<int>(), Name = readers["ADI"].ToString() });
            }
            readers.Close();

            return list;
        }


        public List<CompanyStoreDto> GetCompanyStoreList(bool cache, int isyerKodu, string firmCode)
        {
            var companyStores = new List<CompanyStoreDto>();

            if (cache)
            {
                companyStores = _cacheProvider.Get<List<CompanyStoreDto>>(enmCacheNames.CompanyStoreList);
            }

            if (companyStores == null)
            {
                companyStores = new List<CompanyStoreDto>();

                var readers = _sqlDataService.GetDataList(@"SELECT KODU,ADI FROM KEY_SYSTEM.dbo.DEPO_Karti WHERE FIRMA_KODU = @firmCode AND ISYERI_KODU = @isyeriKodu",
                                                            new List<SqlParameter> { new SqlParameter { ParameterName = "@firmCode", Value = firmCode },
                                                                                 new SqlParameter { ParameterName = "@isyeriKodu", Value = isyerKodu }
                                                             });

                while (readers.Read())
                {
                    companyStores.Add(new CompanyStoreDto { Kodu = readers["KODU"].To<int>(), Adi = readers["ADI"].ToString() });
                }
                readers.Close();

                _cacheProvider.Set<List<CompanyStoreDto>>(enmCacheNames.CompanyStoreList, companyStores);

            }


            return companyStores;
        }





        public List<CompanySectionDto> GetCompanySectionList()
        {
            var list = new List<CompanySectionDto>();
            //list.Add(new CompanySectionDto { Id = 1, Name = "Ciro" }); Not : Keysatıştan dolayı şimdlik iptal ettik
            list.Add(new CompanySectionDto { Id = 2, Name = "Üretim" });
            list.Add(new CompanySectionDto { Id = 3, Name = "Depo" });
            list.Add(new CompanySectionDto { Id = 4, Name = "Muhasebe" });
            return list;
        }

        public List<CompanyTypeDto> GetCompanyTypeList(int companysectionId)
        {

            var list = new List<CompanyTypeDto>();


            //Ciro
            if (companysectionId == 1)
            {
                list = CiroSql();
            }
            else if (companysectionId == 2)
            {//Üretim
                list = UretimSql();
            }
            else if (companysectionId == 3)
            {//Depo
                list = DepoSql();
            }
            else if (companysectionId == 4)
            {//Muhasebe
                list = MuhasebeSql();
            }




            return list;
        }

        //Sefayla yazılacak
        private List<CompanyTypeDto> CiroSql()
        {
            var list = new List<CompanyTypeDto>();
            list.Add(new CompanyTypeDto { Id = "1", Name = "Nakit Satış", Value = "12.000 TL" });
            list.Add(new CompanyTypeDto { Id = "2", Name = "Kredi Kartı", Value = "12.000 TL" });
            list.Add(new CompanyTypeDto { Id = "3", Name = "Zayi", Value = "8.000 TL" });
            list.Add(new CompanyTypeDto { Id = "4", Name = "Fire", Value = "5.000 TL" });
            list.Add(new CompanyTypeDto { Id = "5", Name = "Kalan", Value = "4.000 TL" });
            return list;
        }


        private List<CompanyTypeDto> MuhasebeSql()
        {
            var list = new List<CompanyTypeDto>();
            //Muhasebe-Tipi-Kasa Durumu-KASA_Fis_Kalem,Cari Borç Alacak,Banka Durumu,Çek Senet Ödeme,Çek Senet Tahsilat
            list.Add(new CompanyTypeDto { Id = "10", Name = "Kasa Durumu" });
            list.Add(new CompanyTypeDto { Id = "11", Name = "Cari Borç Alacak" });
            list.Add(new CompanyTypeDto { Id = "12", Name = "Banka Durumu" });
            list.Add(new CompanyTypeDto { Id = "13", Name = "Çek Senet Ödeme" });
            list.Add(new CompanyTypeDto { Id = "14", Name = "Çek Senet Tahsilat" });
            return list;
        }



        private List<CompanyTypeDto> DepoSql()
        {
            var list = new List<CompanyTypeDto>();
            //Depo-stok kodu Harfle başlayan ürün grupları gelicek tipi listesi için

            var readers = _sqlDataService.GetDataList(@"SELECT DISTINCT SK.Ozel_Kodu3 FROM STOK_Karti AS SK
                                                        WHERE SK.IptDrm = 0 AND LTRIM(RTRIM(ISNULL(SK.Ozel_Kodu3, ''))) <> '' AND SK.Stok_Kodu >= 'a'", new List<SqlParameter>() { });

            while (readers.Read())
            {
                list.Add(new CompanyTypeDto { Id = readers["Ozel_Kodu3"].ToString(), Name = readers["Ozel_Kodu3"].ToString() });
            }
            readers.Close();


            //list.Add(new CompanyTypeDto { Id = "8", Name = "Elma" });
            //list.Add(new CompanyTypeDto { Id = "9", Name = "Zeytin" });
            return list;
        }

        private List<CompanyTypeDto> UretimSql()
        {
            var list = new List<CompanyTypeDto>();
            //Üretim seçildiğinde Tipi bölümüne stok kodu sayı ile başlayanlar özel kodlar çekilecek örnek: ADET PASTALAR,SütLÜ tatlı 

            var readers = _sqlDataService.GetDataList(@"SELECT DISTINCT SK.Ozel_Kodu3 FROM STOK_Karti AS SK
                                                        WHERE SK.IptDrm = 0 AND LTRIM(RTRIM(ISNULL(SK.Ozel_Kodu3, ''))) <> '' AND SK.Stok_Kodu >= 'a'", new List<SqlParameter>());

            while (readers.Read())
            {
                list.Add(new CompanyTypeDto { Id = readers["Ozel_Kodu3"].ToString(), Name = readers["Ozel_Kodu3"].ToString() });
            }
            readers.Close();

            //list.Add(new CompanyTypeDto { Id = "1", Name = "Pasta Grupları" });
            //list.Add(new CompanyTypeDto { Id = "2", Name = "Pasta Grupları2" });

            return list;
        }





    }
}
