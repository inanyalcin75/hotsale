﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using HotSale.Dto;
using HotSale.Dto.Helper;
using HotSale.Dto.Operations;
using HotSale.Dto.Operations.Store;
using HotSale.Infrastructure.Extension;
using HotSale.Service.SqlDataService;

namespace HotSale.Service.StoreService
{
    public class StoreService : IStoreService
    {
        private readonly ISqlDataService _sqlDataService;

        public StoreService(ISqlDataService sqlDataService)
        {
            _sqlDataService = sqlDataService;
        }


        public List<StoreDetailDto> GetStoreDetailList(int[] storeIds)
        {
            var storeDetailList = new List<StoreDetailDto>();


            var listParamater = new List<SqlParameter>();

            var sqlParam = string.Join(",", storeIds);

            var reader = _sqlDataService.GetDataList($@"SELECT SFK.Fis_No, SK.Stok_Kodu, SK.Adi_1 AS Stok_Adi, SFK.Miktari
                                                    FROM SIPR_Fis_Kalem AS SFK
                                                    INNER JOIN STOK_Karti AS SK ON SK.Stok_No = SFK.Stok_No
                                                    WHERE SFK.Fis_Durum = 0 AND SFK.Isyeri_Kodu = 1 AND SFK.Fis_Tipi = 37 AND 
                                                          SFK.Fis_No IN ({sqlParam})", listParamater);

            while (reader.Read())
            {
                storeDetailList.Add(new StoreDetailDto { FisNo = reader["Fis_No"].To<int>(), Stok_Kodu = reader["Stok_Kodu"].ToString(), Adi_1 = reader["Stok_Adi"].ToString(), Miktari = reader["Miktari"].To<decimal>() });
            }

            reader.Close();

            return storeDetailList;
        }

        public ServiceResponse<List<StoreDto>> GetStoreList(int companyId, string firmCode, string term, int pageNo, int listCount, DateTime startDate, DateTime finishDate)
        {

            var counter = listCount * (pageNo - 1);
            var totalcount = 0;

            var storeList = new List<StoreDto>();

            var listParamater = new List<SqlParameter>();

            listParamater.Add(new SqlParameter() { ParameterName = "@Isyeri_Kodu", Value = companyId });
            listParamater.Add(new SqlParameter() { ParameterName = "@Firma_Kodu", Value = firmCode });
            listParamater.Add(new SqlParameter { ParameterName = "@PageNo", Value = pageNo });
            listParamater.Add(new SqlParameter { ParameterName = "@ListCount", Value = listCount });
            listParamater.Add(new SqlParameter() { ParameterName = "@StardDate", Value = startDate });
            listParamater.Add(new SqlParameter() { ParameterName = "@FinishDate", Value = finishDate });


            var reader = _sqlDataService.GetDataList($@"Select * From (SELECT Top (@PageNo * @ListCount) row_number() OVER(ORDER BY B.Fis_No DESC) As RowNew,* FROM (SELECT SFB.Fis_No, DK.ADI AS Depo_Adi --Duruma göre alanları eklersin
                                                             FROM SIPR_Fis_Baslik AS SFB
                                                             INNER JOIN KEY_SYSTEM.dbo.DEPO_Karti AS DK ON SFB.Depo_Kodu = DK.KODU AND DK.FIRMA_KODU = @Firma_Kodu
                                                             WHERE SFB.Fis_Durum = 0 AND SFB.Isyeri_Kodu = @Isyeri_Kodu AND SFB.Fis_Tipi = 37 
                                                             AND SFB.Fis_Tarihi >= @StardDate AND SFB.Fis_Tarihi <= @FinishDate
                                                    ) As B) As A Where A.RowNew BETWEEN ((@PageNo-1)*@ListCount)+1 AND (@PageNo * @ListCount)", listParamater);
            var ider = 1;

            while (reader.Read())
            {
                storeList.Add(new StoreDto { Id = ider++, FisNo = reader["Fis_No"].To<int>(), DepoAdi = reader["Depo_Adi"].ToString() });
            }

            reader.Close();

            listParamater = new List<SqlParameter>();
            listParamater.Add(new SqlParameter() { ParameterName = "@Isyeri_Kodu", Value = companyId });
            listParamater.Add(new SqlParameter() { ParameterName = "@Firma_Kodu", Value = firmCode });
            listParamater.Add(new SqlParameter { ParameterName = "@PageNo", Value = pageNo });
            listParamater.Add(new SqlParameter { ParameterName = "@ListCount", Value = listCount });
            listParamater.Add(new SqlParameter() { ParameterName = "@StardDate", Value = startDate });
            listParamater.Add(new SqlParameter() { ParameterName = "@FinishDate", Value = finishDate });


            reader = _sqlDataService.GetDataList($@"SELECT Count(0) As Count
                                                    FROM SIPR_Fis_Baslik AS SFB
                                                    INNER JOIN KEY_SYSTEM.dbo.DEPO_Karti AS DK ON SFB.Depo_Kodu = DK.KODU AND DK.FIRMA_KODU = @Firma_Kodu
                                                    WHERE SFB.Fis_Durum = 0 AND SFB.Isyeri_Kodu = @Isyeri_Kodu AND SFB.Fis_Tipi = 37 
                                                          AND SFB.Fis_Tarihi >= @StardDate AND SFB.Fis_Tarihi <= @FinishDate", listParamater);

            while (reader.Read())
            { totalcount = reader["Count"].To<int>(); }

            reader.Close();


            return new ServiceResponse<List<StoreDto>> { Data = storeList, DataTotalCount = totalcount, Status = true };
        }

        public bool SaveOrder(int storeCode, List<StoreBasketDto> baskets, LoginCheckRes loginCheckRes)
        {
            var listParamCount = new List<SqlParameter>();


            int newFisNo = 1;
            var reader = _sqlDataService.GetDataList("SELECT ISNULL(MAX(Fis_No), CAST(0 AS INT)) + CAST(1 AS INT) AS NewFisNo FROM SIPR_Fis_Baslik WHERE Fis_Tipi = 37 AND Isyeri_Kodu = " + loginCheckRes.BranchId, listParamCount);
            if (reader.Read())
                newFisNo = reader["NewFisNo"].To<int>();
            reader.Close();

            listParamCount.Add(new SqlParameter { ParameterName = "@Fis_Tipi", Value = 37 });
            listParamCount.Add(new SqlParameter { ParameterName = "@Isyeri_Kodu", Value = loginCheckRes.BranchId });
            listParamCount.Add(new SqlParameter { ParameterName = "@Fis_No", Value = newFisNo });
            listParamCount.Add(new SqlParameter { ParameterName = "@Fis_Tarihi", Value = DateTime.Now });
            listParamCount.Add(new SqlParameter { ParameterName = "@Depo_Kodu", Value = storeCode });
            listParamCount.Add(new SqlParameter { ParameterName = "@Cari_No", Value = loginCheckRes.FirmId });
            listParamCount.Add(new SqlParameter { ParameterName = "@Belge_Tarihi", Value = DateTime.Now.Date });
            listParamCount.Add(new SqlParameter { ParameterName = "@Belge_Tipi", Value = 37 });
            listParamCount.Add(new SqlParameter { ParameterName = "@Belge_FisNo", Value = newFisNo });
            listParamCount.Add(new SqlParameter { ParameterName = "@Teslim_Tarihi", Value = DateTime.Now });
            listParamCount.Add(new SqlParameter { ParameterName = "@Kdv_Tipi1", Value = 1 });
            listParamCount.Add(new SqlParameter { ParameterName = "@Kdv_Tipi2", Value = 1 });
            listParamCount.Add(new SqlParameter { ParameterName = "@Para_Birimi", Value = "TL" });
            listParamCount.Add(new SqlParameter { ParameterName = "@Fiyat_Tipi", Value = "AFYT" }); //Değişebilir
            listParamCount.Add(new SqlParameter { ParameterName = "@Kur_Degeri", Value = 1 });
            listParamCount.Add(new SqlParameter { ParameterName = "@TopDeger1", Value = baskets.Sum(p => (p.Quantity * p.Price) / (1 + (p.Tax / 100))) }); //Değişebilir
            listParamCount.Add(new SqlParameter { ParameterName = "@TopDeger2", Value = baskets.Sum(p => (p.Quantity * p.Price) / (1 + (p.Tax / 100))) }); //Değişebilir
            listParamCount.Add(new SqlParameter { ParameterName = "@ToplamKdv", Value = baskets.Sum(p => (p.Quantity * p.Price) - ((p.Quantity * p.Price) / (1 + (p.Tax / 100)))) }); //Değişebilir
            listParamCount.Add(new SqlParameter { ParameterName = "@NetTutar1", Value = baskets.Sum(p => p.Quantity * p.Price) }); //Değişebilir
            listParamCount.Add(new SqlParameter { ParameterName = "@NetTutar2", Value = baskets.Sum(p => p.Quantity * p.Price) }); //Değişebilir
            listParamCount.Add(new SqlParameter { ParameterName = "@Sip_Depo", Value = 1 }); //sipariş depo default yapılacak
            listParamCount.Add(new SqlParameter { ParameterName = "@Odeme_Plani", Value = "WEB" });
            listParamCount.Add(new SqlParameter { ParameterName = "@Acilis_User", Value = loginCheckRes.KeysoftUserId }); // loginCheckRes.UserId + "_" + kaldırıldı Uzun geliyor varchar(10)
            listParamCount.Add(new SqlParameter { ParameterName = "@Acilis_Zaman", Value = DateTime.Now });
            listParamCount.Add(new SqlParameter { ParameterName = "@Adres_SiraNo", Value = loginCheckRes.FirmAddressId });
            _sqlDataService.GetExecute(@"INSERT INTO SIPR_Fis_Baslik (Fis_Tipi, Isyeri_Kodu, Fis_No, Fis_Tarihi, Depo_Kodu, Cari_No, Belge_Tarihi, Belge_Tipi, Belge_FisNo, Teslim_Tarihi, Kdv_Tipi1, Kdv_Tipi2, Para_Birimi, Fiyat_Tipi, Kur_Degeri, TopDeger1, TopDeger2, ToplamKdv, NetTutar1, NetTutar2, Odeme_Plani, Acilis_User, Acilis_Zaman, Adres_SiraNo) " +
                                        "VALUES (@Fis_Tipi, @Isyeri_Kodu, @Fis_No, @Fis_Tarihi, @Depo_Kodu, @Cari_No, @Belge_Tarihi, @Belge_Tipi, @Belge_FisNo, @Teslim_Tarihi, @Kdv_Tipi1, @Kdv_Tipi2, @Para_Birimi, @Fiyat_Tipi, @Kur_Degeri, @TopDeger1, @TopDeger2, @ToplamKdv, @NetTutar1, @NetTutar2, @Odeme_Plani, @Acilis_User, @Acilis_Zaman, @Adres_SiraNo)", listParamCount);

            for (int i = 0; i < baskets.Count; i++)
            {
                listParamCount.Clear();
                listParamCount.Add(new SqlParameter { ParameterName = "@Fis_Tipi", Value = 37 });
                listParamCount.Add(new SqlParameter { ParameterName = "@Isyeri_Kodu", Value = loginCheckRes.BranchId });
                listParamCount.Add(new SqlParameter { ParameterName = "@Fis_No", Value = newFisNo });
                listParamCount.Add(new SqlParameter { ParameterName = "@Fis_Sira", Value = i + 1 });
                listParamCount.Add(new SqlParameter { ParameterName = "@Fis_Tarihi", Value = DateTime.Now });
                listParamCount.Add(new SqlParameter { ParameterName = "@Islem_Depo", Value = storeCode }); //Değişebilir
                listParamCount.Add(new SqlParameter { ParameterName = "@Stok_No", Value = baskets[i].ProductId });
                listParamCount.Add(new SqlParameter { ParameterName = "@Stok_Kodu", Value = baskets[i].ProductCode });
                listParamCount.Add(new SqlParameter { ParameterName = "@Birim_Tipi", Value = baskets[i].ProductUnit });
                listParamCount.Add(new SqlParameter { ParameterName = "@Birim_Orani", Value = baskets[i].ProductUnitRate });
                listParamCount.Add(new SqlParameter { ParameterName = "@Miktari", Value = baskets[i].Quantity });
                listParamCount.Add(new SqlParameter { ParameterName = "@Para_Birimi", Value = "TL" });
                listParamCount.Add(new SqlParameter { ParameterName = "@Birim_Fiyati", Value = baskets[i].Price });
                listParamCount.Add(new SqlParameter { ParameterName = "@KDV_Tipi", Value = 1 });
                listParamCount.Add(new SqlParameter { ParameterName = "@KDV_Orani", Value = baskets[i].Tax });
                listParamCount.Add(new SqlParameter { ParameterName = "@Tutari", Value = baskets[i].Quantity * baskets[i].Price });
                listParamCount.Add(new SqlParameter { ParameterName = "@Tutari1", Value = baskets[i].Quantity * baskets[i].Price });
                listParamCount.Add(new SqlParameter { ParameterName = "@Tutari2", Value = baskets[i].Quantity * baskets[i].Price });
                listParamCount.Add(new SqlParameter { ParameterName = "@TutariP", Value = baskets[i].Quantity * baskets[i].Price });
                listParamCount.Add(new SqlParameter { ParameterName = "@TesTarihi", Value = DateTime.Now });
                listParamCount.Add(new SqlParameter { ParameterName = "@Kur_DegeriP", Value = 1 });
                listParamCount.Add(new SqlParameter { ParameterName = "@BMaliyet", Value = 0 }); //DİKKAT! Değişebilir
                _sqlDataService.GetExecute(@"INSERT INTO SIPR_Fis_Kalem (Fis_Tipi, Isyeri_Kodu, Fis_No, Fis_Sira, Fis_Tarihi, Islem_Depo, Stok_No, Stok_Kodu, Birim_Tipi, Birim_Orani, Miktari, Para_Birimi, Birim_Fiyati, KDV_Tipi, KDV_Orani, Tutari, Tutari1, Tutari2, TutariP, TesTarihi, Kur_DegeriP, BMaliyet) " +
                                            "VALUES (@Fis_Tipi, @Isyeri_Kodu, @Fis_No, @Fis_Sira, @Fis_Tarihi, @Islem_Depo, @Stok_No, @Stok_Kodu, @Birim_Tipi, @Birim_Orani, @Miktari, @Para_Birimi, @Birim_Fiyati, @KDV_Tipi, @KDV_Orani, @Tutari, @Tutari1, @Tutari2, @TutariP, @TesTarihi, @Kur_DegeriP, @BMaliyet)", listParamCount);
            }

            return true;
        }
    }
}
