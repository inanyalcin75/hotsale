﻿using HotSale.Dto;
using HotSale.Dto.Helper;
using HotSale.Dto.Operations;
using HotSale.Dto.Operations.Store;
using System;
using System.Collections.Generic;
using System.Text;

namespace HotSale.Service.StoreService
{
    public interface IStoreService
    {
        ServiceResponse<List<StoreDto>> GetStoreList(int companyId, string firmCode, string term, int pageNo, int listCount, DateTime startDate, DateTime finishDate);
        List<StoreDetailDto> GetStoreDetailList(int[] storeIds);
        bool SaveOrder(int storeCode, List<StoreBasketDto> baskets, LoginCheckRes loginCheckRes);
    }
}
