﻿using HotSale.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace HotSale.Service.AccountService
{
    public interface IAccountService
    {
        List<AccountCardDto> GetAccountList(bool cache = true);
    }
}
