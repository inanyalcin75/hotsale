﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using HotSale.Cache;
using HotSale.Dto;
using HotSale.Infrastructure.Extension;
using HotSale.Service.SqlDataService;

namespace HotSale.Service.AccountService
{
    public class AccountService : IAccountService
    {
        private readonly ISqlDataService _sqlDataService;
        private readonly ICacheProvider _cacheProvider;

        //========================================================================================================================
        // Constructor
        //========================================================================================================================
        public AccountService(ISqlDataService sqlDataService, ICacheProvider cacheProvider)
        {
            //Injection
            _sqlDataService = sqlDataService;
            _cacheProvider = cacheProvider;
        }


        public List<AccountCardDto> GetAccountList(bool cache = true)
        {
            var list = new List<AccountCardDto>();

            list = _cacheProvider.Get<List<AccountCardDto>>(enmCacheNames.AccountCardList);


            if (list == null)
            {
                list = new List<AccountCardDto>();

                var readers = _sqlDataService.GetDataList("SELECT Cari_No,Cari_Kodu,Unvani FROM CARI_Karti WHERE IptDrm = 0", new List<SqlParameter>());

                while (readers.Read())
                {
                    list.Add(new AccountCardDto { AccountCardCode = readers["Cari_Kodu"].ToString(), AccountCardNo = readers["Cari_No"].To<int>(), AccountCardTitle = readers["Unvani"].ToString() });
                }
                readers.Close();

                _cacheProvider.Set<List<AccountCardDto>>(enmCacheNames.StoreBasketList, list);
            }

            return list;
        }
    }
}
