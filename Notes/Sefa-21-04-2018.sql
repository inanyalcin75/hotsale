--DEPO
--Bu raporda normal �artlarda tarih kullan�lmaz ama �lker Abi de zaten normal bir insan de�il
SELECT SK.Adi_1 AS Stok_Adi, SUM(TSGT.GMiktari - TSGT.CMiktari) AS Kalan
FROM T_Stok_GunlukToplam AS TSGT 
INNER JOIN STOK_Karti AS SK ON TSGT.Stok_No = SK.Stok_No AND SK.IptDrm = 0
WHERE TSGT.Islem_Depo IN (SELECT KODU FROM KEY_SYSTEM.dbo.DEPO_Karti WHERE ISYERI_KODU = 1 AND FIRMA_KODU = 'KERKELI16') AND SK.Ozel_Kodu3 = 'HAMMADDE' --AND TSGT.Tarih >= ''
GROUP BY SK.Adi_1

GO

--DEPO GRUP
--Tarih olay� ayn�
SELECT SK.Ozel_Kodu3, SUM(TSGT.GMiktari - TSGT.CMiktari) AS Kalan
FROM T_Stok_GunlukToplam AS TSGT 
INNER JOIN STOK_Karti AS SK ON TSGT.Stok_No = SK.Stok_No AND SK.IptDrm = 0
WHERE TSGT.Islem_Depo IN (SELECT KODU FROM KEY_SYSTEM.dbo.DEPO_Karti WHERE ISYERI_KODU = 1 AND FIRMA_KODU = 'KERKELI16') AND SK.Ozel_Kodu3 = 'HAMMADDE' --AND TSGT.Tarih >= ''
GROUP BY SK.Ozel_Kodu3
----------------------------------------------------------------------------------------------------

--�RET�M
SELECT SK.Adi_1 AS Stok_Adi, SUM(UIEPD.Miktari) AS Uretim_Miktari, UR.Birimi
FROM URET_IsEmri_Plan AS UIEP
INNER JOIN URET_IsEmri_Plan_Detay AS UIEPD ON UIEP.IsYeri_Kodu = UIEPD.IsYeri_Kodu AND UIEP.Fis_Tipi = UIEPD.Fis_Tipi AND UIEP.Fis_No = UIEPD.Fis_No
INNER JOIN URET_Recete AS UR ON UIEPD.Recete_Kodu = UR.Recete_Kodu
INNER JOIN STOK_Karti AS SK ON UR.Stok_No = SK.Stok_No AND SK.IptDrm = 0
WHERE UIEP.Fis_Durum = 0 AND UIEP.IsYeri_Kodu = 1 AND SK.Ozel_Kodu3 = 'HAMMADDE' AND UIEP.Fis_Tarihi >= '2016.01.01 00:00:00' AND UIEP.Fis_Tarihi <= '2018.04.21 23:59:59'
GROUP BY SK.Adi_1, UR.Birimi

--�RET�M GRUP
SELECT SK.Ozel_Kodu3, SUM(UIEPD.Miktari) AS Uretim_Miktari, UR.Birimi
FROM URET_IsEmri_Plan AS UIEP
INNER JOIN URET_IsEmri_Plan_Detay AS UIEPD ON UIEP.IsYeri_Kodu = UIEPD.IsYeri_Kodu AND UIEP.Fis_Tipi = UIEPD.Fis_Tipi AND UIEP.Fis_No = UIEPD.Fis_No
INNER JOIN URET_Recete AS UR ON UIEPD.Recete_Kodu = UR.Recete_Kodu
INNER JOIN STOK_Karti AS SK ON UR.Stok_No = SK.Stok_No AND SK.IptDrm = 0
WHERE UIEP.Fis_Durum = 0 AND UIEP.IsYeri_Kodu = 1 AND SK.Ozel_Kodu3 = 'HAMMADDE' AND UIEP.Fis_Tarihi >= '2016.01.01 00:00:00' AND UIEP.Fis_Tarihi <= '2018.04.21 23:59:59'
GROUP BY SK.Ozel_Kodu3, UR.Birimi
----------------------------------------------------------------------------------------------------

--KASA DEV�R
SELECT SUM(KFK.BorcTutari - KFK.AlckTutari) AS Bakiye
FROM KASA_Fis_Kalem AS KFK 
WHERE KFK.Fis_Durum = 0 AND KFK.Isyeri_Kodu = 1 AND KFK.Fis_Tarihi < '2017.01.01 00:00:00'

--KASA DURUMU
SELECT KFK.Kasa_Kodu + ' - ' + KFK.Aciklama AS Aciklama, CASE KFK.Fis_Tipi WHEN 10 THEN 'Tahsil' WHEN 11 THEN 'Tediye' ELSE KFK.Fis_Tipi END AS Fis_Tipi, (CASE WHEN KFK.BorcTutari > 0 THEN 'B' ELSE 'A' END) AS Tip, ABS(KFK.BorcTutari - KFK.AlckTutari) AS Tutar
FROM KASA_Fis_Kalem AS KFK 
INNER JOIN KASA_Karti AS KK ON KFK.Kasa_Kodu = KK.Kasa_Kodu
WHERE KFK.Fis_Durum = 0 AND KFK.Isyeri_Kodu = 1 AND KFK.Fis_Tarihi >= '2017.01.01 00:00:00' AND KFK.Fis_Tarihi <= '2018.04.21 23:59:59'

----------------------------------------------------------------------------------------------------

--CAR� DEV�R
SELECT SUM(CFK.BorcTutari - CFK.AlckTutari) AS Bakiye
FROM CARI_Fis_Kalem AS CFK 
WHERE CFK.Fis_Durum = 0 AND CFK.Isyeri_Kodu = 1 AND CFK.Fis_Tarihi < '2017.01.01 00:00:00'

--CAR� DURUMU
SELECT CFK.Cari_Kodu + ' - ' + CFK.Aciklama AS Aciklama, CASE CFK.Fis_Tipi WHEN 1 THEN 'Bor�' WHEN 2 THEN 'Alacak' WHEN 3 THEN 'Virman' WHEN 4 THEN 'Devir' ELSE CFK.Fis_Tipi END AS Fis_Tipi, (CASE WHEN CFK.BorcTutari > 0 THEN 'B' ELSE 'A' END) AS Tip, ABS(CFK.BorcTutari - CFK.AlckTutari) AS Tutar
FROM CARI_Fis_Kalem AS CFK 
INNER JOIN CARI_Karti AS CK ON CFK.Cari_No = CK.Cari_No
WHERE CFK.Fis_Durum = 0 AND CFK.Isyeri_Kodu = 1 AND CFK.Fis_Tarihi >= '2017.01.01 00:00:00' AND CFK.Fis_Tarihi <= '2018.04.21 23:59:59'

----------------------------------------------------------------------------------------------------

--BANK DEV�R
SELECT SUM(BFK.BorcTutari - BFK.AlckTutari) AS Bakiye
FROM BANK_Fis_Kalem AS BFK 
WHERE BFK.Fis_Durum = 0 AND BFK.Isyeri_Kodu = 1 AND BFK.Fis_Tarihi < '2017.01.01 00:00:00'

--BANK DURUMU
SELECT BFK.Banka_Kodu + ' - ' + BFK.Aciklama AS Aciklama, CASE BFK.Fis_Tipi WHEN 12 THEN 'Para �ekme' WHEN 13 THEN 'Para Yat�rma' WHEN 14 THEN 'Gelen Havale' WHEN 15 THEN 'Yap�lan Havale' WHEN 16 THEN 'Virman' WHEN 18 THEN 'Masraf' ELSE BFK.Fis_Tipi END AS Fis_Tipi, (CASE WHEN BFK.BorcTutari > 0 THEN 'B' ELSE 'A' END) AS Tip, ABS(BFK.BorcTutari - BFK.AlckTutari) AS Tutar
FROM BANK_Fis_Kalem AS BFK 
INNER JOIN BANK_Karti AS BK ON BFK.Banka_Kodu = BK.Banka_Kodu
WHERE BFK.Fis_Durum = 0 AND BFK.Isyeri_Kodu = 1 AND BFK.Fis_Tarihi >= '2017.01.01 00:00:00' AND BFK.Fis_Tarihi <= '2018.04.21 23:59:59'

----------------------------------------------------------------------------------------------------

--�EK SENET �DEME
SELECT CFK.CS_No + ' - ' + CK.Unvani AS Aciklama,CASE WHEN CFK.Fis_Tipi = 80 THEN '�ek' ELSE 'Senet' END As Fis_Tipi ,CFK.Tutari
FROM CKSN_Fis_Kalem AS CFK
INNER JOIN CKSN_Fis_Baslik AS CFB ON CFK.Fis_Tipi = CFB.Fis_Tipi AND CFK.Isyeri_Kodu = CFB.Isyeri_Kodu AND CFK.Fis_No = CFB.Fis_No AND CFB.Fis_Durum = 0
INNER JOIN CARI_Karti AS CK ON CK.Cari_No = CFB.Cari_No
WHERE CFK.Fis_Durum = 0 AND CFK.Fis_Tipi IN (80, 81) AND CFK.Isyeri_Kodu = 1 AND CFK.Bordro_Tarihi >= '2017.01.01 00:00:00' AND CFK.Bordro_Tarihi <= '2018.04.21 23:59:59'
----------------------------------------------------------------------------------------------------

--�EK SENET TAHS�LAT
SELECT CFK.CS_No + ' - ' + CK.Unvani AS Aciklama,CASE WHEN CFK.Fis_Tipi = 70 THEN '�ek' ELSE 'Senet' END As Fis_Tipi ,CFK.Tutari
FROM CKSN_Fis_Kalem AS CFK
INNER JOIN CKSN_Fis_Baslik AS CFB ON CFK.Fis_Tipi = CFB.Fis_Tipi AND CFK.Isyeri_Kodu = CFB.Isyeri_Kodu AND CFK.Fis_No = CFB.Fis_No AND CFB.Fis_Durum = 0
INNER JOIN CARI_Karti AS CK ON CK.Cari_No = CFB.Cari_No
WHERE CFK.Fis_Durum = 0 AND CFK.Fis_Tipi IN (70, 71) AND CFK.Isyeri_Kodu = 1 AND CFK.Bordro_Tarihi >= '2017.01.01 00:00:00' AND CFK.Bordro_Tarihi <= '2018.04.21 23:59:59'
----------------------------------------------------------------------------------------------------

--DEPO S�PAR�� L�STES�
SELECT SFB.Fis_No, DK.ADI AS Depo_Adi --Duruma g�re alanlar� eklersin
FROM SIPR_Fis_Baslik AS SFB
INNER JOIN KEY_SYSTEM.dbo.DEPO_Karti AS DK ON SFB.Depo_Kodu = DK.KODU AND DK.FIRMA_KODU = 'KERKELI16'
WHERE SFB.Fis_Durum = 0 AND SFB.Isyeri_Kodu = 1 AND SFB.Fis_Tipi = 37 AND SFB.Fis_Tarihi >= '2017.01.01 00:00:00' AND SFB.Fis_Tarihi <= '2018.04.21 23:59:59'

SELECT SFK.Fis_No, SK.Stok_Kodu, SK.Adi_1 AS Stok_Adi, SFK.Miktari
FROM SIPR_Fis_Kalem AS SFK
INNER JOIN STOK_Karti AS SK ON SK.Stok_No = SFK.Stok_No
WHERE SFK.Fis_Durum = 0 AND SFK.Isyeri_Kodu = 1 AND SFK.Fis_Tipi = 37 AND SFK.Fis_No IN (1, 2, 3 /*YAZ BABAM YAZ*/)
----------------------------------------------------------------------------------------------------


--SATI� S�PAR�� L�STES�
SELECT SFB.Fis_No, CK.Unvani, SFB.NetTutar1 --Duruma g�re alanlar� eklersin
FROM SIPR_Fis_Baslik AS SFB
INNER JOIN CARI_Karti AS CK ON SFB.Cari_No = CK.Cari_No AND CK.IptDrm = 0
WHERE SFB.Fis_Durum = 0 AND SFB.Isyeri_Kodu = 1 AND SFB.Fis_Tipi = 36 AND SFB.Fis_Tarihi >= '2017.01.01 00:00:00' AND SFB.Fis_Tarihi <= '2018.04.21 23:59:59'

SELECT SFK.Fis_No, SK.Stok_Kodu, SK.Adi_1 AS Stok_Adi, SFK.Miktari, SFK.Tutari
FROM SIPR_Fis_Kalem AS SFK
INNER JOIN STOK_Karti AS SK ON SK.Stok_No = SFK.Stok_No
WHERE SFK.Fis_Durum = 0 AND SFK.Isyeri_Kodu = 1 AND SFK.Fis_Tipi = 36 AND SFK.Fis_No IN (1, 2, 3 /*YAZ BABAM YAZ*/)
----------------------------------------------------------------------------------------------------