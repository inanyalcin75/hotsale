﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HotSale.Businesss.LoginWork;
using HotSale.Helper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace HotSale.Controllers
{
    public class LoginController : Controller
    {

        private readonly ILoginWork _loginWork;

        //========================================================================================================================
        // Constructor
        //========================================================================================================================
        public LoginController(ILoginWork loginWork)
        {
            //Login Injection
            _loginWork = loginWork;
        }

        public IActionResult Index()
        {
            return View();
        }

        //Post: Login Check
        public JsonResult LoginCheck(string email, string pass)
        {
            var _login = _loginWork.LoginCheck(email, pass);

            if (_login.Status == true)
            {
                WebSession.LoginAdd(HttpContext.Session, _login.Data);
            }

            return Json(_login);
        }

        //Get: Login Out
        [HttpGet]
        public ActionResult LoginOut()
        {
            WebSession.LoginOut(HttpContext.Session);
            return Redirect("/Login/Index");
        }

    }



}