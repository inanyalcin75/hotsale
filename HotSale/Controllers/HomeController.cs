﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using HotSale.Businesss.DashboardWork;
using HotSale.Cache;
using HotSale.Dto.Helper;
using HotSale.Dto.Operations;
using HotSale.Helper;
using HotSale.Infrastructure.Extension;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;

namespace HotSale.Controllers
{
    public class HomeController : BaseController
    {
        private readonly IDashboardWork _dashboardWork;
        private readonly IConfiguration _configuration;
        private readonly ICacheProvider _cacheProvide;

        //========================================================================================================================
        // Constructor
        //========================================================================================================================
        public HomeController(IDashboardWork dashboardWork, IConfiguration configuration, ICacheProvider cacheProvide)
        {
            //Login Injection
            _dashboardWork = dashboardWork;
            _configuration = configuration;
            _cacheProvide = cacheProvide;
        }


        public IActionResult Index()
        {

            var control = WebSession.LoginControl(this.HttpContext.Session);

            var firstDayOfMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);

            var dashboard = _dashboardWork.Get(null, null, null, control.FirmCode, firstDayOfMonth, lastDayOfMonth).Data;
            var dashboard2 = new DashboardRes() { CompanyDto = new List<Dto.CompanyDto>(), CompanySectionsDto = new List<CompanySectionDto>(), CompanyTypeDto = new List<CompanyTypeDto>(), ProductGroupsReportDto = new List<ProductGroupsReportDto>(), ProductsSaleReportDto = new List<ProductsSaleReportDto>() };
            return View(dashboard);
        }

        public IActionResult GetView(string viewName, string companyId, string companySectionId, string companyTypeId, DateTime startDate, DateTime finishDate)
        {
            if (companyId == "0")
                companyId = null;

            var control = WebSession.LoginControl(this.HttpContext.Session);
            ViewBag.NoLayout = false;
            var dashboard = _dashboardWork.Get(companyId.To<int?>(), companySectionId.To<int?>(), companyTypeId, control.FirmCode, startDate, finishDate).Data;
            return PartialView("~/Views/Home/Index.cshtml", dashboard);
        }

    }
}
