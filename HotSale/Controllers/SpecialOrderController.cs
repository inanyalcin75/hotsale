﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HotSale.Businesss.OrderWork;
using HotSale.Dto.Operations.SpecialOrder;
using HotSale.Helper;
using HotSale.Infrastructure.Extension;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace HotSale.Controllers
{
    public class SpecialOrderController : Controller
    {
        private readonly ISpecialOrderWork _specialOrderWork;
        private readonly IConfiguration _configuration;

        //========================================================================================================================
        // Constructor
        //========================================================================================================================
        public SpecialOrderController(ISpecialOrderWork specialOrderWork, IConfiguration configuration)
        {
            //Injection
            _specialOrderWork = specialOrderWork;
            _configuration = configuration;
        }

        public IActionResult Index()
        {
            var control = WebSession.LoginControl(this.HttpContext.Session);
            var model = _specialOrderWork.GetSpacialOrder(control.BranchId).Data;

            return View(model);
        }

        public JsonResult SaveSpecialOrders(string StockCode, string StockDetail, string Description, IFormFile Filer, string DeliveryAddress, string DeliveryDate, string DeliveryTime)
        {

            //Business fature add
            var control = WebSession.LoginControl(this.HttpContext.Session);
            var depoKodu = _configuration.GetSection("AppParamConfig")["CompanyOrderDefaultCode"].To<int>(); //Appconfigten çekilecek

            var order = _specialOrderWork.SetOrderSave(control.BranchId, depoKodu, StockCode, StockDetail, Description, Filer?.OpenReadStream(), DeliveryAddress, DeliveryDate, DeliveryTime);

            return new JsonResult(order);
        }


        public JsonResult GetStock(string search)
        {
            return new JsonResult(_specialOrderWork.GetStock(search).Data);
        }



    }
}