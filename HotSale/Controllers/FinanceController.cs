﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HotSale.Businesss.FinancialWork;
using HotSale.Helper;
using Microsoft.AspNetCore.Mvc;

namespace HotSale.Controllers
{
    public class FinanceController : BaseController
    {

        private readonly IFinancialWork _financialWork;

        //========================================================================================================================
        // Constructor
        //========================================================================================================================
        public FinanceController(IFinancialWork financialWork)
        {
            //Injection
            _financialWork = financialWork;

        }

        public IActionResult Bill(string term, int pageNo, int listCount, DateTime startDate, DateTime finishDate)
        {
            var control = WebSession.LoginControl(this.HttpContext.Session);

            var model = _financialWork.GetBillList(control.BranchId, term, pageNo, listCount, startDate, finishDate);

            return View(model);
        }

        public IActionResult WayBill()
        {
            string term = null;
            var startDate = DateTime.Now.AddYears(-1);
            var finishDate = DateTime.Now.AddDays(1);
            var pageNo = 1;
            var listCount = 50;

            var control = WebSession.LoginControl(this.HttpContext.Session);

            var model = _financialWork.GetWayBillList(control.BranchId, control.FirmId, control.IsCustomer, term, pageNo, listCount, startDate, finishDate);

            return View(model);
        }


        public IActionResult WayBillAjax(string term, int pageNo, int listCount, DateTime startDate, DateTime finishDate)
        {
            var control = WebSession.LoginControl(this.HttpContext.Session);

            var model = _financialWork.GetWayBillList(control.BranchId, control.FirmId, control.IsCustomer, term, pageNo, listCount, startDate, finishDate);

            return PartialView("~/Views/Finance/Partial/WayBillSub.cshtml", model);
        }

        public IActionResult CurrentAccount()
        {

            var startDate = DateTime.Now.AddYears(-1);
            var finishDate = DateTime.Now.AddDays(1);
            var pageNo = 1;
            var listCount = 50;
            var control = WebSession.LoginControl(this.HttpContext.Session);

            var model = _financialWork.GetCurrentAccountList(control.FirmId, control.IsCustomer, pageNo, listCount, startDate, finishDate).Data;

            return View(model);
        }

        public IActionResult CurrentAccountAjax(string term, int pageNo, int listCount, DateTime startDate, DateTime finishDate)
        {
            var control = WebSession.LoginControl(this.HttpContext.Session);

            var model = _financialWork.GetCurrentAccountList(control.FirmId, control.IsCustomer, pageNo, listCount, startDate, finishDate).Data;

            return PartialView("~/Views/Finance/Partial/CurrentAccountSub.cshtml", model);
        }
    }
}