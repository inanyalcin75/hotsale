﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HotSale.Dto.Operations;
using HotSale.Helper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;

namespace HotSale.Controllers
{
    [ServiceFilter(typeof(CustomAuthorizationAttribute))]
    public class BaseController : Controller
    {
    }

    //========================================================================================================================
    // Business Logic
    //========================================================================================================================

    public class CustomAuthorizationAttribute : ActionFilterAttribute
    {
        //private readonly IMyDepedency _dp;
        public CustomAuthorizationAttribute(/*IMyDepedency dp*/)
        {
            //_dp = dp;
        }
        public override void OnActionExecuting(ActionExecutingContext context)
        {

            var control = WebSession.LoginControl(context.HttpContext.Session);

            //write my validation and authorization logic here 
            if (control == null)
            {
                //var unauthResult = new UnauthorizedResult();
                //context.Result = unauthResult;
                context.Result = new RedirectResult("/Login/Index");
            }
            else
            {
                base.OnActionExecuting(context);
            }
        }
    }

}