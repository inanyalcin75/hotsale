﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HotSale.Businesss.StoreWork;
using HotSale.Helper;
using HotSale.Infrastructure.Extension;
using HotSale.Service.CompanyService;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace HotSale.Controllers
{
    public class StoreController : BaseController
    {

        private readonly IStoreWork _storeWork;
        private readonly ICompanyService _companyService;
        private readonly IConfiguration _configuration;

        //========================================================================================================================
        // Constructor
        //========================================================================================================================
        public StoreController(IStoreWork storeWork, ICompanyService companyService, IConfiguration configuration)
        {
            //Injection
            _storeWork = storeWork;
            _companyService = companyService;
            _configuration = configuration;
        }

        public IActionResult Index()
        {
            var control = WebSession.LoginControl(this.HttpContext.Session);
            var modelOrder = _storeWork.GetStoreProductList(null, 1, 8);

            return View(modelOrder);
        }


        public IActionResult GetStoreProducts(string term, int pageNo, int listCount)
        {
            var control = WebSession.LoginControl(this.HttpContext.Session);
            var modelOrder = _storeWork.GetStoreProductList(term, pageNo.To<int>(), listCount);
            modelOrder.Term = term;
            return PartialView("~/Views/Store/Partial/StoreProducts.cshtml", modelOrder);
        }



        public IActionResult GetStores(string term, int pageNo, int listCount, DateTime startDate, DateTime finishDate)
        {
            var control = WebSession.LoginControl(this.HttpContext.Session);

            var modelStore = _storeWork.GetStoreList(control.BranchId, control.FirmCode, term, pageNo, listCount, startDate, finishDate);
            modelStore.StartDate = startDate;
            modelStore.FinishDate = finishDate;
            modelStore.Term = term;
            return PartialView("~/Views/Store/Partial/Stores.cshtml", modelStore);
        }

        public IActionResult GetCacheStores()
        {
            var control = WebSession.LoginControl(this.HttpContext.Session);

            ViewBag.CompanyStores = _companyService.GetCompanyStoreList(true, control.BranchId, control.FirmCode);
            ViewBag.CompanyStoresDefaultCode = _configuration.GetSection("AppParamConfig")["CompanyStoresDefaultCode"].To<int>(); //Appconfigten çekilecek

            var order = _storeWork.GetCacheStoreList(control.UserId);
            return PartialView("~/Views/Store/Partial/StoreCreate.cshtml", order.Data);
        }


        public JsonResult SaveStores(int storeCode)
        {
            //Business fature add
            var control = WebSession.LoginControl(this.HttpContext.Session);
            var order = _storeWork.SetStoreSave(storeCode, control);
            return new JsonResult(order);
        }


        public JsonResult AddStores(int productId, string productCode, string productName, string price, string quantity, string tax, string productUnit, decimal productUnitRate)
        {
            var control = WebSession.LoginControl(this.HttpContext.Session);
            var order = _storeWork.SetStoreAdd(control.UserId, productId, productCode, productName, price.To<decimal>(), quantity.To<decimal>(), tax.To<decimal>(), productUnit, productUnitRate.To<decimal>());
            return new JsonResult(order);
        }

        public JsonResult DeleteCacheStoresProduct(int productId)
        {
            var control = WebSession.LoginControl(this.HttpContext.Session);
            var order = _storeWork.SetStoreRemove(control.UserId, productId);
            return new JsonResult(order);
        }


        public JsonResult DeleteCacheStoresProductAll()
        {
            var control = WebSession.LoginControl(this.HttpContext.Session);
            var order = _storeWork.SetStoreRemoveAll(control.UserId);
            return new JsonResult(order);
        }



    }
}