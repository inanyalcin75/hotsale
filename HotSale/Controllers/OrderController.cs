﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using HotSale.Businesss.OrderWork;
using HotSale.Helper;
using HotSale.Infrastructure.Extension;
using HotSale.Service.AccountService;
using HotSale.Service.CompanyService;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace HotSale.Controllers
{
    public class OrderController : BaseController
    {
        private readonly IOrderWork _orderWork;
        private IHostingEnvironment _hostingEnvironment;
        private readonly ICompanyService _companyService;
        private readonly IAccountService _accountService;
        private readonly IConfiguration _configuration;

        //========================================================================================================================
        // Constructor
        //========================================================================================================================
        public OrderController(IOrderWork orderWork, IHostingEnvironment environment, ICompanyService companyService, IAccountService accountService, IConfiguration configuration)
        {
            //Injection
            _orderWork = orderWork; ;
            _hostingEnvironment = environment;
            _companyService = companyService;
            _accountService = accountService;
            _configuration = configuration;

        }

        public IActionResult Index()
        {
            var control = WebSession.LoginControl(this.HttpContext.Session);
            var modelOrder = _orderWork.GetProductGroupList(null, control.FirmId, null, 1, 40);

            return View(modelOrder.Data);
        }


        public IActionResult GetOrderProducts(string productGroupId, string term, int pageNo, int listCount)
        {
            var control = WebSession.LoginControl(this.HttpContext.Session);
            var modelOrder = _orderWork.GetProductGroupList(productGroupId, control.FirmId, term, pageNo.To<int>(), listCount);
            return PartialView("~/Views/Order/Partial/OrderProducts.cshtml", modelOrder.Data);
        }

        public IActionResult GetOrderProductsItem(string productGroupId, string term, int pageNo, int listCount)
        {
            var control = WebSession.LoginControl(this.HttpContext.Session);
            var modelOrder = _orderWork.GetProductGroupList(productGroupId, control.FirmId, term, pageNo.To<int>(), listCount);
            return PartialView("~/Views/Order/Partial/OrderProductsItem.cshtml", modelOrder.Data);
        }


        public IActionResult GetOrders(string term, int pageNo, int listCount, DateTime startDate, DateTime finishDate)
        {
            var control = WebSession.LoginControl(this.HttpContext.Session);

            var modelOrder = _orderWork.GetOrderList(control.BranchId, control.FirmId, control.IsCustomer, term, pageNo, listCount, startDate, finishDate);
            modelOrder.StartDate = startDate;
            modelOrder.FinishDate = finishDate;
            modelOrder.Term = term;
            return PartialView("~/Views/Order/Partial/Orders.cshtml", modelOrder);
        }

        public IActionResult GetCacheOrders()
        {
            var control = WebSession.LoginControl(this.HttpContext.Session);

            ViewBag.CompanyStores = _companyService.GetCompanyStoreList(true, control.BranchId, control.FirmCode);
            ViewBag.CompanyOrderDefaultCode = _configuration.GetSection("AppParamConfig")["CompanyOrderDefaultCode"].To<int>(); //Appconfigten çekilecek
            ViewBag.AccountCustomers = _accountService.GetAccountList(true);

            var order = _orderWork.GetCacheOrderList(control.UserId);
            return PartialView("~/Views/Order/Partial/OrderCreate.cshtml", order.Data);
        }


        public async Task<JsonResult> SaveImage(IFormFile file, int stockNo)
        {
            if (file.Length > 0)
            {

                var filePath = Path.Combine(_hostingEnvironment.WebRootPath + "/images/stock/" + stockNo, stockNo + ".png");

                if (!Directory.Exists(_hostingEnvironment.WebRootPath + "/images/stock/" + stockNo))
                    Directory.CreateDirectory(_hostingEnvironment.WebRootPath + "/images/stock/" + stockNo);

                using (var fileStream = new FileStream(filePath, FileMode.Create))
                {
                    await file.CopyToAsync(fileStream);
                }

                return new JsonResult(true);
            }
            else return new JsonResult(false);

        }


        public JsonResult SaveOrders(int storeCode, int? accountNo)
        {
            //Business fature add
            var control = WebSession.LoginControl(this.HttpContext.Session);
            var order = _orderWork.SetOrderSave(storeCode, accountNo, control);
            return new JsonResult(order);
        }


        public JsonResult AddOrders(int productId, string productCode, string productName, string price, string quantity, string tax, string productUnit, decimal productUnitRate)
        {
            var control = WebSession.LoginControl(this.HttpContext.Session);
            var order = _orderWork.SetOrderAdd(control.UserId, productId, productCode, productName, price.To<decimal>(), quantity.To<decimal>(), tax.To<decimal>(), productUnit, productUnitRate.To<decimal>());
            return new JsonResult(order);
        }

        public JsonResult DeleteCacheOrdersProduct(int productId)
        {
            var control = WebSession.LoginControl(this.HttpContext.Session);
            var order = _orderWork.SetOrderRemove(control.UserId, productId);
            return new JsonResult(order);
        }

        public JsonResult DeleteCacheOrdersProductAll()
        {
            var control = WebSession.LoginControl(this.HttpContext.Session);
            var order = _orderWork.SetOrderRemoveAll(control.UserId);
            return new JsonResult(order);
        }




    }
}