﻿const webpack = require('webpack')
const path = require('path')
console.log(__dirname);

const config = {

    context: path.resolve(__dirname, 'src'),
    entry: {
        Login: __dirname + '/TypeScript/Service/LoginService.ts',
        Home: __dirname + '/TypeScript/Service/HomeService.ts',
        Store: __dirname + '/TypeScript/Service/StoreService.ts',
        Order: __dirname + '/TypeScript/Service/OrderService.ts',
        SpecialOrder: __dirname + '/TypeScript/Service/SpecialOrderService.ts',
        WayBill: __dirname + '/TypeScript/Service/WayBillService.ts',
        Bill: __dirname + '/TypeScript/Service/BillService.ts',
        CurrentAccount: __dirname + '/TypeScript/Service/CurrentAccountService.ts', 
        commons: [__dirname + '/TypeScript/Common/Helper.ts', __dirname + '/TypeScript/Common/Ajax.ts', __dirname + '/TypeScript/Common/Message.ts', __dirname + '/TypeScript/Common/ConfigVariable.ts', __dirname + '/TypeScript/Common/Modal.ts']
    },
    output: {
        path: path.resolve(__dirname, 'assets/js'),
        filename: '[name].js'
    },
    module: {
        loaders: [{
            //test: /\.ts$/,
            include: path.resolve(__dirname, "TypeScript"),
            loader: 'ts-loader'
        }]
    },
    plugins: [
        new webpack.optimize.CommonsChunkPlugin({
            name: 'commons',
            filename: 'commons.js'
        })
    ],
    resolve: {
        extensions: ['.js', '.ts', '.tsx']
    },
    devtool: 'inline-source-map',
    //devtool: 'none'
}

module.exports = config
