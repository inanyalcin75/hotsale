webpackJsonp([3],{

/***/ 8:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderModule", function() { return OrderModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Common_Modal__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__Common_Ajax__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__Common_Message__ = __webpack_require__(1);



//Business
var OrderModule;
(function (OrderModule) {
    class Order {
        productlist(ProductGroupId, Term, PageNo, ListCount) {
            var message = new __WEBPACK_IMPORTED_MODULE_2__Common_Message__["MessageModule"].Message();
            $(".row-products").trigger('loading-overlay:show');
            var ajaxPost = new __WEBPACK_IMPORTED_MODULE_1__Common_Ajax__["Ajax"].Service();
            var object = { productGroupId: ProductGroupId, term: Term, pageNo: PageNo, listCount: ListCount };
            var url = "/Order/GetOrderProducts";
            ajaxPost.post(url, object, function Success(data) {
                $(".row-products").trigger('loading-overlay:hide');
                $(".row-products").html(data);
                $('.number-Order').keyboard({ type: 'numpad', placement: 'top' });
                $(".order-product-search").focus();
                var strLength = $(".order-product-search").val().length * 2;
                $(".order-product-search")[0].setSelectionRange(strLength, strLength);
            }, function Error(data) {
                message.show("error", "Bildirim", "is problems");
                $(".row-products").trigger('loading-overlay:hide');
            });
        }
        productItemlist(ProductGroupId, Term, PageNo, ListCount) {
            var message = new __WEBPACK_IMPORTED_MODULE_2__Common_Message__["MessageModule"].Message();
            $(".row-products").trigger('loading-overlay:show');
            var ajaxPost = new __WEBPACK_IMPORTED_MODULE_1__Common_Ajax__["Ajax"].Service();
            var object = { productGroupId: ProductGroupId, term: Term, pageNo: PageNo, listCount: ListCount };
            var url = "/Order/GetOrderProductsItem";
            ajaxPost.post(url, object, function Success(data) {
                $(".row-products .loading-order-products").remove();
                $(".row-products").trigger('loading-overlay:hide');
                var allData = $(".row-productsItem").html() + data;
                $(".row-productsItem").html(allData);
                $('.number-Order').keyboard({ type: 'numpad', placement: 'top' });
            }, function Error(data) {
                message.show("error", "Bildirim", "is problems");
                $(".row-products").trigger('loading-overlay:hide');
            });
        }
        productImageSave(data, successCall, progressCall, errorCall) {
            var ajaxPost = new __WEBPACK_IMPORTED_MODULE_1__Common_Ajax__["Ajax"].Service();
            ajaxPost.postFormdata("/Order/SaveImage", data, successCall, progressCall, errorCall);
        }
        orderlist(Term, PageNo, ListCount, startdate, finishdate) {
            var message = new __WEBPACK_IMPORTED_MODULE_2__Common_Message__["MessageModule"].Message();
            $(".row-orders").trigger('loading-overlay:show');
            var ajaxPost = new __WEBPACK_IMPORTED_MODULE_1__Common_Ajax__["Ajax"].Service();
            var object = { term: Term, pageNo: PageNo, listCount: ListCount, startDate: startdate, finishDate: finishdate };
            var url = "/Order/GetOrders";
            ajaxPost.post(url, object, function Success(data) {
                $(".row-orders").trigger('loading-overlay:hide');
                $(".row-orders").html(data);
                new OrderModule.Order().calandertrigger();
            }, function Error(data) {
                message.show("error", "Bildirim", "is problems");
                $(".row-orders").trigger('loading-overlay:hide');
            });
        }
        orderadd(ProductId, ProductCode, ProductName, Price, Quantity, Tax, ProductUnit, ProductUnitRate) {
            var message = new __WEBPACK_IMPORTED_MODULE_2__Common_Message__["MessageModule"].Message();
            if (Quantity == "") {
                message.show("error", "Bildirim", "Miktar giriniz.");
                return false;
            }
            $(".row-products").trigger('loading-overlay:show');
            var ajaxPost = new __WEBPACK_IMPORTED_MODULE_1__Common_Ajax__["Ajax"].Service();
            var object = { productId: ProductId, productCode: ProductCode, productName: ProductName, price: Price, quantity: Quantity, tax: Tax, productUnit: ProductUnit, productUnitRate: ProductUnitRate };
            var url = "/Order/AddOrders";
            ajaxPost.post(url, object, function Success(data) {
                $(".row-products").trigger('loading-overlay:hide');
                message.show("success", "Bildirim", "Ürün eklendi.");
                $(".number-Order-" + ProductId).val("");
            }, function Error(data) {
                message.show("error", "Bildirim", "is problems");
                $(".row-products").trigger('loading-overlay:hide');
            });
        }
        orderCacheList() {
            var message = new __WEBPACK_IMPORTED_MODULE_2__Common_Message__["MessageModule"].Message();
            $(".row-orders-cache").trigger('loading-overlay:show');
            var ajaxPost = new __WEBPACK_IMPORTED_MODULE_1__Common_Ajax__["Ajax"].Service();
            var object = {};
            var url = "/Order/GetCacheOrders";
            ajaxPost.post(url, object, function Success(data) {
                $(".row-orders-cache").trigger('loading-overlay:hide');
                $(".row-orders-cache").html(data);
                new OrderModule.Order().select2trigger();
            }, function Error(data) {
                message.show("error", "Bildirim", "is problems");
                $(".row-orders-cache").trigger('loading-overlay:hide');
            });
        }
        orderCacheProductDelete(ProductId) {
            var message = new __WEBPACK_IMPORTED_MODULE_2__Common_Message__["MessageModule"].Message();
            $(".row-orders-cache").trigger('loading-overlay:show');
            var ajaxPost = new __WEBPACK_IMPORTED_MODULE_1__Common_Ajax__["Ajax"].Service();
            var object = { productId: ProductId };
            var url = "/Order/DeleteCacheOrdersProduct";
            ajaxPost.post(url, object, function Success(data) {
                $(".row-orders-cache").trigger('loading-overlay:hide');
                $(".row-orders-cache").html(data);
                new OrderModule.Order().orderCacheList();
            }, function Error(data) {
                message.show("error", "Bildirim", "is problems");
                $(".row-orders-cache").trigger('loading-overlay:hide');
            });
        }
        orderCacheProductAllDelete() {
            var message = new __WEBPACK_IMPORTED_MODULE_2__Common_Message__["MessageModule"].Message();
            $(".row-orders-cache").trigger('loading-overlay:show');
            var ajaxPost = new __WEBPACK_IMPORTED_MODULE_1__Common_Ajax__["Ajax"].Service();
            var object = {};
            var url = "/Order/DeleteCacheOrdersProductAll";
            ajaxPost.post(url, object, function Success(data) {
                $(".row-orders-cache").trigger('loading-overlay:hide');
                $(".row-orders-cache").html(data);
                new OrderModule.Order().orderCacheList();
            }, function Error(data) {
                message.show("error", "Bildirim", "is problems");
                $(".row-orders-cache").trigger('loading-overlay:hide');
            });
        }
        orderSave() {
            var message = new __WEBPACK_IMPORTED_MODULE_2__Common_Message__["MessageModule"].Message();
            $(".row-orders-cache").trigger('loading-overlay:show');
            var storecode = $(".slct-store").val();
            var accountno = $(".slct-account").val();
            var ajaxPost = new __WEBPACK_IMPORTED_MODULE_1__Common_Ajax__["Ajax"].Service();
            var object = { storeCode: storecode, accountNo: accountno };
            var url = "/Order/SaveOrders";
            ajaxPost.post(url, object, function Success(data) {
                $(".row-orders-cache").trigger('loading-overlay:hide');
                if (data.data) {
                    message.show("success", "Bildirim", "Siparişiniz oluşturuldu.");
                    $(".order-list").click();
                }
                else {
                    message.show("error", "Bildirim", "Siparişiniz oluşturulamadı");
                }
            }, function Error(data) {
                message.show("error", "Bildirim", "Sipariş oluştururken sorun oluştu");
                $(".row-orders-cache").trigger('loading-overlay:hide');
            });
        }
        calandertrigger() {
            $('[data-plugin-datepicker]').each(function () {
                var $this = $(this), opts = {};
                var pluginOptions = $this.data('plugin-options');
                if (pluginOptions)
                    opts = pluginOptions;
                $this.themePluginDatePicker(opts);
            });
        }
        select2trigger() {
            // Select2
            if ($.isFunction($.fn['select2'])) {
                $(function () {
                    $('[data-plugin-selectTwo]').each(function () {
                        var $this = $(this), opts = {};
                        var pluginOptions = $this.data('plugin-options');
                        if (pluginOptions)
                            opts = pluginOptions;
                        $this.adminPluginSelect2(opts);
                    });
                });
            }
            // Select2
            var admin = admin || {};
            var PluginSelect2;
            var instanceName = '__select2';
            PluginSelect2 = function ($el, opts) {
                return this.initialize($el, opts);
            };
            PluginSelect2.defaults = {
                theme: 'bootstrap'
            };
            PluginSelect2.prototype = {
                initialize: function ($el, opts) {
                    if ($el.data(instanceName)) {
                        return this;
                    }
                    this.$el = $el;
                    this
                        .setData()
                        .setOptions(opts)
                        .build();
                    return this;
                },
                setData: function () {
                    this.$el.data(instanceName, this);
                    return this;
                },
                setOptions: function (opts) {
                    this.options = $.extend(true, {}, PluginSelect2.defaults, opts);
                    return this;
                },
                build: function () {
                    this.$el.select2(this.options);
                    return this;
                }
            };
            // expose to scope
            $.extend(admin, {
                PluginSelect2: PluginSelect2
            });
            // jquery plugin
            $.fn.adminPluginSelect2 = function (opts) {
                return this.each(function () {
                    var $this = $(this);
                    if ($this.data(instanceName)) {
                        return $this.data(instanceName);
                    }
                    else {
                        return new PluginSelect2($this, opts);
                    }
                });
            };
        }
    }
    OrderModule.Order = Order;
})(OrderModule || (OrderModule = {}));
$(function () {
    /*
     Page Load
    */
    $(document).ready(function () {
        $('.row-products').on('click', '.order-text', function (event) {
            var productGroupId = $(this).attr("ProductGroupId");
            var term = $(".order-product-search").val();
            var listCount = 40;
            new OrderModule.Order().productlist(productGroupId, term, 1, listCount);
        });
        //search product event
        $('.row-products').on('keydown', '.order-product-search', function (e) {
            if (e.which == 13) {
                var productGroupId = $(".simple-post-list").find(".btn-primary").attr("ProductGroupId");
                var term = $(".order-product-search").val();
                var listCount = 40;
                new OrderModule.Order().productlist(productGroupId, term, 1, listCount);
            }
        });
        //fileuplaod product event
        $('.row-products').on('change', '.file-product', function (e) {
            var objectFile = this;
            var file = objectFile.files[0];
            var stockNo = $(objectFile).attr('stokNo');
            var formData = new FormData();
            formData.append('file', file);
            formData.append('stockNo', stockNo);
            new OrderModule.Order().productImageSave(formData, function Success(data) {
                console.log(data);
                var d = new Date();
                $(objectFile).parents(".thumb-image").find(".img-responsive").attr("src", "/images/stock/" + stockNo + "/" + stockNo + ".png?" + d.getTime());
            }, function Progress(e) { }, function Error(data) {
                new __WEBPACK_IMPORTED_MODULE_2__Common_Message__["MessageModule"].Message().show("error", "Hata", "işlem sırasında bir hata oluştu.");
            });
        });
        //Order List
        $('.order-list').on('click', function (event) {
            var term = $(".order-search").val();
            var startDate = "01.01.1990";
            var finishDate = new Date().toLocaleDateString();
            var listCount = 8;
            new OrderModule.Order().orderlist(term, 1, listCount, startDate, finishDate);
        });
        //Order page List
        $('body').on('click', '.btn-order-list', function (event) {
            var term = $(".order-search").val();
            var startDate = $('#txt-company-startDate').val();
            var finishDate = $('#txt-company-finishDate').val();
            var pageNo = 1;
            var listCount = 8;
            new OrderModule.Order().orderlist(term, pageNo, listCount, startDate, finishDate);
        });
        //Order page change List
        $('body').on('click', '.btn-order-page-change', function (event) {
            var term = $(".order-search").val();
            var startDate = $('#txt-company-startDate').val();
            var finishDate = $('#txt-company-finishDate').val();
            var pageNo = $(this).attr("pageNo");
            var listCount = 8;
            new OrderModule.Order().orderlist(term, pageNo, listCount, startDate, finishDate);
        });
        $('.row-orders').on('click', '.fa-selected', function () {
            if ($(this).hasClass("fa-minus-square-o")) {
                $(this).removeClass('fa-minus-square-o').addClass('fa-plus-square-o');
                $(this).parents(".selectedtr").next('.details ').fadeOut().addClass("hide");
            }
            else {
                $(this).removeClass('fa-plus-square-o').addClass('fa-minus-square-o');
                $(this).parents(".selectedtr").next('.details ').fadeIn().removeClass("hide");
            }
        });
        $('.row-products').on('click', '.btn-order-add', function () {
            var quantity = $(this).parents(".input-group").find(".number-Order").val();
            var productId = $(this).parents(".input-group").find(".number-Order").attr("ProductId");
            var productName = $(this).parents(".input-group").find(".number-Order").attr("ProductName");
            var productPrice = $(this).parents(".input-group").find(".number-Order").attr("ProductPrice");
            var productTax = $(this).parents(".input-group").find(".number-Order").attr("ProductTax");
            var productCode = $(this).parents(".input-group").find(".number-Order").attr("ProductCode");
            var productUnit = $(this).parents(".input-group").find(".number-Order").attr("ProductUnit");
            var productUnitRate = $(this).parents(".input-group").find(".number-Order").attr("ProductUnitRate");
            new OrderModule.Order().orderadd(productId, productCode, productName, productPrice, quantity, productTax, productUnit, productUnitRate);
        });
        $('.row-products').on('click', '.btn-order-create', function () {
            new OrderModule.Order().orderCacheList();
        });
        $('.row-orders-cache').on('click', '.btn-order-product-delete', function () {
            var productId = $(this).attr("productId");
            var arr = [];
            arr.push(productId);
            __WEBPACK_IMPORTED_MODULE_0__Common_Modal__["ModalModule"].modalFunc = new OrderModule.Order().orderCacheProductDelete;
            __WEBPACK_IMPORTED_MODULE_0__Common_Modal__["ModalModule"].modalArgs = arr;
            new __WEBPACK_IMPORTED_MODULE_0__Common_Modal__["ModalModule"].Modal().show("Ürünü silmek istediğinize eminmisiniz ?");
        });
        $('.row-orders-cache').on('click', '.btn-order-clear', function () {
            __WEBPACK_IMPORTED_MODULE_0__Common_Modal__["ModalModule"].modalFunc = new OrderModule.Order().orderCacheProductAllDelete;
            __WEBPACK_IMPORTED_MODULE_0__Common_Modal__["ModalModule"].modalArgs = null;
            new __WEBPACK_IMPORTED_MODULE_0__Common_Modal__["ModalModule"].Modal().show("Ürünler tamamen silinmesi istediğinize eminmisiniz ?");
        });
        $('.row-orders-cache').on('click', '.btn-order-save', function () {
            __WEBPACK_IMPORTED_MODULE_0__Common_Modal__["ModalModule"].modalFunc = new OrderModule.Order().orderSave;
            __WEBPACK_IMPORTED_MODULE_0__Common_Modal__["ModalModule"].modalArgs = null;
            new __WEBPACK_IMPORTED_MODULE_0__Common_Modal__["ModalModule"].Modal().show("Bu sipariş kaydı gerçekleştirilmesini istiyormusunuz ?");
        });
        var scrollcheck = false;
        $(window).scroll(function () {
            if ($(window).scrollTop() + $(window).height() > $(document).height() - 100) {
                if (scrollcheck == false) {
                    $(".loading-order-products").removeClass("hide");
                    scrollcheck = true;
                    setTimeout(function () {
                        scrollcheck = false;
                        var productGroupId = $(".order-text.btn-primary").attr("ProductGroupId");
                        var term = $(".order-product-search").val();
                        var listCount = 40;
                        var pageNo = $(".row-products .loading-order-products").attr("pageNo");
                        new OrderModule.Order().productItemlist(productGroupId, term, pageNo, listCount);
                    }, 3000);
                }
            }
        });
    });
});
//Frontend 


/***/ })

},[8]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi4vVHlwZVNjcmlwdC9TZXJ2aWNlL09yZGVyU2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQUE4QztBQUNSO0FBRVk7QUFFbEQsVUFBVTtBQUNKLElBQVEsV0FBVyxDQXlUeEI7QUF6VEQsV0FBYyxXQUFXO0lBRXJCO1FBRUksV0FBVyxDQUFDLGNBQXNCLEVBQUUsSUFBWSxFQUFFLE1BQWMsRUFBRSxTQUFpQjtZQUMvRSxJQUFJLE9BQU8sR0FBRyxJQUFJLDhEQUFhLENBQUMsT0FBTyxFQUFFLENBQUM7WUFDMUMsQ0FBQyxDQUFDLGVBQWUsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDO1lBRW5ELElBQUksUUFBUSxHQUFHLElBQUksa0RBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztZQUVsQyxJQUFJLE1BQU0sR0FBRyxFQUFFLGNBQWMsRUFBRSxjQUFjLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxNQUFNLEVBQUUsTUFBTSxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQUUsQ0FBQztZQUVsRyxJQUFJLEdBQUcsR0FBRyx5QkFBeUIsQ0FBQztZQUNwQyxRQUFRLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxNQUFNLEVBQUUsaUJBQWlCLElBQVM7Z0JBRWpELENBQUMsQ0FBQyxlQUFlLENBQUMsQ0FBQyxPQUFPLENBQUMsc0JBQXNCLENBQUMsQ0FBQztnQkFDbkQsQ0FBQyxDQUFDLGVBQWUsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDOUIsQ0FBQyxDQUFDLGVBQWUsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxFQUFFLElBQUksRUFBRSxRQUFRLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBRSxDQUFDLENBQUM7Z0JBQ2xFLENBQUMsQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDLEtBQUssRUFBRSxDQUFDO2dCQUNuQyxJQUFJLFNBQVMsR0FBRyxDQUFDLENBQUMsdUJBQXVCLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDO2dCQUM1RCxDQUFDLENBQUMsdUJBQXVCLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTLEVBQUUsU0FBUyxDQUFDLENBQUM7WUFFMUUsQ0FBQyxFQUFFLGVBQWUsSUFBUztnQkFDdkIsT0FBTyxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsVUFBVSxFQUFFLGFBQWEsQ0FBQyxDQUFDO2dCQUNqRCxDQUFDLENBQUMsZUFBZSxDQUFDLENBQUMsT0FBTyxDQUFDLHNCQUFzQixDQUFDLENBQUM7WUFDdkQsQ0FBQyxDQUFDLENBQUM7UUFFUCxDQUFDO1FBRUQsZUFBZSxDQUFDLGNBQXNCLEVBQUUsSUFBWSxFQUFFLE1BQWMsRUFBRSxTQUFpQjtZQUNuRixJQUFJLE9BQU8sR0FBRyxJQUFJLDhEQUFhLENBQUMsT0FBTyxFQUFFLENBQUM7WUFDMUMsQ0FBQyxDQUFDLGVBQWUsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDO1lBRW5ELElBQUksUUFBUSxHQUFHLElBQUksa0RBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztZQUVsQyxJQUFJLE1BQU0sR0FBRyxFQUFFLGNBQWMsRUFBRSxjQUFjLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxNQUFNLEVBQUUsTUFBTSxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQUUsQ0FBQztZQUVsRyxJQUFJLEdBQUcsR0FBRyw2QkFBNkIsQ0FBQztZQUN4QyxRQUFRLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxNQUFNLEVBQUUsaUJBQWlCLElBQVM7Z0JBQ2pELENBQUMsQ0FBQyx1Q0FBdUMsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDO2dCQUNwRCxDQUFDLENBQUMsZUFBZSxDQUFDLENBQUMsT0FBTyxDQUFDLHNCQUFzQixDQUFDLENBQUM7Z0JBQ25ELElBQUksT0FBTyxHQUFHLENBQUMsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLElBQUksRUFBRSxHQUFHLElBQUksQ0FBQztnQkFDbkQsQ0FBQyxDQUFDLG1CQUFtQixDQUFDLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO2dCQUNyQyxDQUFDLENBQUMsZUFBZSxDQUFDLENBQUMsUUFBUSxDQUFDLEVBQUUsSUFBSSxFQUFFLFFBQVEsRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLENBQUMsQ0FBQztZQUN0RSxDQUFDLEVBQUUsZUFBZSxJQUFTO2dCQUN2QixPQUFPLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxVQUFVLEVBQUUsYUFBYSxDQUFDLENBQUM7Z0JBQ2pELENBQUMsQ0FBQyxlQUFlLENBQUMsQ0FBQyxPQUFPLENBQUMsc0JBQXNCLENBQUMsQ0FBQztZQUN2RCxDQUFDLENBQUMsQ0FBQztRQUVQLENBQUM7UUFFRCxnQkFBZ0IsQ0FBQyxJQUFTLEVBQUUsV0FBcUIsRUFBRSxZQUFzQixFQUFFLFNBQW1CO1lBRTFGLElBQUksUUFBUSxHQUFHLElBQUksa0RBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztZQUNsQyxRQUFRLENBQUMsWUFBWSxDQUFDLGtCQUFrQixFQUFFLElBQUksRUFBRSxXQUFXLEVBQUUsWUFBWSxFQUFFLFNBQVMsQ0FBQyxDQUFDO1FBQzFGLENBQUM7UUFFRCxTQUFTLENBQUMsSUFBWSxFQUFFLE1BQWMsRUFBRSxTQUFpQixFQUFFLFNBQWlCLEVBQUUsVUFBa0I7WUFDNUYsSUFBSSxPQUFPLEdBQUcsSUFBSSw4REFBYSxDQUFDLE9BQU8sRUFBRSxDQUFDO1lBQzFDLENBQUMsQ0FBQyxhQUFhLENBQUMsQ0FBQyxPQUFPLENBQUMsc0JBQXNCLENBQUMsQ0FBQztZQUVqRCxJQUFJLFFBQVEsR0FBRyxJQUFJLGtEQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7WUFFbEMsSUFBSSxNQUFNLEdBQUcsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLE1BQU0sRUFBRSxNQUFNLEVBQUUsU0FBUyxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQUUsU0FBUyxFQUFFLFVBQVUsRUFBRSxVQUFVLEVBQUUsQ0FBQztZQUVoSCxJQUFJLEdBQUcsR0FBRyxrQkFBa0IsQ0FBQztZQUM3QixRQUFRLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxNQUFNLEVBQUUsaUJBQWlCLElBQVM7Z0JBRWpELENBQUMsQ0FBQyxhQUFhLENBQUMsQ0FBQyxPQUFPLENBQUMsc0JBQXNCLENBQUMsQ0FBQztnQkFDakQsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDNUIsSUFBSSxXQUFXLENBQUMsS0FBSyxFQUFFLENBQUMsZUFBZSxFQUFFLENBQUM7WUFFOUMsQ0FBQyxFQUFFLGVBQWUsSUFBUztnQkFDdkIsT0FBTyxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsVUFBVSxFQUFFLGFBQWEsQ0FBQyxDQUFDO2dCQUNqRCxDQUFDLENBQUMsYUFBYSxDQUFDLENBQUMsT0FBTyxDQUFDLHNCQUFzQixDQUFDLENBQUM7WUFDckQsQ0FBQyxDQUFDLENBQUM7UUFFUCxDQUFDO1FBRUQsUUFBUSxDQUFDLFNBQWlCLEVBQUUsV0FBbUIsRUFBRSxXQUFtQixFQUFFLEtBQWEsRUFBRSxRQUFnQixFQUFFLEdBQVcsRUFBRSxXQUFtQixFQUFFLGVBQXVCO1lBRTVKLElBQUksT0FBTyxHQUFHLElBQUksOERBQWEsQ0FBQyxPQUFPLEVBQUUsQ0FBQztZQUUxQyxFQUFFLENBQUMsQ0FBQyxRQUFRLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQztnQkFDakIsT0FBTyxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsVUFBVSxFQUFFLGlCQUFpQixDQUFDLENBQUM7Z0JBQ3JELE1BQU0sQ0FBQyxLQUFLLENBQUM7WUFDakIsQ0FBQztZQUVELENBQUMsQ0FBQyxlQUFlLENBQUMsQ0FBQyxPQUFPLENBQUMsc0JBQXNCLENBQUMsQ0FBQztZQUVuRCxJQUFJLFFBQVEsR0FBRyxJQUFJLGtEQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7WUFFbEMsSUFBSSxNQUFNLEdBQUcsRUFBRSxTQUFTLEVBQUUsU0FBUyxFQUFFLFdBQVcsRUFBRSxXQUFXLEVBQUUsV0FBVyxFQUFFLFdBQVcsRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLFFBQVEsRUFBRSxRQUFRLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxXQUFXLEVBQUUsV0FBVyxFQUFFLGVBQWUsRUFBRSxlQUFlLEVBQUUsQ0FBQztZQUVsTSxJQUFJLEdBQUcsR0FBRyxrQkFBa0IsQ0FBQztZQUM3QixRQUFRLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxNQUFNLEVBQUUsaUJBQWlCLElBQVM7Z0JBQ2pELENBQUMsQ0FBQyxlQUFlLENBQUMsQ0FBQyxPQUFPLENBQUMsc0JBQXNCLENBQUMsQ0FBQztnQkFDbkQsT0FBTyxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsVUFBVSxFQUFFLGVBQWUsQ0FBQyxDQUFDO2dCQUNyRCxDQUFDLENBQUMsZ0JBQWdCLEdBQUcsU0FBUyxDQUFDLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQzVDLENBQUMsRUFBRSxlQUFlLElBQVM7Z0JBQ3ZCLE9BQU8sQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLFVBQVUsRUFBRSxhQUFhLENBQUMsQ0FBQztnQkFDakQsQ0FBQyxDQUFDLGVBQWUsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDO1lBQ3ZELENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQztRQUVELGNBQWM7WUFFVixJQUFJLE9BQU8sR0FBRyxJQUFJLDhEQUFhLENBQUMsT0FBTyxFQUFFLENBQUM7WUFDMUMsQ0FBQyxDQUFDLG1CQUFtQixDQUFDLENBQUMsT0FBTyxDQUFDLHNCQUFzQixDQUFDLENBQUM7WUFFdkQsSUFBSSxRQUFRLEdBQUcsSUFBSSxrREFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO1lBRWxDLElBQUksTUFBTSxHQUFHLEVBQUUsQ0FBQztZQUVoQixJQUFJLEdBQUcsR0FBRyx1QkFBdUIsQ0FBQztZQUNsQyxRQUFRLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxNQUFNLEVBQUUsaUJBQWlCLElBQVM7Z0JBRWpELENBQUMsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDO2dCQUN2RCxDQUFDLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ2xDLElBQUksV0FBVyxDQUFDLEtBQUssRUFBRSxDQUFDLGNBQWMsRUFBRSxDQUFDO1lBRTdDLENBQUMsRUFBRSxlQUFlLElBQVM7Z0JBQ3ZCLE9BQU8sQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLFVBQVUsRUFBRSxhQUFhLENBQUMsQ0FBQztnQkFDakQsQ0FBQyxDQUFDLG1CQUFtQixDQUFDLENBQUMsT0FBTyxDQUFDLHNCQUFzQixDQUFDLENBQUM7WUFDM0QsQ0FBQyxDQUFDLENBQUM7UUFHUCxDQUFDO1FBRUQsdUJBQXVCLENBQUMsU0FBUztZQUU3QixJQUFJLE9BQU8sR0FBRyxJQUFJLDhEQUFhLENBQUMsT0FBTyxFQUFFLENBQUM7WUFDMUMsQ0FBQyxDQUFDLG1CQUFtQixDQUFDLENBQUMsT0FBTyxDQUFDLHNCQUFzQixDQUFDLENBQUM7WUFFdkQsSUFBSSxRQUFRLEdBQUcsSUFBSSxrREFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO1lBRWxDLElBQUksTUFBTSxHQUFHLEVBQUUsU0FBUyxFQUFFLFNBQVMsRUFBRSxDQUFDO1lBRXRDLElBQUksR0FBRyxHQUFHLGlDQUFpQyxDQUFDO1lBQzVDLFFBQVEsQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLE1BQU0sRUFBRSxpQkFBaUIsSUFBUztnQkFFakQsQ0FBQyxDQUFDLG1CQUFtQixDQUFDLENBQUMsT0FBTyxDQUFDLHNCQUFzQixDQUFDLENBQUM7Z0JBQ3ZELENBQUMsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFFbEMsSUFBSSxXQUFXLENBQUMsS0FBSyxFQUFFLENBQUMsY0FBYyxFQUFFLENBQUM7WUFDN0MsQ0FBQyxFQUFFLGVBQWUsSUFBUztnQkFDdkIsT0FBTyxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsVUFBVSxFQUFFLGFBQWEsQ0FBQyxDQUFDO2dCQUNqRCxDQUFDLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxPQUFPLENBQUMsc0JBQXNCLENBQUMsQ0FBQztZQUMzRCxDQUFDLENBQUMsQ0FBQztRQUNQLENBQUM7UUFFRCwwQkFBMEI7WUFFdEIsSUFBSSxPQUFPLEdBQUcsSUFBSSw4REFBYSxDQUFDLE9BQU8sRUFBRSxDQUFDO1lBQzFDLENBQUMsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDO1lBRXZELElBQUksUUFBUSxHQUFHLElBQUksa0RBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztZQUVsQyxJQUFJLE1BQU0sR0FBRyxFQUFFLENBQUM7WUFFaEIsSUFBSSxHQUFHLEdBQUcsb0NBQW9DLENBQUM7WUFDL0MsUUFBUSxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsTUFBTSxFQUFFLGlCQUFpQixJQUFTO2dCQUVqRCxDQUFDLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxPQUFPLENBQUMsc0JBQXNCLENBQUMsQ0FBQztnQkFDdkQsQ0FBQyxDQUFDLG1CQUFtQixDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUVsQyxJQUFJLFdBQVcsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxjQUFjLEVBQUUsQ0FBQztZQUM3QyxDQUFDLEVBQUUsZUFBZSxJQUFTO2dCQUN2QixPQUFPLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxVQUFVLEVBQUUsYUFBYSxDQUFDLENBQUM7Z0JBQ2pELENBQUMsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDO1lBQzNELENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQztRQUVELFNBQVM7WUFFTCxJQUFJLE9BQU8sR0FBRyxJQUFJLDhEQUFhLENBQUMsT0FBTyxFQUFFLENBQUM7WUFDMUMsQ0FBQyxDQUFDLG1CQUFtQixDQUFDLENBQUMsT0FBTyxDQUFDLHNCQUFzQixDQUFDLENBQUM7WUFFdkQsSUFBSSxTQUFTLEdBQUcsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDO1lBQ3ZDLElBQUksU0FBUyxHQUFHLENBQUMsQ0FBQyxlQUFlLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQztZQUV6QyxJQUFJLFFBQVEsR0FBRyxJQUFJLGtEQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7WUFFbEMsSUFBSSxNQUFNLEdBQUcsRUFBRSxTQUFTLEVBQUUsU0FBUyxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQUUsQ0FBQztZQUU1RCxJQUFJLEdBQUcsR0FBRyxtQkFBbUIsQ0FBQztZQUM5QixRQUFRLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxNQUFNLEVBQUUsaUJBQWlCLElBQVM7Z0JBQ2pELENBQUMsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDO2dCQUV2RCxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztvQkFDWixPQUFPLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxVQUFVLEVBQUUsMEJBQTBCLENBQUMsQ0FBQztvQkFDaEUsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxDQUFDLEtBQUssRUFBRSxDQUFDO2dCQUM3QixDQUFDO2dCQUNELElBQUksQ0FBQyxDQUFDO29CQUNGLE9BQU8sQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLFVBQVUsRUFBRSw0QkFBNEIsQ0FBQyxDQUFDO2dCQUVwRSxDQUFDO1lBRUwsQ0FBQyxFQUFFLGVBQWUsSUFBUztnQkFDdkIsT0FBTyxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsVUFBVSxFQUFFLG1DQUFtQyxDQUFDLENBQUM7Z0JBQ3ZFLENBQUMsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDO1lBQzNELENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQztRQUVELGVBQWU7WUFFWCxDQUFDLENBQUMsMEJBQTBCLENBQUMsQ0FBQyxJQUFJLENBQUM7Z0JBQy9CLElBQUksS0FBSyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsRUFDZixJQUFJLEdBQUcsRUFBRSxDQUFDO2dCQUVkLElBQUksYUFBYSxHQUFHLEtBQUssQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztnQkFDakQsRUFBRSxDQUFDLENBQUMsYUFBYSxDQUFDO29CQUNkLElBQUksR0FBRyxhQUFhLENBQUM7Z0JBRXpCLEtBQUssQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUN0QyxDQUFDLENBQUMsQ0FBQztRQUVQLENBQUM7UUFFRCxjQUFjO1lBRVYsVUFBVTtZQUNWLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFFaEMsQ0FBQyxDQUFDO29CQUNFLENBQUMsQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDLElBQUksQ0FBQzt3QkFDOUIsSUFBSSxLQUFLLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxFQUNmLElBQUksR0FBRyxFQUFFLENBQUM7d0JBRWQsSUFBSSxhQUFhLEdBQUcsS0FBSyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO3dCQUNqRCxFQUFFLENBQUMsQ0FBQyxhQUFhLENBQUM7NEJBQ2QsSUFBSSxHQUFHLGFBQWEsQ0FBQzt3QkFFekIsS0FBSyxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxDQUFDO29CQUNuQyxDQUFDLENBQUMsQ0FBQztnQkFDUCxDQUFDLENBQUMsQ0FBQztZQUVQLENBQUM7WUFHRCxVQUFVO1lBQ1YsSUFBSSxLQUFLLEdBQUcsS0FBSyxJQUFJLEVBQUUsQ0FBQztZQUN4QixJQUFJLGFBQWtCLENBQUM7WUFFdkIsSUFBSSxZQUFZLEdBQUcsV0FBVyxDQUFDO1lBRS9CLGFBQWEsR0FBRyxVQUFVLEdBQUcsRUFBRSxJQUFJO2dCQUMvQixNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLEVBQUUsSUFBSSxDQUFDLENBQUM7WUFDdEMsQ0FBQyxDQUFDO1lBRUYsYUFBYSxDQUFDLFFBQVEsR0FBRztnQkFDckIsS0FBSyxFQUFFLFdBQVc7YUFDckIsQ0FBQztZQUVGLGFBQWEsQ0FBQyxTQUFTLEdBQUc7Z0JBQ3RCLFVBQVUsRUFBRSxVQUFVLEdBQUcsRUFBRSxJQUFJO29CQUMzQixFQUFFLENBQUMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQzt3QkFDekIsTUFBTSxDQUFDLElBQUksQ0FBQztvQkFDaEIsQ0FBQztvQkFFRCxJQUFJLENBQUMsR0FBRyxHQUFHLEdBQUcsQ0FBQztvQkFFZixJQUFJO3lCQUNDLE9BQU8sRUFBRTt5QkFDVCxVQUFVLENBQUMsSUFBSSxDQUFDO3lCQUNoQixLQUFLLEVBQUUsQ0FBQztvQkFFYixNQUFNLENBQUMsSUFBSSxDQUFDO2dCQUNoQixDQUFDO2dCQUVELE9BQU8sRUFBRTtvQkFDTCxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLENBQUM7b0JBRWxDLE1BQU0sQ0FBQyxJQUFJLENBQUM7Z0JBQ2hCLENBQUM7Z0JBRUQsVUFBVSxFQUFFLFVBQVUsSUFBSTtvQkFDdEIsSUFBSSxDQUFDLE9BQU8sR0FBRyxDQUFDLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxFQUFFLEVBQUUsYUFBYSxDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsQ0FBQztvQkFFaEUsTUFBTSxDQUFDLElBQUksQ0FBQztnQkFDaEIsQ0FBQztnQkFFRCxLQUFLLEVBQUU7b0JBQ0gsSUFBSSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO29CQUUvQixNQUFNLENBQUMsSUFBSSxDQUFDO2dCQUNoQixDQUFDO2FBQ0osQ0FBQztZQUVGLGtCQUFrQjtZQUNsQixDQUFDLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRTtnQkFDWixhQUFhLEVBQUUsYUFBYTthQUMvQixDQUFDLENBQUM7WUFFSCxnQkFBZ0I7WUFDaEIsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxrQkFBa0IsR0FBRyxVQUFVLElBQUk7Z0JBQ3BDLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO29CQUNiLElBQUksS0FBSyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFFcEIsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUM7d0JBQzNCLE1BQU0sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO29CQUNwQyxDQUFDO29CQUFDLElBQUksQ0FBQyxDQUFDO3dCQUNKLE1BQU0sQ0FBQyxJQUFJLGFBQWEsQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLENBQUM7b0JBQzFDLENBQUM7Z0JBRUwsQ0FBQyxDQUFDLENBQUM7WUFDUCxDQUFDO1FBSUwsQ0FBQztLQUVKO0lBdFRZLGlCQUFLLFFBc1RqQjtBQUNMLENBQUMsRUF6VGEsV0FBVyxLQUFYLFdBQVcsUUF5VHhCO0FBT0QsQ0FBQyxDQUFDO0lBRUU7O01BRUU7SUFDRixDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsS0FBSyxDQUFDO1FBRWQsQ0FBQyxDQUFDLGVBQWUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxPQUFPLEVBQUUsYUFBYSxFQUFFLFVBQVUsS0FBVTtZQUM5RCxJQUFJLGNBQWMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUM7WUFDcEQsSUFBSSxJQUFJLEdBQUcsQ0FBQyxDQUFDLHVCQUF1QixDQUFDLENBQUMsR0FBRyxFQUFFLENBQUM7WUFDNUMsSUFBSSxTQUFTLEdBQUcsRUFBRSxDQUFDO1lBQ25CLElBQUksV0FBVyxDQUFDLEtBQUssRUFBRSxDQUFDLFdBQVcsQ0FBQyxjQUFjLEVBQUUsSUFBSSxFQUFFLENBQUMsRUFBRSxTQUFTLENBQUMsQ0FBQztRQUM1RSxDQUFDLENBQUMsQ0FBQztRQUVILHNCQUFzQjtRQUN0QixDQUFDLENBQUMsZUFBZSxDQUFDLENBQUMsRUFBRSxDQUFDLFNBQVMsRUFBRSx1QkFBdUIsRUFBRSxVQUFVLENBQUM7WUFDakUsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDO2dCQUNoQixJQUFJLGNBQWMsR0FBRyxDQUFDLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUM7Z0JBQ3hGLElBQUksSUFBSSxHQUFHLENBQUMsQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDO2dCQUM1QyxJQUFJLFNBQVMsR0FBRyxFQUFFLENBQUM7Z0JBQ25CLElBQUksV0FBVyxDQUFDLEtBQUssRUFBRSxDQUFDLFdBQVcsQ0FBQyxjQUFjLEVBQUUsSUFBSSxFQUFFLENBQUMsRUFBRSxTQUFTLENBQUMsQ0FBQztZQUM1RSxDQUFDO1FBQ0wsQ0FBQyxDQUFDLENBQUM7UUFFSCwwQkFBMEI7UUFDMUIsQ0FBQyxDQUFDLGVBQWUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxRQUFRLEVBQUUsZUFBZSxFQUFFLFVBQVUsQ0FBQztZQUV4RCxJQUFJLFVBQVUsR0FBRyxJQUFJLENBQUM7WUFFdEIsSUFBSSxJQUFJLEdBQUcsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUMvQixJQUFJLE9BQU8sR0FBRyxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBRTNDLElBQUksUUFBUSxHQUFHLElBQUksUUFBUSxFQUFFLENBQUM7WUFDOUIsUUFBUSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLENBQUM7WUFDOUIsUUFBUSxDQUFDLE1BQU0sQ0FBQyxTQUFTLEVBQUUsT0FBTyxDQUFDLENBQUM7WUFFcEMsSUFBSSxXQUFXLENBQUMsS0FBSyxFQUFFLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxFQUFFLGlCQUFpQixJQUFJO2dCQUNwRSxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNsQixJQUFJLENBQUMsR0FBRyxJQUFJLElBQUksRUFBRSxDQUFDO2dCQUNuQixDQUFDLENBQUMsVUFBVSxDQUFDLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsZ0JBQWdCLEdBQUcsT0FBTyxHQUFHLEdBQUcsR0FBRyxPQUFPLEdBQUcsT0FBTyxHQUFHLENBQUMsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDO1lBRWxKLENBQUMsRUFBRSxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsRUFBRSxlQUFlLElBQUk7Z0JBQzVDLElBQUksOERBQWEsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLE1BQU0sRUFBRSxrQ0FBa0MsQ0FBQyxDQUFDO1lBQzFGLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUM7UUFFSCxZQUFZO1FBQ1osQ0FBQyxDQUFDLGFBQWEsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxPQUFPLEVBQUUsVUFBVSxLQUFVO1lBQzdDLElBQUksSUFBSSxHQUFHLENBQUMsQ0FBQyxlQUFlLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQztZQUNwQyxJQUFJLFNBQVMsR0FBRyxZQUFZLENBQUM7WUFDN0IsSUFBSSxVQUFVLEdBQUcsSUFBSSxJQUFJLEVBQUUsQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO1lBQ2pELElBQUksU0FBUyxHQUFHLENBQUMsQ0FBQztZQUNsQixJQUFJLFdBQVcsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxTQUFTLENBQUMsSUFBSSxFQUFFLENBQUMsRUFBRSxTQUFTLEVBQUUsU0FBUyxFQUFFLFVBQVUsQ0FBQyxDQUFDO1FBQ2pGLENBQUMsQ0FBQyxDQUFDO1FBRUgsaUJBQWlCO1FBQ2pCLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFFLENBQUMsT0FBTyxFQUFFLGlCQUFpQixFQUFFLFVBQVUsS0FBVTtZQUN6RCxJQUFJLElBQUksR0FBRyxDQUFDLENBQUMsZUFBZSxDQUFDLENBQUMsR0FBRyxFQUFFLENBQUM7WUFDcEMsSUFBSSxTQUFTLEdBQUcsQ0FBQyxDQUFDLHdCQUF3QixDQUFDLENBQUMsR0FBRyxFQUFFLENBQUM7WUFDbEQsSUFBSSxVQUFVLEdBQUcsQ0FBQyxDQUFDLHlCQUF5QixDQUFDLENBQUMsR0FBRyxFQUFFLENBQUM7WUFDcEQsSUFBSSxNQUFNLEdBQUcsQ0FBQyxDQUFDO1lBQ2YsSUFBSSxTQUFTLEdBQUcsQ0FBQyxDQUFDO1lBQ2xCLElBQUksV0FBVyxDQUFDLEtBQUssRUFBRSxDQUFDLFNBQVMsQ0FBQyxJQUFJLEVBQUUsTUFBTSxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQUUsVUFBVSxDQUFDLENBQUM7UUFDdEYsQ0FBQyxDQUFDLENBQUM7UUFFSCx3QkFBd0I7UUFDeEIsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUUsQ0FBQyxPQUFPLEVBQUUsd0JBQXdCLEVBQUUsVUFBVSxLQUFVO1lBQ2hFLElBQUksSUFBSSxHQUFHLENBQUMsQ0FBQyxlQUFlLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQztZQUNwQyxJQUFJLFNBQVMsR0FBRyxDQUFDLENBQUMsd0JBQXdCLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQztZQUNsRCxJQUFJLFVBQVUsR0FBRyxDQUFDLENBQUMseUJBQXlCLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQztZQUNwRCxJQUFJLE1BQU0sR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQ3BDLElBQUksU0FBUyxHQUFHLENBQUMsQ0FBQztZQUNsQixJQUFJLFdBQVcsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxTQUFTLENBQUMsSUFBSSxFQUFFLE1BQU0sRUFBRSxTQUFTLEVBQUUsU0FBUyxFQUFFLFVBQVUsQ0FBQyxDQUFDO1FBQ3RGLENBQUMsQ0FBQyxDQUFDO1FBRUgsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxPQUFPLEVBQUUsY0FBYyxFQUFFO1lBQ3pDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxRQUFRLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3hDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxXQUFXLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxRQUFRLENBQUMsa0JBQWtCLENBQUMsQ0FBQztnQkFDdEUsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsT0FBTyxFQUFFLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQ2hGLENBQUM7WUFDRCxJQUFJLENBQUMsQ0FBQztnQkFDRixDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsV0FBVyxDQUFDLGtCQUFrQixDQUFDLENBQUMsUUFBUSxDQUFDLG1CQUFtQixDQUFDLENBQUM7Z0JBQ3RFLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUVsRixDQUFDO1FBQ0wsQ0FBQyxDQUFDLENBQUM7UUFFSCxDQUFDLENBQUMsZUFBZSxDQUFDLENBQUMsRUFBRSxDQUFDLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRTtZQUM3QyxJQUFJLFFBQVEsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQztZQUMzRSxJQUFJLFNBQVMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDeEYsSUFBSSxXQUFXLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1lBQzVGLElBQUksWUFBWSxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQztZQUM5RixJQUFJLFVBQVUsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7WUFFMUYsSUFBSSxXQUFXLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1lBQzVGLElBQUksV0FBVyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQztZQUM1RixJQUFJLGVBQWUsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQztZQUNwRyxJQUFJLFdBQVcsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxRQUFRLENBQUMsU0FBUyxFQUFFLFdBQVcsRUFBRSxXQUFXLEVBQUUsWUFBWSxFQUFFLFFBQVEsRUFBRSxVQUFVLEVBQUUsV0FBVyxFQUFFLGVBQWUsQ0FBQyxDQUFDO1FBQzVJLENBQUMsQ0FBQyxDQUFDO1FBRUgsQ0FBQyxDQUFDLGVBQWUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxPQUFPLEVBQUUsbUJBQW1CLEVBQUU7WUFDaEQsSUFBSSxXQUFXLENBQUMsS0FBSyxFQUFFLENBQUMsY0FBYyxFQUFFLENBQUM7UUFDN0MsQ0FBQyxDQUFDLENBQUM7UUFFSCxDQUFDLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxFQUFFLENBQUMsT0FBTyxFQUFFLDJCQUEyQixFQUFFO1lBQzVELElBQUksU0FBUyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDO1lBQ3pDLElBQUksR0FBRyxHQUFHLEVBQUUsQ0FBQztZQUNiLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDcEIsMERBQVcsQ0FBQyxTQUFTLEdBQUcsSUFBSSxXQUFXLENBQUMsS0FBSyxFQUFFLENBQUMsdUJBQXVCLENBQUM7WUFDeEUsMERBQVcsQ0FBQyxTQUFTLEdBQUcsR0FBRyxDQUFDO1lBQzVCLElBQUksMERBQVcsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxJQUFJLENBQUMseUNBQXlDLENBQUMsQ0FBQztRQUM1RSxDQUFDLENBQUMsQ0FBQztRQUdILENBQUMsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxPQUFPLEVBQUUsa0JBQWtCLEVBQUU7WUFDbkQsMERBQVcsQ0FBQyxTQUFTLEdBQUcsSUFBSSxXQUFXLENBQUMsS0FBSyxFQUFFLENBQUMsMEJBQTBCLENBQUM7WUFDM0UsMERBQVcsQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO1lBQzdCLElBQUksMERBQVcsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxJQUFJLENBQUMsc0RBQXNELENBQUMsQ0FBQztRQUN6RixDQUFDLENBQUMsQ0FBQztRQUVILENBQUMsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxPQUFPLEVBQUUsaUJBQWlCLEVBQUU7WUFDbEQsMERBQVcsQ0FBQyxTQUFTLEdBQUcsSUFBSSxXQUFXLENBQUMsS0FBSyxFQUFFLENBQUMsU0FBUyxDQUFDO1lBQzFELDBEQUFXLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztZQUM3QixJQUFJLDBEQUFXLENBQUMsS0FBSyxFQUFFLENBQUMsSUFBSSxDQUFDLHdEQUF3RCxDQUFDLENBQUM7UUFDM0YsQ0FBQyxDQUFDLENBQUM7UUFHSCxJQUFJLFdBQVcsR0FBRyxLQUFLLENBQUM7UUFDeEIsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLE1BQU0sQ0FBQztZQUNiLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxTQUFTLEVBQUUsR0FBRyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsTUFBTSxFQUFFLEdBQUcsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLE1BQU0sRUFBRSxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUM7Z0JBRTFFLEVBQUUsQ0FBQyxDQUFDLFdBQVcsSUFBSSxLQUFLLENBQUMsQ0FBQyxDQUFDO29CQUV2QixDQUFDLENBQUMseUJBQXlCLENBQUMsQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLENBQUM7b0JBQ2pELFdBQVcsR0FBRyxJQUFJLENBQUM7b0JBQ25CLFVBQVUsQ0FBQzt3QkFDUCxXQUFXLEdBQUcsS0FBSyxDQUFDO3dCQUdwQixJQUFJLGNBQWMsR0FBRyxDQUFDLENBQUMseUJBQXlCLENBQUMsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQzt3QkFDekUsSUFBSSxJQUFJLEdBQUcsQ0FBQyxDQUFDLHVCQUF1QixDQUFDLENBQUMsR0FBRyxFQUFFLENBQUM7d0JBQzVDLElBQUksU0FBUyxHQUFHLEVBQUUsQ0FBQzt3QkFDbkIsSUFBSSxNQUFNLEdBQUcsQ0FBQyxDQUFDLHVDQUF1QyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO3dCQUN2RSxJQUFJLFdBQVcsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxlQUFlLENBQUMsY0FBYyxFQUFFLElBQUksRUFBRSxNQUFNLEVBQUUsU0FBUyxDQUFDLENBQUM7b0JBSXJGLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztnQkFDYixDQUFDO1lBSUwsQ0FBQztRQUNMLENBQUMsQ0FBQyxDQUFDO0lBRVAsQ0FBQyxDQUFDLENBQUM7QUFLUCxDQUFDLENBQUMsQ0FBQztBQUNILFVBQVUiLCJmaWxlIjoiT3JkZXIuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBNb2RhbE1vZHVsZSB9IGZyb20gXCIuLi9Db21tb24vTW9kYWxcIjtcclxuaW1wb3J0IHsgQWpheCB9IGZyb20gXCIuLi9Db21tb24vQWpheFwiO1xyXG5pbXBvcnQgeyBDb25maWcgfSBmcm9tIFwiLi4vQ29tbW9uL0NvbmZpZ1ZhcmlhYmxlXCI7XHJcbmltcG9ydCB7IE1lc3NhZ2VNb2R1bGUgfSBmcm9tIFwiLi4vQ29tbW9uL01lc3NhZ2VcIjtcclxuXHJcbi8vQnVzaW5lc3NcclxuZXhwb3J0IG1vZHVsZSBPcmRlck1vZHVsZSB7XHJcbiAgICBkZWNsYXJlIHZhciAkOiBhbnk7XHJcbiAgICBleHBvcnQgY2xhc3MgT3JkZXIge1xyXG5cclxuICAgICAgICBwcm9kdWN0bGlzdChQcm9kdWN0R3JvdXBJZDogc3RyaW5nLCBUZXJtOiBzdHJpbmcsIFBhZ2VObzogbnVtYmVyLCBMaXN0Q291bnQ6IG51bWJlcikge1xyXG4gICAgICAgICAgICB2YXIgbWVzc2FnZSA9IG5ldyBNZXNzYWdlTW9kdWxlLk1lc3NhZ2UoKTtcclxuICAgICAgICAgICAgJChcIi5yb3ctcHJvZHVjdHNcIikudHJpZ2dlcignbG9hZGluZy1vdmVybGF5OnNob3cnKTtcclxuXHJcbiAgICAgICAgICAgIHZhciBhamF4UG9zdCA9IG5ldyBBamF4LlNlcnZpY2UoKTtcclxuXHJcbiAgICAgICAgICAgIHZhciBvYmplY3QgPSB7IHByb2R1Y3RHcm91cElkOiBQcm9kdWN0R3JvdXBJZCwgdGVybTogVGVybSwgcGFnZU5vOiBQYWdlTm8sIGxpc3RDb3VudDogTGlzdENvdW50IH07XHJcblxyXG4gICAgICAgICAgICB2YXIgdXJsID0gXCIvT3JkZXIvR2V0T3JkZXJQcm9kdWN0c1wiO1xyXG4gICAgICAgICAgICBhamF4UG9zdC5wb3N0KHVybCwgb2JqZWN0LCBmdW5jdGlvbiBTdWNjZXNzKGRhdGE6IGFueSkge1xyXG5cclxuICAgICAgICAgICAgICAgICQoXCIucm93LXByb2R1Y3RzXCIpLnRyaWdnZXIoJ2xvYWRpbmctb3ZlcmxheTpoaWRlJyk7XHJcbiAgICAgICAgICAgICAgICAkKFwiLnJvdy1wcm9kdWN0c1wiKS5odG1sKGRhdGEpO1xyXG4gICAgICAgICAgICAgICAgJCgnLm51bWJlci1PcmRlcicpLmtleWJvYXJkKHsgdHlwZTogJ251bXBhZCcsIHBsYWNlbWVudDogJ3RvcCcgfSk7XHJcbiAgICAgICAgICAgICAgICAkKFwiLm9yZGVyLXByb2R1Y3Qtc2VhcmNoXCIpLmZvY3VzKCk7XHJcbiAgICAgICAgICAgICAgICB2YXIgc3RyTGVuZ3RoID0gJChcIi5vcmRlci1wcm9kdWN0LXNlYXJjaFwiKS52YWwoKS5sZW5ndGggKiAyO1xyXG4gICAgICAgICAgICAgICAgJChcIi5vcmRlci1wcm9kdWN0LXNlYXJjaFwiKVswXS5zZXRTZWxlY3Rpb25SYW5nZShzdHJMZW5ndGgsIHN0ckxlbmd0aCk7XHJcblxyXG4gICAgICAgICAgICB9LCBmdW5jdGlvbiBFcnJvcihkYXRhOiBhbnkpIHtcclxuICAgICAgICAgICAgICAgIG1lc3NhZ2Uuc2hvdyhcImVycm9yXCIsIFwiQmlsZGlyaW1cIiwgXCJpcyBwcm9ibGVtc1wiKTtcclxuICAgICAgICAgICAgICAgICQoXCIucm93LXByb2R1Y3RzXCIpLnRyaWdnZXIoJ2xvYWRpbmctb3ZlcmxheTpoaWRlJyk7XHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHByb2R1Y3RJdGVtbGlzdChQcm9kdWN0R3JvdXBJZDogc3RyaW5nLCBUZXJtOiBzdHJpbmcsIFBhZ2VObzogbnVtYmVyLCBMaXN0Q291bnQ6IG51bWJlcikge1xyXG4gICAgICAgICAgICB2YXIgbWVzc2FnZSA9IG5ldyBNZXNzYWdlTW9kdWxlLk1lc3NhZ2UoKTtcclxuICAgICAgICAgICAgJChcIi5yb3ctcHJvZHVjdHNcIikudHJpZ2dlcignbG9hZGluZy1vdmVybGF5OnNob3cnKTtcclxuXHJcbiAgICAgICAgICAgIHZhciBhamF4UG9zdCA9IG5ldyBBamF4LlNlcnZpY2UoKTtcclxuXHJcbiAgICAgICAgICAgIHZhciBvYmplY3QgPSB7IHByb2R1Y3RHcm91cElkOiBQcm9kdWN0R3JvdXBJZCwgdGVybTogVGVybSwgcGFnZU5vOiBQYWdlTm8sIGxpc3RDb3VudDogTGlzdENvdW50IH07XHJcblxyXG4gICAgICAgICAgICB2YXIgdXJsID0gXCIvT3JkZXIvR2V0T3JkZXJQcm9kdWN0c0l0ZW1cIjtcclxuICAgICAgICAgICAgYWpheFBvc3QucG9zdCh1cmwsIG9iamVjdCwgZnVuY3Rpb24gU3VjY2VzcyhkYXRhOiBhbnkpIHtcclxuICAgICAgICAgICAgICAgICQoXCIucm93LXByb2R1Y3RzIC5sb2FkaW5nLW9yZGVyLXByb2R1Y3RzXCIpLnJlbW92ZSgpO1xyXG4gICAgICAgICAgICAgICAgJChcIi5yb3ctcHJvZHVjdHNcIikudHJpZ2dlcignbG9hZGluZy1vdmVybGF5OmhpZGUnKTtcclxuICAgICAgICAgICAgICAgIHZhciBhbGxEYXRhID0gJChcIi5yb3ctcHJvZHVjdHNJdGVtXCIpLmh0bWwoKSArIGRhdGE7XHJcbiAgICAgICAgICAgICAgICAkKFwiLnJvdy1wcm9kdWN0c0l0ZW1cIikuaHRtbChhbGxEYXRhKTtcclxuICAgICAgICAgICAgICAgICQoJy5udW1iZXItT3JkZXInKS5rZXlib2FyZCh7IHR5cGU6ICdudW1wYWQnLCBwbGFjZW1lbnQ6ICd0b3AnIH0pO1xyXG4gICAgICAgICAgICB9LCBmdW5jdGlvbiBFcnJvcihkYXRhOiBhbnkpIHtcclxuICAgICAgICAgICAgICAgIG1lc3NhZ2Uuc2hvdyhcImVycm9yXCIsIFwiQmlsZGlyaW1cIiwgXCJpcyBwcm9ibGVtc1wiKTtcclxuICAgICAgICAgICAgICAgICQoXCIucm93LXByb2R1Y3RzXCIpLnRyaWdnZXIoJ2xvYWRpbmctb3ZlcmxheTpoaWRlJyk7XHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHByb2R1Y3RJbWFnZVNhdmUoZGF0YTogYW55LCBzdWNjZXNzQ2FsbDogRnVuY3Rpb24sIHByb2dyZXNzQ2FsbDogRnVuY3Rpb24sIGVycm9yQ2FsbDogRnVuY3Rpb24pIHtcclxuXHJcbiAgICAgICAgICAgIHZhciBhamF4UG9zdCA9IG5ldyBBamF4LlNlcnZpY2UoKTtcclxuICAgICAgICAgICAgYWpheFBvc3QucG9zdEZvcm1kYXRhKFwiL09yZGVyL1NhdmVJbWFnZVwiLCBkYXRhLCBzdWNjZXNzQ2FsbCwgcHJvZ3Jlc3NDYWxsLCBlcnJvckNhbGwpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgb3JkZXJsaXN0KFRlcm06IHN0cmluZywgUGFnZU5vOiBudW1iZXIsIExpc3RDb3VudDogbnVtYmVyLCBzdGFydGRhdGU6IHN0cmluZywgZmluaXNoZGF0ZTogc3RyaW5nKSB7XHJcbiAgICAgICAgICAgIHZhciBtZXNzYWdlID0gbmV3IE1lc3NhZ2VNb2R1bGUuTWVzc2FnZSgpO1xyXG4gICAgICAgICAgICAkKFwiLnJvdy1vcmRlcnNcIikudHJpZ2dlcignbG9hZGluZy1vdmVybGF5OnNob3cnKTtcclxuXHJcbiAgICAgICAgICAgIHZhciBhamF4UG9zdCA9IG5ldyBBamF4LlNlcnZpY2UoKTtcclxuXHJcbiAgICAgICAgICAgIHZhciBvYmplY3QgPSB7IHRlcm06IFRlcm0sIHBhZ2VObzogUGFnZU5vLCBsaXN0Q291bnQ6IExpc3RDb3VudCwgc3RhcnREYXRlOiBzdGFydGRhdGUsIGZpbmlzaERhdGU6IGZpbmlzaGRhdGUgfTtcclxuXHJcbiAgICAgICAgICAgIHZhciB1cmwgPSBcIi9PcmRlci9HZXRPcmRlcnNcIjtcclxuICAgICAgICAgICAgYWpheFBvc3QucG9zdCh1cmwsIG9iamVjdCwgZnVuY3Rpb24gU3VjY2VzcyhkYXRhOiBhbnkpIHtcclxuXHJcbiAgICAgICAgICAgICAgICAkKFwiLnJvdy1vcmRlcnNcIikudHJpZ2dlcignbG9hZGluZy1vdmVybGF5OmhpZGUnKTtcclxuICAgICAgICAgICAgICAgICQoXCIucm93LW9yZGVyc1wiKS5odG1sKGRhdGEpO1xyXG4gICAgICAgICAgICAgICAgbmV3IE9yZGVyTW9kdWxlLk9yZGVyKCkuY2FsYW5kZXJ0cmlnZ2VyKCk7XHJcblxyXG4gICAgICAgICAgICB9LCBmdW5jdGlvbiBFcnJvcihkYXRhOiBhbnkpIHtcclxuICAgICAgICAgICAgICAgIG1lc3NhZ2Uuc2hvdyhcImVycm9yXCIsIFwiQmlsZGlyaW1cIiwgXCJpcyBwcm9ibGVtc1wiKTtcclxuICAgICAgICAgICAgICAgICQoXCIucm93LW9yZGVyc1wiKS50cmlnZ2VyKCdsb2FkaW5nLW92ZXJsYXk6aGlkZScpO1xyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBvcmRlcmFkZChQcm9kdWN0SWQ6IHN0cmluZywgUHJvZHVjdENvZGU6IHN0cmluZywgUHJvZHVjdE5hbWU6IHN0cmluZywgUHJpY2U6IHN0cmluZywgUXVhbnRpdHk6IHN0cmluZywgVGF4OiBzdHJpbmcsIFByb2R1Y3RVbml0OiBzdHJpbmcsIFByb2R1Y3RVbml0UmF0ZTogc3RyaW5nKSB7XHJcblxyXG4gICAgICAgICAgICB2YXIgbWVzc2FnZSA9IG5ldyBNZXNzYWdlTW9kdWxlLk1lc3NhZ2UoKTtcclxuXHJcbiAgICAgICAgICAgIGlmIChRdWFudGl0eSA9PSBcIlwiKSB7XHJcbiAgICAgICAgICAgICAgICBtZXNzYWdlLnNob3coXCJlcnJvclwiLCBcIkJpbGRpcmltXCIsIFwiTWlrdGFyIGdpcmluaXouXCIpO1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAkKFwiLnJvdy1wcm9kdWN0c1wiKS50cmlnZ2VyKCdsb2FkaW5nLW92ZXJsYXk6c2hvdycpO1xyXG5cclxuICAgICAgICAgICAgdmFyIGFqYXhQb3N0ID0gbmV3IEFqYXguU2VydmljZSgpO1xyXG5cclxuICAgICAgICAgICAgdmFyIG9iamVjdCA9IHsgcHJvZHVjdElkOiBQcm9kdWN0SWQsIHByb2R1Y3RDb2RlOiBQcm9kdWN0Q29kZSwgcHJvZHVjdE5hbWU6IFByb2R1Y3ROYW1lLCBwcmljZTogUHJpY2UsIHF1YW50aXR5OiBRdWFudGl0eSwgdGF4OiBUYXgsIHByb2R1Y3RVbml0OiBQcm9kdWN0VW5pdCwgcHJvZHVjdFVuaXRSYXRlOiBQcm9kdWN0VW5pdFJhdGUgfTtcclxuXHJcbiAgICAgICAgICAgIHZhciB1cmwgPSBcIi9PcmRlci9BZGRPcmRlcnNcIjtcclxuICAgICAgICAgICAgYWpheFBvc3QucG9zdCh1cmwsIG9iamVjdCwgZnVuY3Rpb24gU3VjY2VzcyhkYXRhOiBhbnkpIHtcclxuICAgICAgICAgICAgICAgICQoXCIucm93LXByb2R1Y3RzXCIpLnRyaWdnZXIoJ2xvYWRpbmctb3ZlcmxheTpoaWRlJyk7XHJcbiAgICAgICAgICAgICAgICBtZXNzYWdlLnNob3coXCJzdWNjZXNzXCIsIFwiQmlsZGlyaW1cIiwgXCLDnHLDvG4gZWtsZW5kaS5cIik7XHJcbiAgICAgICAgICAgICAgICAkKFwiLm51bWJlci1PcmRlci1cIiArIFByb2R1Y3RJZCkudmFsKFwiXCIpO1xyXG4gICAgICAgICAgICB9LCBmdW5jdGlvbiBFcnJvcihkYXRhOiBhbnkpIHtcclxuICAgICAgICAgICAgICAgIG1lc3NhZ2Uuc2hvdyhcImVycm9yXCIsIFwiQmlsZGlyaW1cIiwgXCJpcyBwcm9ibGVtc1wiKTtcclxuICAgICAgICAgICAgICAgICQoXCIucm93LXByb2R1Y3RzXCIpLnRyaWdnZXIoJ2xvYWRpbmctb3ZlcmxheTpoaWRlJyk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgb3JkZXJDYWNoZUxpc3QoKSB7XHJcblxyXG4gICAgICAgICAgICB2YXIgbWVzc2FnZSA9IG5ldyBNZXNzYWdlTW9kdWxlLk1lc3NhZ2UoKTtcclxuICAgICAgICAgICAgJChcIi5yb3ctb3JkZXJzLWNhY2hlXCIpLnRyaWdnZXIoJ2xvYWRpbmctb3ZlcmxheTpzaG93Jyk7XHJcblxyXG4gICAgICAgICAgICB2YXIgYWpheFBvc3QgPSBuZXcgQWpheC5TZXJ2aWNlKCk7XHJcblxyXG4gICAgICAgICAgICB2YXIgb2JqZWN0ID0ge307XHJcblxyXG4gICAgICAgICAgICB2YXIgdXJsID0gXCIvT3JkZXIvR2V0Q2FjaGVPcmRlcnNcIjtcclxuICAgICAgICAgICAgYWpheFBvc3QucG9zdCh1cmwsIG9iamVjdCwgZnVuY3Rpb24gU3VjY2VzcyhkYXRhOiBhbnkpIHtcclxuXHJcbiAgICAgICAgICAgICAgICAkKFwiLnJvdy1vcmRlcnMtY2FjaGVcIikudHJpZ2dlcignbG9hZGluZy1vdmVybGF5OmhpZGUnKTtcclxuICAgICAgICAgICAgICAgICQoXCIucm93LW9yZGVycy1jYWNoZVwiKS5odG1sKGRhdGEpO1xyXG4gICAgICAgICAgICAgICAgbmV3IE9yZGVyTW9kdWxlLk9yZGVyKCkuc2VsZWN0MnRyaWdnZXIoKTtcclxuXHJcbiAgICAgICAgICAgIH0sIGZ1bmN0aW9uIEVycm9yKGRhdGE6IGFueSkge1xyXG4gICAgICAgICAgICAgICAgbWVzc2FnZS5zaG93KFwiZXJyb3JcIiwgXCJCaWxkaXJpbVwiLCBcImlzIHByb2JsZW1zXCIpO1xyXG4gICAgICAgICAgICAgICAgJChcIi5yb3ctb3JkZXJzLWNhY2hlXCIpLnRyaWdnZXIoJ2xvYWRpbmctb3ZlcmxheTpoaWRlJyk7XHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuXHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBvcmRlckNhY2hlUHJvZHVjdERlbGV0ZShQcm9kdWN0SWQpIHtcclxuXHJcbiAgICAgICAgICAgIHZhciBtZXNzYWdlID0gbmV3IE1lc3NhZ2VNb2R1bGUuTWVzc2FnZSgpO1xyXG4gICAgICAgICAgICAkKFwiLnJvdy1vcmRlcnMtY2FjaGVcIikudHJpZ2dlcignbG9hZGluZy1vdmVybGF5OnNob3cnKTtcclxuXHJcbiAgICAgICAgICAgIHZhciBhamF4UG9zdCA9IG5ldyBBamF4LlNlcnZpY2UoKTtcclxuXHJcbiAgICAgICAgICAgIHZhciBvYmplY3QgPSB7IHByb2R1Y3RJZDogUHJvZHVjdElkIH07XHJcblxyXG4gICAgICAgICAgICB2YXIgdXJsID0gXCIvT3JkZXIvRGVsZXRlQ2FjaGVPcmRlcnNQcm9kdWN0XCI7XHJcbiAgICAgICAgICAgIGFqYXhQb3N0LnBvc3QodXJsLCBvYmplY3QsIGZ1bmN0aW9uIFN1Y2Nlc3MoZGF0YTogYW55KSB7XHJcblxyXG4gICAgICAgICAgICAgICAgJChcIi5yb3ctb3JkZXJzLWNhY2hlXCIpLnRyaWdnZXIoJ2xvYWRpbmctb3ZlcmxheTpoaWRlJyk7XHJcbiAgICAgICAgICAgICAgICAkKFwiLnJvdy1vcmRlcnMtY2FjaGVcIikuaHRtbChkYXRhKTtcclxuXHJcbiAgICAgICAgICAgICAgICBuZXcgT3JkZXJNb2R1bGUuT3JkZXIoKS5vcmRlckNhY2hlTGlzdCgpO1xyXG4gICAgICAgICAgICB9LCBmdW5jdGlvbiBFcnJvcihkYXRhOiBhbnkpIHtcclxuICAgICAgICAgICAgICAgIG1lc3NhZ2Uuc2hvdyhcImVycm9yXCIsIFwiQmlsZGlyaW1cIiwgXCJpcyBwcm9ibGVtc1wiKTtcclxuICAgICAgICAgICAgICAgICQoXCIucm93LW9yZGVycy1jYWNoZVwiKS50cmlnZ2VyKCdsb2FkaW5nLW92ZXJsYXk6aGlkZScpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIG9yZGVyQ2FjaGVQcm9kdWN0QWxsRGVsZXRlKCkge1xyXG5cclxuICAgICAgICAgICAgdmFyIG1lc3NhZ2UgPSBuZXcgTWVzc2FnZU1vZHVsZS5NZXNzYWdlKCk7XHJcbiAgICAgICAgICAgICQoXCIucm93LW9yZGVycy1jYWNoZVwiKS50cmlnZ2VyKCdsb2FkaW5nLW92ZXJsYXk6c2hvdycpO1xyXG5cclxuICAgICAgICAgICAgdmFyIGFqYXhQb3N0ID0gbmV3IEFqYXguU2VydmljZSgpO1xyXG5cclxuICAgICAgICAgICAgdmFyIG9iamVjdCA9IHt9O1xyXG5cclxuICAgICAgICAgICAgdmFyIHVybCA9IFwiL09yZGVyL0RlbGV0ZUNhY2hlT3JkZXJzUHJvZHVjdEFsbFwiO1xyXG4gICAgICAgICAgICBhamF4UG9zdC5wb3N0KHVybCwgb2JqZWN0LCBmdW5jdGlvbiBTdWNjZXNzKGRhdGE6IGFueSkge1xyXG5cclxuICAgICAgICAgICAgICAgICQoXCIucm93LW9yZGVycy1jYWNoZVwiKS50cmlnZ2VyKCdsb2FkaW5nLW92ZXJsYXk6aGlkZScpO1xyXG4gICAgICAgICAgICAgICAgJChcIi5yb3ctb3JkZXJzLWNhY2hlXCIpLmh0bWwoZGF0YSk7XHJcblxyXG4gICAgICAgICAgICAgICAgbmV3IE9yZGVyTW9kdWxlLk9yZGVyKCkub3JkZXJDYWNoZUxpc3QoKTtcclxuICAgICAgICAgICAgfSwgZnVuY3Rpb24gRXJyb3IoZGF0YTogYW55KSB7XHJcbiAgICAgICAgICAgICAgICBtZXNzYWdlLnNob3coXCJlcnJvclwiLCBcIkJpbGRpcmltXCIsIFwiaXMgcHJvYmxlbXNcIik7XHJcbiAgICAgICAgICAgICAgICAkKFwiLnJvdy1vcmRlcnMtY2FjaGVcIikudHJpZ2dlcignbG9hZGluZy1vdmVybGF5OmhpZGUnKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBvcmRlclNhdmUoKSB7XHJcblxyXG4gICAgICAgICAgICB2YXIgbWVzc2FnZSA9IG5ldyBNZXNzYWdlTW9kdWxlLk1lc3NhZ2UoKTtcclxuICAgICAgICAgICAgJChcIi5yb3ctb3JkZXJzLWNhY2hlXCIpLnRyaWdnZXIoJ2xvYWRpbmctb3ZlcmxheTpzaG93Jyk7XHJcblxyXG4gICAgICAgICAgICB2YXIgc3RvcmVjb2RlID0gJChcIi5zbGN0LXN0b3JlXCIpLnZhbCgpO1xyXG4gICAgICAgICAgICB2YXIgYWNjb3VudG5vID0gJChcIi5zbGN0LWFjY291bnRcIikudmFsKCk7XHJcblxyXG4gICAgICAgICAgICB2YXIgYWpheFBvc3QgPSBuZXcgQWpheC5TZXJ2aWNlKCk7XHJcblxyXG4gICAgICAgICAgICB2YXIgb2JqZWN0ID0geyBzdG9yZUNvZGU6IHN0b3JlY29kZSwgYWNjb3VudE5vOiBhY2NvdW50bm8gfTtcclxuXHJcbiAgICAgICAgICAgIHZhciB1cmwgPSBcIi9PcmRlci9TYXZlT3JkZXJzXCI7XHJcbiAgICAgICAgICAgIGFqYXhQb3N0LnBvc3QodXJsLCBvYmplY3QsIGZ1bmN0aW9uIFN1Y2Nlc3MoZGF0YTogYW55KSB7XHJcbiAgICAgICAgICAgICAgICAkKFwiLnJvdy1vcmRlcnMtY2FjaGVcIikudHJpZ2dlcignbG9hZGluZy1vdmVybGF5OmhpZGUnKTtcclxuXHJcbiAgICAgICAgICAgICAgICBpZiAoZGF0YS5kYXRhKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgbWVzc2FnZS5zaG93KFwic3VjY2Vzc1wiLCBcIkJpbGRpcmltXCIsIFwiU2lwYXJpxZ9pbml6IG9sdcWfdHVydWxkdS5cIik7XHJcbiAgICAgICAgICAgICAgICAgICAgJChcIi5vcmRlci1saXN0XCIpLmNsaWNrKCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICBtZXNzYWdlLnNob3coXCJlcnJvclwiLCBcIkJpbGRpcmltXCIsIFwiU2lwYXJpxZ9pbml6IG9sdcWfdHVydWxhbWFkxLFcIik7XHJcblxyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgfSwgZnVuY3Rpb24gRXJyb3IoZGF0YTogYW55KSB7XHJcbiAgICAgICAgICAgICAgICBtZXNzYWdlLnNob3coXCJlcnJvclwiLCBcIkJpbGRpcmltXCIsIFwiU2lwYXJpxZ8gb2x1xZ90dXJ1cmtlbiBzb3J1biBvbHXFn3R1XCIpO1xyXG4gICAgICAgICAgICAgICAgJChcIi5yb3ctb3JkZXJzLWNhY2hlXCIpLnRyaWdnZXIoJ2xvYWRpbmctb3ZlcmxheTpoaWRlJyk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY2FsYW5kZXJ0cmlnZ2VyKCkge1xyXG5cclxuICAgICAgICAgICAgJCgnW2RhdGEtcGx1Z2luLWRhdGVwaWNrZXJdJykuZWFjaChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICB2YXIgJHRoaXMgPSAkKHRoaXMpLFxyXG4gICAgICAgICAgICAgICAgICAgIG9wdHMgPSB7fTtcclxuXHJcbiAgICAgICAgICAgICAgICB2YXIgcGx1Z2luT3B0aW9ucyA9ICR0aGlzLmRhdGEoJ3BsdWdpbi1vcHRpb25zJyk7XHJcbiAgICAgICAgICAgICAgICBpZiAocGx1Z2luT3B0aW9ucylcclxuICAgICAgICAgICAgICAgICAgICBvcHRzID0gcGx1Z2luT3B0aW9ucztcclxuXHJcbiAgICAgICAgICAgICAgICAkdGhpcy50aGVtZVBsdWdpbkRhdGVQaWNrZXIob3B0cyk7XHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHNlbGVjdDJ0cmlnZ2VyKCkge1xyXG5cclxuICAgICAgICAgICAgLy8gU2VsZWN0MlxyXG4gICAgICAgICAgICBpZiAoJC5pc0Z1bmN0aW9uKCQuZm5bJ3NlbGVjdDInXSkpIHtcclxuXHJcbiAgICAgICAgICAgICAgICAkKGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgICAgICAkKCdbZGF0YS1wbHVnaW4tc2VsZWN0VHdvXScpLmVhY2goZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgJHRoaXMgPSAkKHRoaXMpLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb3B0cyA9IHt9O1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFyIHBsdWdpbk9wdGlvbnMgPSAkdGhpcy5kYXRhKCdwbHVnaW4tb3B0aW9ucycpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAocGx1Z2luT3B0aW9ucylcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9wdHMgPSBwbHVnaW5PcHRpb25zO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgJHRoaXMuYWRtaW5QbHVnaW5TZWxlY3QyKG9wdHMpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICB9XHJcblxyXG5cclxuICAgICAgICAgICAgLy8gU2VsZWN0MlxyXG4gICAgICAgICAgICB2YXIgYWRtaW4gPSBhZG1pbiB8fCB7fTtcclxuICAgICAgICAgICAgdmFyIFBsdWdpblNlbGVjdDI6IGFueTtcclxuXHJcbiAgICAgICAgICAgIHZhciBpbnN0YW5jZU5hbWUgPSAnX19zZWxlY3QyJztcclxuXHJcbiAgICAgICAgICAgIFBsdWdpblNlbGVjdDIgPSBmdW5jdGlvbiAoJGVsLCBvcHRzKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5pbml0aWFsaXplKCRlbCwgb3B0cyk7XHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICBQbHVnaW5TZWxlY3QyLmRlZmF1bHRzID0ge1xyXG4gICAgICAgICAgICAgICAgdGhlbWU6ICdib290c3RyYXAnXHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICBQbHVnaW5TZWxlY3QyLnByb3RvdHlwZSA9IHtcclxuICAgICAgICAgICAgICAgIGluaXRpYWxpemU6IGZ1bmN0aW9uICgkZWwsIG9wdHMpIHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoJGVsLmRhdGEoaW5zdGFuY2VOYW1lKSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gdGhpcztcclxuICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuJGVsID0gJGVsO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICB0aGlzXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC5zZXREYXRhKClcclxuICAgICAgICAgICAgICAgICAgICAgICAgLnNldE9wdGlvbnMob3B0cylcclxuICAgICAgICAgICAgICAgICAgICAgICAgLmJ1aWxkKCk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiB0aGlzO1xyXG4gICAgICAgICAgICAgICAgfSxcclxuXHJcbiAgICAgICAgICAgICAgICBzZXREYXRhOiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy4kZWwuZGF0YShpbnN0YW5jZU5hbWUsIHRoaXMpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gdGhpcztcclxuICAgICAgICAgICAgICAgIH0sXHJcblxyXG4gICAgICAgICAgICAgICAgc2V0T3B0aW9uczogZnVuY3Rpb24gKG9wdHMpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm9wdGlvbnMgPSAkLmV4dGVuZCh0cnVlLCB7fSwgUGx1Z2luU2VsZWN0Mi5kZWZhdWx0cywgb3B0cyk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiB0aGlzO1xyXG4gICAgICAgICAgICAgICAgfSxcclxuXHJcbiAgICAgICAgICAgICAgICBidWlsZDogZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuJGVsLnNlbGVjdDIodGhpcy5vcHRpb25zKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHRoaXM7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICAvLyBleHBvc2UgdG8gc2NvcGVcclxuICAgICAgICAgICAgJC5leHRlbmQoYWRtaW4sIHtcclxuICAgICAgICAgICAgICAgIFBsdWdpblNlbGVjdDI6IFBsdWdpblNlbGVjdDJcclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAvLyBqcXVlcnkgcGx1Z2luXHJcbiAgICAgICAgICAgICQuZm4uYWRtaW5QbHVnaW5TZWxlY3QyID0gZnVuY3Rpb24gKG9wdHMpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLmVhY2goZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHZhciAkdGhpcyA9ICQodGhpcyk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIGlmICgkdGhpcy5kYXRhKGluc3RhbmNlTmFtZSkpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuICR0aGlzLmRhdGEoaW5zdGFuY2VOYW1lKTtcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gbmV3IFBsdWdpblNlbGVjdDIoJHRoaXMsIG9wdHMpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG5cclxuXHJcblxyXG4gICAgICAgIH1cclxuXHJcbiAgICB9XHJcbn1cclxuLy9CdXNpbmVzc1xyXG5cclxuXHJcblxyXG4vL0Zyb250ZW5kXHJcbmRlY2xhcmUgdmFyICQ6IGFueTtcclxuJChmdW5jdGlvbiAoKSB7XHJcblxyXG4gICAgLypcclxuICAgICBQYWdlIExvYWRcclxuICAgICovXHJcbiAgICAkKGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbiAoKSB7XHJcblxyXG4gICAgICAgICQoJy5yb3ctcHJvZHVjdHMnKS5vbignY2xpY2snLCAnLm9yZGVyLXRleHQnLCBmdW5jdGlvbiAoZXZlbnQ6IGFueSkge1xyXG4gICAgICAgICAgICB2YXIgcHJvZHVjdEdyb3VwSWQgPSAkKHRoaXMpLmF0dHIoXCJQcm9kdWN0R3JvdXBJZFwiKTtcclxuICAgICAgICAgICAgdmFyIHRlcm0gPSAkKFwiLm9yZGVyLXByb2R1Y3Qtc2VhcmNoXCIpLnZhbCgpO1xyXG4gICAgICAgICAgICB2YXIgbGlzdENvdW50ID0gNDA7XHJcbiAgICAgICAgICAgIG5ldyBPcmRlck1vZHVsZS5PcmRlcigpLnByb2R1Y3RsaXN0KHByb2R1Y3RHcm91cElkLCB0ZXJtLCAxLCBsaXN0Q291bnQpO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICAvL3NlYXJjaCBwcm9kdWN0IGV2ZW50XHJcbiAgICAgICAgJCgnLnJvdy1wcm9kdWN0cycpLm9uKCdrZXlkb3duJywgJy5vcmRlci1wcm9kdWN0LXNlYXJjaCcsIGZ1bmN0aW9uIChlKSB7XHJcbiAgICAgICAgICAgIGlmIChlLndoaWNoID09IDEzKSB7XHJcbiAgICAgICAgICAgICAgICB2YXIgcHJvZHVjdEdyb3VwSWQgPSAkKFwiLnNpbXBsZS1wb3N0LWxpc3RcIikuZmluZChcIi5idG4tcHJpbWFyeVwiKS5hdHRyKFwiUHJvZHVjdEdyb3VwSWRcIik7XHJcbiAgICAgICAgICAgICAgICB2YXIgdGVybSA9ICQoXCIub3JkZXItcHJvZHVjdC1zZWFyY2hcIikudmFsKCk7XHJcbiAgICAgICAgICAgICAgICB2YXIgbGlzdENvdW50ID0gNDA7XHJcbiAgICAgICAgICAgICAgICBuZXcgT3JkZXJNb2R1bGUuT3JkZXIoKS5wcm9kdWN0bGlzdChwcm9kdWN0R3JvdXBJZCwgdGVybSwgMSwgbGlzdENvdW50KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICAvL2ZpbGV1cGxhb2QgcHJvZHVjdCBldmVudFxyXG4gICAgICAgICQoJy5yb3ctcHJvZHVjdHMnKS5vbignY2hhbmdlJywgJy5maWxlLXByb2R1Y3QnLCBmdW5jdGlvbiAoZSkge1xyXG5cclxuICAgICAgICAgICAgdmFyIG9iamVjdEZpbGUgPSB0aGlzO1xyXG5cclxuICAgICAgICAgICAgdmFyIGZpbGUgPSBvYmplY3RGaWxlLmZpbGVzWzBdO1xyXG4gICAgICAgICAgICB2YXIgc3RvY2tObyA9ICQob2JqZWN0RmlsZSkuYXR0cignc3Rva05vJyk7XHJcblxyXG4gICAgICAgICAgICB2YXIgZm9ybURhdGEgPSBuZXcgRm9ybURhdGEoKTtcclxuICAgICAgICAgICAgZm9ybURhdGEuYXBwZW5kKCdmaWxlJywgZmlsZSk7XHJcbiAgICAgICAgICAgIGZvcm1EYXRhLmFwcGVuZCgnc3RvY2tObycsIHN0b2NrTm8pO1xyXG5cclxuICAgICAgICAgICAgbmV3IE9yZGVyTW9kdWxlLk9yZGVyKCkucHJvZHVjdEltYWdlU2F2ZShmb3JtRGF0YSwgZnVuY3Rpb24gU3VjY2VzcyhkYXRhKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhkYXRhKTtcclxuICAgICAgICAgICAgICAgIHZhciBkID0gbmV3IERhdGUoKTtcclxuICAgICAgICAgICAgICAgICQob2JqZWN0RmlsZSkucGFyZW50cyhcIi50aHVtYi1pbWFnZVwiKS5maW5kKFwiLmltZy1yZXNwb25zaXZlXCIpLmF0dHIoXCJzcmNcIiwgXCIvaW1hZ2VzL3N0b2NrL1wiICsgc3RvY2tObyArIFwiL1wiICsgc3RvY2tObyArIFwiLnBuZz9cIiArIGQuZ2V0VGltZSgpKTtcclxuXHJcbiAgICAgICAgICAgIH0sIGZ1bmN0aW9uIFByb2dyZXNzKGUpIHsgfSwgZnVuY3Rpb24gRXJyb3IoZGF0YSkge1xyXG4gICAgICAgICAgICAgICAgbmV3IE1lc3NhZ2VNb2R1bGUuTWVzc2FnZSgpLnNob3coXCJlcnJvclwiLCBcIkhhdGFcIiwgXCJpxZ9sZW0gc8SxcmFzxLFuZGEgYmlyIGhhdGEgb2x1xZ90dS5cIik7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICAvL09yZGVyIExpc3RcclxuICAgICAgICAkKCcub3JkZXItbGlzdCcpLm9uKCdjbGljaycsIGZ1bmN0aW9uIChldmVudDogYW55KSB7XHJcbiAgICAgICAgICAgIHZhciB0ZXJtID0gJChcIi5vcmRlci1zZWFyY2hcIikudmFsKCk7XHJcbiAgICAgICAgICAgIHZhciBzdGFydERhdGUgPSBcIjAxLjAxLjE5OTBcIjtcclxuICAgICAgICAgICAgdmFyIGZpbmlzaERhdGUgPSBuZXcgRGF0ZSgpLnRvTG9jYWxlRGF0ZVN0cmluZygpO1xyXG4gICAgICAgICAgICB2YXIgbGlzdENvdW50ID0gODtcclxuICAgICAgICAgICAgbmV3IE9yZGVyTW9kdWxlLk9yZGVyKCkub3JkZXJsaXN0KHRlcm0sIDEsIGxpc3RDb3VudCwgc3RhcnREYXRlLCBmaW5pc2hEYXRlKTtcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgLy9PcmRlciBwYWdlIExpc3RcclxuICAgICAgICAkKCdib2R5Jykub24oJ2NsaWNrJywgJy5idG4tb3JkZXItbGlzdCcsIGZ1bmN0aW9uIChldmVudDogYW55KSB7XHJcbiAgICAgICAgICAgIHZhciB0ZXJtID0gJChcIi5vcmRlci1zZWFyY2hcIikudmFsKCk7XHJcbiAgICAgICAgICAgIHZhciBzdGFydERhdGUgPSAkKCcjdHh0LWNvbXBhbnktc3RhcnREYXRlJykudmFsKCk7XHJcbiAgICAgICAgICAgIHZhciBmaW5pc2hEYXRlID0gJCgnI3R4dC1jb21wYW55LWZpbmlzaERhdGUnKS52YWwoKTtcclxuICAgICAgICAgICAgdmFyIHBhZ2VObyA9IDE7XHJcbiAgICAgICAgICAgIHZhciBsaXN0Q291bnQgPSA4O1xyXG4gICAgICAgICAgICBuZXcgT3JkZXJNb2R1bGUuT3JkZXIoKS5vcmRlcmxpc3QodGVybSwgcGFnZU5vLCBsaXN0Q291bnQsIHN0YXJ0RGF0ZSwgZmluaXNoRGF0ZSk7XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIC8vT3JkZXIgcGFnZSBjaGFuZ2UgTGlzdFxyXG4gICAgICAgICQoJ2JvZHknKS5vbignY2xpY2snLCAnLmJ0bi1vcmRlci1wYWdlLWNoYW5nZScsIGZ1bmN0aW9uIChldmVudDogYW55KSB7XHJcbiAgICAgICAgICAgIHZhciB0ZXJtID0gJChcIi5vcmRlci1zZWFyY2hcIikudmFsKCk7XHJcbiAgICAgICAgICAgIHZhciBzdGFydERhdGUgPSAkKCcjdHh0LWNvbXBhbnktc3RhcnREYXRlJykudmFsKCk7XHJcbiAgICAgICAgICAgIHZhciBmaW5pc2hEYXRlID0gJCgnI3R4dC1jb21wYW55LWZpbmlzaERhdGUnKS52YWwoKTtcclxuICAgICAgICAgICAgdmFyIHBhZ2VObyA9ICQodGhpcykuYXR0cihcInBhZ2VOb1wiKTtcclxuICAgICAgICAgICAgdmFyIGxpc3RDb3VudCA9IDg7XHJcbiAgICAgICAgICAgIG5ldyBPcmRlck1vZHVsZS5PcmRlcigpLm9yZGVybGlzdCh0ZXJtLCBwYWdlTm8sIGxpc3RDb3VudCwgc3RhcnREYXRlLCBmaW5pc2hEYXRlKTtcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgJCgnLnJvdy1vcmRlcnMnKS5vbignY2xpY2snLCAnLmZhLXNlbGVjdGVkJywgZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICBpZiAoJCh0aGlzKS5oYXNDbGFzcyhcImZhLW1pbnVzLXNxdWFyZS1vXCIpKSB7XHJcbiAgICAgICAgICAgICAgICAkKHRoaXMpLnJlbW92ZUNsYXNzKCdmYS1taW51cy1zcXVhcmUtbycpLmFkZENsYXNzKCdmYS1wbHVzLXNxdWFyZS1vJyk7XHJcbiAgICAgICAgICAgICAgICAkKHRoaXMpLnBhcmVudHMoXCIuc2VsZWN0ZWR0clwiKS5uZXh0KCcuZGV0YWlscyAnKS5mYWRlT3V0KCkuYWRkQ2xhc3MoXCJoaWRlXCIpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgJCh0aGlzKS5yZW1vdmVDbGFzcygnZmEtcGx1cy1zcXVhcmUtbycpLmFkZENsYXNzKCdmYS1taW51cy1zcXVhcmUtbycpO1xyXG4gICAgICAgICAgICAgICAgJCh0aGlzKS5wYXJlbnRzKFwiLnNlbGVjdGVkdHJcIikubmV4dCgnLmRldGFpbHMgJykuZmFkZUluKCkucmVtb3ZlQ2xhc3MoXCJoaWRlXCIpO1xyXG5cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICAkKCcucm93LXByb2R1Y3RzJykub24oJ2NsaWNrJywgJy5idG4tb3JkZXItYWRkJywgZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICB2YXIgcXVhbnRpdHkgPSAkKHRoaXMpLnBhcmVudHMoXCIuaW5wdXQtZ3JvdXBcIikuZmluZChcIi5udW1iZXItT3JkZXJcIikudmFsKCk7XHJcbiAgICAgICAgICAgIHZhciBwcm9kdWN0SWQgPSAkKHRoaXMpLnBhcmVudHMoXCIuaW5wdXQtZ3JvdXBcIikuZmluZChcIi5udW1iZXItT3JkZXJcIikuYXR0cihcIlByb2R1Y3RJZFwiKTtcclxuICAgICAgICAgICAgdmFyIHByb2R1Y3ROYW1lID0gJCh0aGlzKS5wYXJlbnRzKFwiLmlucHV0LWdyb3VwXCIpLmZpbmQoXCIubnVtYmVyLU9yZGVyXCIpLmF0dHIoXCJQcm9kdWN0TmFtZVwiKTtcclxuICAgICAgICAgICAgdmFyIHByb2R1Y3RQcmljZSA9ICQodGhpcykucGFyZW50cyhcIi5pbnB1dC1ncm91cFwiKS5maW5kKFwiLm51bWJlci1PcmRlclwiKS5hdHRyKFwiUHJvZHVjdFByaWNlXCIpO1xyXG4gICAgICAgICAgICB2YXIgcHJvZHVjdFRheCA9ICQodGhpcykucGFyZW50cyhcIi5pbnB1dC1ncm91cFwiKS5maW5kKFwiLm51bWJlci1PcmRlclwiKS5hdHRyKFwiUHJvZHVjdFRheFwiKTtcclxuXHJcbiAgICAgICAgICAgIHZhciBwcm9kdWN0Q29kZSA9ICQodGhpcykucGFyZW50cyhcIi5pbnB1dC1ncm91cFwiKS5maW5kKFwiLm51bWJlci1PcmRlclwiKS5hdHRyKFwiUHJvZHVjdENvZGVcIik7XHJcbiAgICAgICAgICAgIHZhciBwcm9kdWN0VW5pdCA9ICQodGhpcykucGFyZW50cyhcIi5pbnB1dC1ncm91cFwiKS5maW5kKFwiLm51bWJlci1PcmRlclwiKS5hdHRyKFwiUHJvZHVjdFVuaXRcIik7XHJcbiAgICAgICAgICAgIHZhciBwcm9kdWN0VW5pdFJhdGUgPSAkKHRoaXMpLnBhcmVudHMoXCIuaW5wdXQtZ3JvdXBcIikuZmluZChcIi5udW1iZXItT3JkZXJcIikuYXR0cihcIlByb2R1Y3RVbml0UmF0ZVwiKTtcclxuICAgICAgICAgICAgbmV3IE9yZGVyTW9kdWxlLk9yZGVyKCkub3JkZXJhZGQocHJvZHVjdElkLCBwcm9kdWN0Q29kZSwgcHJvZHVjdE5hbWUsIHByb2R1Y3RQcmljZSwgcXVhbnRpdHksIHByb2R1Y3RUYXgsIHByb2R1Y3RVbml0LCBwcm9kdWN0VW5pdFJhdGUpO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICAkKCcucm93LXByb2R1Y3RzJykub24oJ2NsaWNrJywgJy5idG4tb3JkZXItY3JlYXRlJywgZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICBuZXcgT3JkZXJNb2R1bGUuT3JkZXIoKS5vcmRlckNhY2hlTGlzdCgpO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICAkKCcucm93LW9yZGVycy1jYWNoZScpLm9uKCdjbGljaycsICcuYnRuLW9yZGVyLXByb2R1Y3QtZGVsZXRlJywgZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICB2YXIgcHJvZHVjdElkID0gJCh0aGlzKS5hdHRyKFwicHJvZHVjdElkXCIpXHJcbiAgICAgICAgICAgIHZhciBhcnIgPSBbXTtcclxuICAgICAgICAgICAgYXJyLnB1c2gocHJvZHVjdElkKTtcclxuICAgICAgICAgICAgTW9kYWxNb2R1bGUubW9kYWxGdW5jID0gbmV3IE9yZGVyTW9kdWxlLk9yZGVyKCkub3JkZXJDYWNoZVByb2R1Y3REZWxldGU7XHJcbiAgICAgICAgICAgIE1vZGFsTW9kdWxlLm1vZGFsQXJncyA9IGFycjtcclxuICAgICAgICAgICAgbmV3IE1vZGFsTW9kdWxlLk1vZGFsKCkuc2hvdyhcIsOccsO8bsO8IHNpbG1layBpc3RlZGnEn2luaXplIGVtaW5taXNpbml6ID9cIik7XHJcbiAgICAgICAgfSk7XHJcblxyXG5cclxuICAgICAgICAkKCcucm93LW9yZGVycy1jYWNoZScpLm9uKCdjbGljaycsICcuYnRuLW9yZGVyLWNsZWFyJywgZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICBNb2RhbE1vZHVsZS5tb2RhbEZ1bmMgPSBuZXcgT3JkZXJNb2R1bGUuT3JkZXIoKS5vcmRlckNhY2hlUHJvZHVjdEFsbERlbGV0ZTtcclxuICAgICAgICAgICAgTW9kYWxNb2R1bGUubW9kYWxBcmdzID0gbnVsbDtcclxuICAgICAgICAgICAgbmV3IE1vZGFsTW9kdWxlLk1vZGFsKCkuc2hvdyhcIsOccsO8bmxlciB0YW1hbWVuIHNpbGlubWVzaSBpc3RlZGnEn2luaXplIGVtaW5taXNpbml6ID9cIik7XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICQoJy5yb3ctb3JkZXJzLWNhY2hlJykub24oJ2NsaWNrJywgJy5idG4tb3JkZXItc2F2ZScsIGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgTW9kYWxNb2R1bGUubW9kYWxGdW5jID0gbmV3IE9yZGVyTW9kdWxlLk9yZGVyKCkub3JkZXJTYXZlO1xyXG4gICAgICAgICAgICBNb2RhbE1vZHVsZS5tb2RhbEFyZ3MgPSBudWxsO1xyXG4gICAgICAgICAgICBuZXcgTW9kYWxNb2R1bGUuTW9kYWwoKS5zaG93KFwiQnUgc2lwYXJpxZ8ga2F5ZMSxIGdlcsOnZWtsZcWfdGlyaWxtZXNpbmkgaXN0aXlvcm11c3VudXogP1wiKTtcclxuICAgICAgICB9KTtcclxuXHJcblxyXG4gICAgICAgIHZhciBzY3JvbGxjaGVjayA9IGZhbHNlO1xyXG4gICAgICAgICQod2luZG93KS5zY3JvbGwoZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICBpZiAoJCh3aW5kb3cpLnNjcm9sbFRvcCgpICsgJCh3aW5kb3cpLmhlaWdodCgpID4gJChkb2N1bWVudCkuaGVpZ2h0KCkgLSAxMDApIHtcclxuXHJcbiAgICAgICAgICAgICAgICBpZiAoc2Nyb2xsY2hlY2sgPT0gZmFsc2UpIHtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgJChcIi5sb2FkaW5nLW9yZGVyLXByb2R1Y3RzXCIpLnJlbW92ZUNsYXNzKFwiaGlkZVwiKTtcclxuICAgICAgICAgICAgICAgICAgICBzY3JvbGxjaGVjayA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHNjcm9sbGNoZWNrID0gZmFsc2U7XHJcblxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFyIHByb2R1Y3RHcm91cElkID0gJChcIi5vcmRlci10ZXh0LmJ0bi1wcmltYXJ5XCIpLmF0dHIoXCJQcm9kdWN0R3JvdXBJZFwiKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFyIHRlcm0gPSAkKFwiLm9yZGVyLXByb2R1Y3Qtc2VhcmNoXCIpLnZhbCgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgbGlzdENvdW50ID0gNDA7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciBwYWdlTm8gPSAkKFwiLnJvdy1wcm9kdWN0cyAubG9hZGluZy1vcmRlci1wcm9kdWN0c1wiKS5hdHRyKFwicGFnZU5vXCIpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBuZXcgT3JkZXJNb2R1bGUuT3JkZXIoKS5wcm9kdWN0SXRlbWxpc3QocHJvZHVjdEdyb3VwSWQsIHRlcm0sIHBhZ2VObywgbGlzdENvdW50KTtcclxuXHJcblxyXG5cclxuICAgICAgICAgICAgICAgICAgICB9LCAzMDAwKTtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcblxyXG5cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgIH0pO1xyXG5cclxuXHJcblxyXG5cclxufSk7XHJcbi8vRnJvbnRlbmRcblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi4vVHlwZVNjcmlwdC9TZXJ2aWNlL09yZGVyU2VydmljZS50cyJdLCJzb3VyY2VSb290IjoiIn0=