/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	var parentJsonpFunction = window["webpackJsonp"];
/******/ 	window["webpackJsonp"] = function webpackJsonpCallback(chunkIds, moreModules, executeModules) {
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [], result;
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(installedChunks[chunkId]) {
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			}
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(chunkIds, moreModules, executeModules);
/******/ 		while(resolves.length) {
/******/ 			resolves.shift()();
/******/ 		}
/******/ 		if(executeModules) {
/******/ 			for(i=0; i < executeModules.length; i++) {
/******/ 				result = __webpack_require__(__webpack_require__.s = executeModules[i]);
/******/ 			}
/******/ 		}
/******/ 		return result;
/******/ 	};
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// objects to store loaded and loading chunks
/******/ 	var installedChunks = {
/******/ 		8: 0
/******/ 	};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// on error function for async loading
/******/ 	__webpack_require__.oe = function(err) { console.error(err); throw err; };
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 13);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Ajax", function() { return Ajax; });
var Ajax;
(function (Ajax) {
    class Options {
        constructor(url, method, data) {
            this.url = url;
            this.method = method || "get";
            this.data = data || {};
        }
    }
    Ajax.Options = Options;
    class Service {
        constructor() {
            this.request = (options, successCallback, errorCallback) => {
                var that = this;
                $.ajax({
                    url: options.url,
                    type: options.method,
                    data: options.data,
                    traditional: true,
                    cache: false,
                    success: function (d) {
                        successCallback(d);
                    },
                    error: function (d) {
                        if (errorCallback) {
                            errorCallback(d);
                            return;
                        }
                        var errorTitle = "Error in (" + options.url + ")";
                        var fullError = JSON.stringify(d);
                        console.log(errorTitle);
                        console.log(fullError);
                    }
                });
            };
            this.requestFormdata = function (options, successCallback, progressCallback, errorCallback) {
                var that = this;
                $.ajax({
                    url: options.url,
                    type: options.method,
                    data: options.data,
                    processData: false,
                    contentType: false,
                    timeout: 10000000,
                    success: function (xhr) {
                        successCallback(xhr);
                    },
                    xhr: function () {
                        var xhr = new XMLHttpRequest();
                        //Upload progress
                        xhr.upload.addEventListener("progress", function (evt) {
                            if (evt.lengthComputable) {
                                var percentComplete = evt.loaded / evt.total;
                                //Do something with upload progress
                                progressCallback(evt);
                            }
                        }, false);
                        //Download progress
                        xhr.addEventListener("progress", function (evt) {
                            if (evt.lengthComputable) {
                                var percentComplete = evt.loaded / evt.total;
                                //Do something with download progress
                                progressCallback(evt);
                            }
                        }, false);
                        return xhr;
                    },
                    error: function (d) {
                        if (errorCallback) {
                            errorCallback(d);
                            return;
                        }
                        var errorTitle = "Error in (" + options.url + ")";
                        var fullError = JSON.stringify(d);
                        console.log(errorTitle);
                        console.log(fullError);
                    }
                });
            };
            this.get = (url, successCallback, errorCallback) => {
                this.request(new Options(url), successCallback, errorCallback);
            };
            this.post = (url, data, successCallback, errorCallback) => {
                this.request(new Options(url, "post", data), successCallback, errorCallback);
            };
            this.postFormdata = function (url, data, successCallback, progressCallback, errorCallback) {
                this.requestFormdata(new Options(url, "post", data), successCallback, progressCallback, errorCallback);
            };
        }
    }
    Ajax.Service = Service;
})(Ajax || (Ajax = {}));


/***/ }),
/* 1 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MessageModule", function() { return MessageModule; });
var MessageModule;
(function (MessageModule) {
    var stack_bar_top = { "dir1": "down", "dir2": "right", "push": "top", "spacing1": 0, "spacing2": 0 };
    //Type : error,success
    class Message {
        show(type, title, message) {
            $(function () {
                var notice = new PNotify({
                    title: title,
                    text: message,
                    type: type,
                });
            });
        }
    }
    MessageModule.Message = Message;
})(MessageModule || (MessageModule = {}));


/***/ }),
/* 2 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
class Config {
}
/* harmony export (immutable) */ __webpack_exports__["Config"] = Config;

Config.BaseUrl = '/';


/***/ }),
/* 3 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HelperModule", function() { return HelperModule; });
var HelperModule;
(function (HelperModule) {
    class Helper {
        getParameterByName(name, url) {
            if (!url)
                url = window.location.href;
            name = name.replace(/[\[\]]/g, "\\$&");
            var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"), results = regex.exec(url);
            if (!results)
                return null;
            if (!results[2])
                return '';
            return decodeURIComponent(results[2].replace(/\+/g, " "));
        }
    }
    HelperModule.Helper = Helper;
})(HelperModule || (HelperModule = {}));


/***/ }),
/* 4 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalModule", function() { return ModalModule; });
var ModalModule;
(function (ModalModule) {
    class Modal {
        //show(func: Function, args: any[], html: string) { bu yapı değiştirildi çünkü sayfa farklı birden fazla popup çağırıldığında parametreler eski geliyor
        show(html) {
            $('#ModalBasic').find('.modal-text').html(html);
            //Bu kontrol popup birden fazla tıklama oluyordu ondan dolayı yapıldı tekrar butona basıldığı durumlarda
            if ($.magnificPopup.instance.items == undefined) {
                $("#ModalBasic").on('click', '.modal-confirm', function (event) {
                    ModalModule.modalFunc.apply(null, ModalModule.modalArgs);
                    $.magnificPopup.proto.close.call(this);
                });
                $('.modal-dismiss').on('click', function (event) {
                    $.magnificPopup.proto.close.call(this);
                });
            }
            $.magnificPopup.open({
                items: {
                    src: '#ModalBasic'
                },
                type: 'inline',
            });
        }
    }
    ModalModule.Modal = Modal;
})(ModalModule || (ModalModule = {}));


/***/ }),
/* 5 */,
/* 6 */,
/* 7 */,
/* 8 */,
/* 9 */,
/* 10 */,
/* 11 */,
/* 12 */,
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(3);
__webpack_require__(0);
__webpack_require__(1);
__webpack_require__(2);
module.exports = __webpack_require__(4);


/***/ })
/******/ ]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgNTkzMjgwYWIxMDgyNjM3MDVmMDEiLCJ3ZWJwYWNrOi8vLy4uL1R5cGVTY3JpcHQvQ29tbW9uL0FqYXgudHMiLCJ3ZWJwYWNrOi8vLy4uL1R5cGVTY3JpcHQvQ29tbW9uL01lc3NhZ2UudHMiLCJ3ZWJwYWNrOi8vLy4uL1R5cGVTY3JpcHQvQ29tbW9uL0NvbmZpZ1ZhcmlhYmxlLnRzIiwid2VicGFjazovLy8uLi9UeXBlU2NyaXB0L0NvbW1vbi9IZWxwZXIudHMiLCJ3ZWJwYWNrOi8vLy4uL1R5cGVTY3JpcHQvQ29tbW9uL01vZGFsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxnQkFBUSxvQkFBb0I7QUFDNUI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxvQkFBWSwyQkFBMkI7QUFDdkM7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBSztBQUNMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsbUNBQTJCLDBCQUEwQixFQUFFO0FBQ3ZELHlDQUFpQyxlQUFlO0FBQ2hEO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLDhEQUFzRCwrREFBK0Q7O0FBRXJIO0FBQ0E7O0FBRUE7QUFDQSxrREFBMEMsb0JBQW9CLFdBQVc7O0FBRXpFO0FBQ0E7Ozs7Ozs7Ozs7QUNuR00sSUFBUSxJQUFJLENBcUdqQjtBQXJHRCxXQUFjLElBQUk7SUFJZDtRQUlJLFlBQVksR0FBVyxFQUFFLE1BQWUsRUFBRSxJQUFhO1lBQ25ELElBQUksQ0FBQyxHQUFHLEdBQUcsR0FBRyxDQUFDO1lBQ2YsSUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLElBQUksS0FBSyxDQUFDO1lBQzlCLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxJQUFJLEVBQUUsQ0FBQztRQUMzQixDQUFDO0tBQ0o7SUFUWSxZQUFPLFVBU25CO0lBRUQ7UUFBQTtZQUVXLFlBQU8sR0FBRyxDQUFDLE9BQWdCLEVBQUUsZUFBeUIsRUFBRSxhQUF3QixFQUFRLEVBQUU7Z0JBQzdGLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQztnQkFDaEIsQ0FBQyxDQUFDLElBQUksQ0FBQztvQkFDSCxHQUFHLEVBQUUsT0FBTyxDQUFDLEdBQUc7b0JBQ2hCLElBQUksRUFBRSxPQUFPLENBQUMsTUFBTTtvQkFDcEIsSUFBSSxFQUFFLE9BQU8sQ0FBQyxJQUFJO29CQUNsQixXQUFXLEVBQUUsSUFBSTtvQkFDakIsS0FBSyxFQUFFLEtBQUs7b0JBQ1osT0FBTyxFQUFFLFVBQVUsQ0FBQzt3QkFDaEIsZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUN2QixDQUFDO29CQUNELEtBQUssRUFBRSxVQUFVLENBQUM7d0JBQ2QsRUFBRSxDQUFDLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQzs0QkFDaEIsYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDOzRCQUNqQixNQUFNLENBQUM7d0JBQ1gsQ0FBQzt3QkFDRCxJQUFJLFVBQVUsR0FBRyxZQUFZLEdBQUcsT0FBTyxDQUFDLEdBQUcsR0FBRyxHQUFHLENBQUM7d0JBQ2xELElBQUksU0FBUyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUM7d0JBQ2xDLE9BQU8sQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLENBQUM7d0JBQ3hCLE9BQU8sQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLENBQUM7b0JBQzNCLENBQUM7aUJBQ0osQ0FBQyxDQUFDO1lBQ1AsQ0FBQztZQUVNLG9CQUFlLEdBQUcsVUFBVSxPQUFnQixFQUFFLGVBQXlCLEVBQUUsZ0JBQTBCLEVBQUUsYUFBd0I7Z0JBQ2hJLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQztnQkFDaEIsQ0FBQyxDQUFDLElBQUksQ0FBQztvQkFDSCxHQUFHLEVBQUUsT0FBTyxDQUFDLEdBQUc7b0JBQ2hCLElBQUksRUFBRSxPQUFPLENBQUMsTUFBTTtvQkFDcEIsSUFBSSxFQUFFLE9BQU8sQ0FBQyxJQUFJO29CQUNsQixXQUFXLEVBQUUsS0FBSztvQkFDbEIsV0FBVyxFQUFFLEtBQUs7b0JBQ2xCLE9BQU8sRUFBRSxRQUFRO29CQUNqQixPQUFPLEVBQUUsVUFBVSxHQUFRO3dCQUN2QixlQUFlLENBQUMsR0FBRyxDQUFDLENBQUM7b0JBRXpCLENBQUM7b0JBQ0QsR0FBRyxFQUFFO3dCQUNELElBQUksR0FBRyxHQUFHLElBQUksY0FBYyxFQUFFLENBQUM7d0JBQy9CLGlCQUFpQjt3QkFDakIsR0FBRyxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxVQUFVLEVBQUUsVUFBVSxHQUFHOzRCQUNqRCxFQUFFLENBQUMsQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDO2dDQUN2QixJQUFJLGVBQWUsR0FBRyxHQUFHLENBQUMsTUFBTSxHQUFHLEdBQUcsQ0FBQyxLQUFLLENBQUM7Z0NBQzdDLG1DQUFtQztnQ0FDbkMsZ0JBQWdCLENBQUMsR0FBRyxDQUFDLENBQUM7NEJBQzFCLENBQUM7d0JBQ0wsQ0FBQyxFQUFFLEtBQUssQ0FBQyxDQUFDO3dCQUNWLG1CQUFtQjt3QkFDbkIsR0FBRyxDQUFDLGdCQUFnQixDQUFDLFVBQVUsRUFBRSxVQUFVLEdBQUc7NEJBQzFDLEVBQUUsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUM7Z0NBQ3ZCLElBQUksZUFBZSxHQUFHLEdBQUcsQ0FBQyxNQUFNLEdBQUcsR0FBRyxDQUFDLEtBQUssQ0FBQztnQ0FDN0MscUNBQXFDO2dDQUNyQyxnQkFBZ0IsQ0FBQyxHQUFHLENBQUMsQ0FBQzs0QkFDMUIsQ0FBQzt3QkFDTCxDQUFDLEVBQUUsS0FBSyxDQUFDLENBQUM7d0JBQ1YsTUFBTSxDQUFDLEdBQUcsQ0FBQztvQkFDZixDQUFDO29CQUNELEtBQUssRUFBRSxVQUFVLENBQU07d0JBQ25CLEVBQUUsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUM7NEJBQ2hCLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQzs0QkFDakIsTUFBTSxDQUFDO3dCQUNYLENBQUM7d0JBQ0QsSUFBSSxVQUFVLEdBQUcsWUFBWSxHQUFHLE9BQU8sQ0FBQyxHQUFHLEdBQUcsR0FBRyxDQUFDO3dCQUNsRCxJQUFJLFNBQVMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDO3dCQUNsQyxPQUFPLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxDQUFDO3dCQUN4QixPQUFPLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFDO29CQUMzQixDQUFDO2lCQUVKLENBQUMsQ0FBQztZQUNQLENBQUM7WUFFTSxRQUFHLEdBQUcsQ0FBQyxHQUFXLEVBQUUsZUFBeUIsRUFBRSxhQUF3QixFQUFRLEVBQUU7Z0JBQ3BGLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxPQUFPLENBQUMsR0FBRyxDQUFDLEVBQUUsZUFBZSxFQUFFLGFBQWEsQ0FBQyxDQUFDO1lBQ25FLENBQUM7WUFFTSxTQUFJLEdBQUcsQ0FBQyxHQUFXLEVBQUUsSUFBUyxFQUFFLGVBQXlCLEVBQUUsYUFBd0IsRUFBUSxFQUFFO2dCQUNoRyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksT0FBTyxDQUFDLEdBQUcsRUFBRSxNQUFNLEVBQUUsSUFBSSxDQUFDLEVBQUUsZUFBZSxFQUFFLGFBQWEsQ0FBQyxDQUFDO1lBQ2pGLENBQUM7WUFFTSxpQkFBWSxHQUFHLFVBQVUsR0FBVyxFQUFFLElBQVMsRUFBRSxlQUF5QixFQUFFLGdCQUEwQixFQUFFLGFBQXdCO2dCQUNuSSxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksT0FBTyxDQUFDLEdBQUcsRUFBRSxNQUFNLEVBQUUsSUFBSSxDQUFDLEVBQUUsZUFBZSxFQUFFLGdCQUFnQixFQUFFLGFBQWEsQ0FBQyxDQUFDO1lBQzNHLENBQUM7UUFFTCxDQUFDO0tBQUE7SUFyRlksWUFBTyxVQXFGbkI7QUFDTCxDQUFDLEVBckdhLElBQUksS0FBSixJQUFJLFFBcUdqQjs7Ozs7Ozs7OztBQ3JHSyxJQUFRLGFBQWEsQ0FrQjFCO0FBbEJELFdBQWMsYUFBYTtJQUd2QixJQUFJLGFBQWEsR0FBRyxFQUFFLE1BQU0sRUFBRSxNQUFNLEVBQUUsTUFBTSxFQUFFLE9BQU8sRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFLFVBQVUsRUFBRSxDQUFDLEVBQUUsVUFBVSxFQUFFLENBQUMsRUFBRSxDQUFDO0lBQ3JHLHNCQUFzQjtJQUV0QjtRQUNJLElBQUksQ0FBQyxJQUFhLEVBQUMsS0FBYyxFQUFFLE9BQWU7WUFDaEQsQ0FBQyxDQUFDO2dCQUNJLElBQUksTUFBTSxHQUFHLElBQUksT0FBTyxDQUFDO29CQUNsQyxLQUFLLEVBQUUsS0FBSztvQkFDWixJQUFJLEVBQUUsT0FBTztvQkFDYixJQUFJLEVBQUUsSUFBSTtpQkFDQSxDQUFDLENBQUM7WUFDVixDQUFDLENBQUMsQ0FBQztRQUNKLENBQUM7S0FDSjtJQVZZLHFCQUFPLFVBVW5CO0FBRUwsQ0FBQyxFQWxCYSxhQUFhLEtBQWIsYUFBYSxRQWtCMUI7Ozs7Ozs7OztBQ2xCSzs7OztBQUVZLGNBQU8sR0FBVyxHQUFHLENBQUM7Ozs7Ozs7Ozs7QUNGbEMsSUFBUSxZQUFZLENBY3pCO0FBZEQsV0FBYyxZQUFZO0lBRXRCO1FBQ0ksa0JBQWtCLENBQUMsSUFBWSxFQUFFLEdBQVc7WUFDeEMsRUFBRSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUM7Z0JBQUMsR0FBRyxHQUFHLE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDO1lBQ3JDLElBQUksR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsRUFBRSxNQUFNLENBQUMsQ0FBQztZQUN2QyxJQUFJLEtBQUssR0FBRyxJQUFJLE1BQU0sQ0FBQyxNQUFNLEdBQUcsSUFBSSxHQUFHLG1CQUFtQixDQUFDLEVBQ3ZELE9BQU8sR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQzlCLEVBQUUsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDO2dCQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUM7WUFDMUIsRUFBRSxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQztZQUMzQixNQUFNLENBQUMsa0JBQWtCLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsR0FBRyxDQUFDLENBQUMsQ0FBQztRQUM5RCxDQUFDO0tBQ0o7SUFWWSxtQkFBTSxTQVVsQjtBQUVMLENBQUMsRUFkYSxZQUFZLEtBQVosWUFBWSxRQWN6Qjs7Ozs7Ozs7OztBQ2RLLElBQVEsV0FBVyxDQXFDeEI7QUFyQ0QsV0FBYyxXQUFXO0lBTXJCO1FBRUksdUpBQXVKO1FBQ3ZKLElBQUksQ0FBQyxJQUFZO1lBRWIsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7WUFFaEQsd0dBQXdHO1lBQ3hHLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLEtBQUssSUFBSSxTQUFTLENBQUMsQ0FBQyxDQUFDO2dCQUM5QyxDQUFDLENBQUMsYUFBYSxDQUFDLENBQUMsRUFBRSxDQUFDLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxVQUFVLEtBQVU7b0JBQy9ELHFCQUFTLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRSxxQkFBUyxDQUFDLENBQUM7b0JBQ2pDLENBQUMsQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQzNDLENBQUMsQ0FBQyxDQUFDO2dCQUVILENBQUMsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxPQUFPLEVBQUUsVUFBVSxLQUFLO29CQUMzQyxDQUFDLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUMzQyxDQUFDLENBQUMsQ0FBQztZQUNQLENBQUM7WUFFRCxDQUFDLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQztnQkFDakIsS0FBSyxFQUFFO29CQUNILEdBQUcsRUFBRSxhQUFhO2lCQUNyQjtnQkFDRCxJQUFJLEVBQUUsUUFBUTthQUNqQixDQUFDLENBQUM7UUFJUCxDQUFDO0tBRUo7SUE5QlksaUJBQUssUUE4QmpCO0FBQ0wsQ0FBQyxFQXJDYSxXQUFXLEtBQVgsV0FBVyxRQXFDeEIiLCJmaWxlIjoiY29tbW9ucy5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIGluc3RhbGwgYSBKU09OUCBjYWxsYmFjayBmb3IgY2h1bmsgbG9hZGluZ1xuIFx0dmFyIHBhcmVudEpzb25wRnVuY3Rpb24gPSB3aW5kb3dbXCJ3ZWJwYWNrSnNvbnBcIl07XG4gXHR3aW5kb3dbXCJ3ZWJwYWNrSnNvbnBcIl0gPSBmdW5jdGlvbiB3ZWJwYWNrSnNvbnBDYWxsYmFjayhjaHVua0lkcywgbW9yZU1vZHVsZXMsIGV4ZWN1dGVNb2R1bGVzKSB7XG4gXHRcdC8vIGFkZCBcIm1vcmVNb2R1bGVzXCIgdG8gdGhlIG1vZHVsZXMgb2JqZWN0LFxuIFx0XHQvLyB0aGVuIGZsYWcgYWxsIFwiY2h1bmtJZHNcIiBhcyBsb2FkZWQgYW5kIGZpcmUgY2FsbGJhY2tcbiBcdFx0dmFyIG1vZHVsZUlkLCBjaHVua0lkLCBpID0gMCwgcmVzb2x2ZXMgPSBbXSwgcmVzdWx0O1xuIFx0XHRmb3IoO2kgPCBjaHVua0lkcy5sZW5ndGg7IGkrKykge1xuIFx0XHRcdGNodW5rSWQgPSBjaHVua0lkc1tpXTtcbiBcdFx0XHRpZihpbnN0YWxsZWRDaHVua3NbY2h1bmtJZF0pIHtcbiBcdFx0XHRcdHJlc29sdmVzLnB1c2goaW5zdGFsbGVkQ2h1bmtzW2NodW5rSWRdWzBdKTtcbiBcdFx0XHR9XG4gXHRcdFx0aW5zdGFsbGVkQ2h1bmtzW2NodW5rSWRdID0gMDtcbiBcdFx0fVxuIFx0XHRmb3IobW9kdWxlSWQgaW4gbW9yZU1vZHVsZXMpIHtcbiBcdFx0XHRpZihPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwobW9yZU1vZHVsZXMsIG1vZHVsZUlkKSkge1xuIFx0XHRcdFx0bW9kdWxlc1ttb2R1bGVJZF0gPSBtb3JlTW9kdWxlc1ttb2R1bGVJZF07XG4gXHRcdFx0fVxuIFx0XHR9XG4gXHRcdGlmKHBhcmVudEpzb25wRnVuY3Rpb24pIHBhcmVudEpzb25wRnVuY3Rpb24oY2h1bmtJZHMsIG1vcmVNb2R1bGVzLCBleGVjdXRlTW9kdWxlcyk7XG4gXHRcdHdoaWxlKHJlc29sdmVzLmxlbmd0aCkge1xuIFx0XHRcdHJlc29sdmVzLnNoaWZ0KCkoKTtcbiBcdFx0fVxuIFx0XHRpZihleGVjdXRlTW9kdWxlcykge1xuIFx0XHRcdGZvcihpPTA7IGkgPCBleGVjdXRlTW9kdWxlcy5sZW5ndGg7IGkrKykge1xuIFx0XHRcdFx0cmVzdWx0ID0gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSBleGVjdXRlTW9kdWxlc1tpXSk7XG4gXHRcdFx0fVxuIFx0XHR9XG4gXHRcdHJldHVybiByZXN1bHQ7XG4gXHR9O1xuXG4gXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBvYmplY3RzIHRvIHN0b3JlIGxvYWRlZCBhbmQgbG9hZGluZyBjaHVua3NcbiBcdHZhciBpbnN0YWxsZWRDaHVua3MgPSB7XG4gXHRcdDg6IDBcbiBcdH07XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwge1xuIFx0XHRcdFx0Y29uZmlndXJhYmxlOiBmYWxzZSxcbiBcdFx0XHRcdGVudW1lcmFibGU6IHRydWUsXG4gXHRcdFx0XHRnZXQ6IGdldHRlclxuIFx0XHRcdH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG4gXHQvLyBvbiBlcnJvciBmdW5jdGlvbiBmb3IgYXN5bmMgbG9hZGluZ1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vZSA9IGZ1bmN0aW9uKGVycikgeyBjb25zb2xlLmVycm9yKGVycik7IHRocm93IGVycjsgfTtcblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSAxMyk7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gd2VicGFjay9ib290c3RyYXAgNTkzMjgwYWIxMDgyNjM3MDVmMDEiLCJleHBvcnQgbW9kdWxlIEFqYXgge1xyXG5cclxuICAgIGRlY2xhcmUgdmFyICQ6IGFueTtcclxuXHJcbiAgICBleHBvcnQgY2xhc3MgT3B0aW9ucyB7XHJcbiAgICAgICAgdXJsOiBzdHJpbmc7XHJcbiAgICAgICAgbWV0aG9kOiBzdHJpbmc7XHJcbiAgICAgICAgZGF0YTogT2JqZWN0O1xyXG4gICAgICAgIGNvbnN0cnVjdG9yKHVybDogc3RyaW5nLCBtZXRob2Q/OiBzdHJpbmcsIGRhdGE/OiBPYmplY3QpIHtcclxuICAgICAgICAgICAgdGhpcy51cmwgPSB1cmw7XHJcbiAgICAgICAgICAgIHRoaXMubWV0aG9kID0gbWV0aG9kIHx8IFwiZ2V0XCI7XHJcbiAgICAgICAgICAgIHRoaXMuZGF0YSA9IGRhdGEgfHwge307XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGV4cG9ydCBjbGFzcyBTZXJ2aWNlIHtcclxuXHJcbiAgICAgICAgcHVibGljIHJlcXVlc3QgPSAob3B0aW9uczogT3B0aW9ucywgc3VjY2Vzc0NhbGxiYWNrOiBGdW5jdGlvbiwgZXJyb3JDYWxsYmFjaz86IEZ1bmN0aW9uKTogdm9pZCA9PiB7XHJcbiAgICAgICAgICAgIHZhciB0aGF0ID0gdGhpcztcclxuICAgICAgICAgICAgJC5hamF4KHtcclxuICAgICAgICAgICAgICAgIHVybDogb3B0aW9ucy51cmwsXHJcbiAgICAgICAgICAgICAgICB0eXBlOiBvcHRpb25zLm1ldGhvZCxcclxuICAgICAgICAgICAgICAgIGRhdGE6IG9wdGlvbnMuZGF0YSxcclxuICAgICAgICAgICAgICAgIHRyYWRpdGlvbmFsOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgY2FjaGU6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgc3VjY2VzczogZnVuY3Rpb24gKGQpIHtcclxuICAgICAgICAgICAgICAgICAgICBzdWNjZXNzQ2FsbGJhY2soZCk7XHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgZXJyb3I6IGZ1bmN0aW9uIChkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKGVycm9yQ2FsbGJhY2spIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZXJyb3JDYWxsYmFjayhkKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB2YXIgZXJyb3JUaXRsZSA9IFwiRXJyb3IgaW4gKFwiICsgb3B0aW9ucy51cmwgKyBcIilcIjtcclxuICAgICAgICAgICAgICAgICAgICB2YXIgZnVsbEVycm9yID0gSlNPTi5zdHJpbmdpZnkoZCk7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coZXJyb3JUaXRsZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coZnVsbEVycm9yKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBwdWJsaWMgcmVxdWVzdEZvcm1kYXRhID0gZnVuY3Rpb24gKG9wdGlvbnM6IE9wdGlvbnMsIHN1Y2Nlc3NDYWxsYmFjazogRnVuY3Rpb24sIHByb2dyZXNzQ2FsbGJhY2s6IEZ1bmN0aW9uLCBlcnJvckNhbGxiYWNrPzogRnVuY3Rpb24pIHtcclxuICAgICAgICAgICAgdmFyIHRoYXQgPSB0aGlzO1xyXG4gICAgICAgICAgICAkLmFqYXgoe1xyXG4gICAgICAgICAgICAgICAgdXJsOiBvcHRpb25zLnVybCxcclxuICAgICAgICAgICAgICAgIHR5cGU6IG9wdGlvbnMubWV0aG9kLFxyXG4gICAgICAgICAgICAgICAgZGF0YTogb3B0aW9ucy5kYXRhLFxyXG4gICAgICAgICAgICAgICAgcHJvY2Vzc0RhdGE6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgY29udGVudFR5cGU6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgdGltZW91dDogMTAwMDAwMDAsXHJcbiAgICAgICAgICAgICAgICBzdWNjZXNzOiBmdW5jdGlvbiAoeGhyOiBhbnkpIHtcclxuICAgICAgICAgICAgICAgICAgICBzdWNjZXNzQ2FsbGJhY2soeGhyKTtcclxuXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgeGhyOiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdmFyIHhociA9IG5ldyBYTUxIdHRwUmVxdWVzdCgpO1xyXG4gICAgICAgICAgICAgICAgICAgIC8vVXBsb2FkIHByb2dyZXNzXHJcbiAgICAgICAgICAgICAgICAgICAgeGhyLnVwbG9hZC5hZGRFdmVudExpc3RlbmVyKFwicHJvZ3Jlc3NcIiwgZnVuY3Rpb24gKGV2dCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoZXZ0Lmxlbmd0aENvbXB1dGFibGUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciBwZXJjZW50Q29tcGxldGUgPSBldnQubG9hZGVkIC8gZXZ0LnRvdGFsO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLy9EbyBzb21ldGhpbmcgd2l0aCB1cGxvYWQgcHJvZ3Jlc3NcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHByb2dyZXNzQ2FsbGJhY2soZXZ0KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH0sIGZhbHNlKTtcclxuICAgICAgICAgICAgICAgICAgICAvL0Rvd25sb2FkIHByb2dyZXNzXHJcbiAgICAgICAgICAgICAgICAgICAgeGhyLmFkZEV2ZW50TGlzdGVuZXIoXCJwcm9ncmVzc1wiLCBmdW5jdGlvbiAoZXZ0KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChldnQubGVuZ3RoQ29tcHV0YWJsZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIHBlcmNlbnRDb21wbGV0ZSA9IGV2dC5sb2FkZWQgLyBldnQudG90YWw7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAvL0RvIHNvbWV0aGluZyB3aXRoIGRvd25sb2FkIHByb2dyZXNzXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBwcm9ncmVzc0NhbGxiYWNrKGV2dCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9LCBmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHhocjtcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICBlcnJvcjogZnVuY3Rpb24gKGQ6IGFueSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChlcnJvckNhbGxiYWNrKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGVycm9yQ2FsbGJhY2soZCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgdmFyIGVycm9yVGl0bGUgPSBcIkVycm9yIGluIChcIiArIG9wdGlvbnMudXJsICsgXCIpXCI7XHJcbiAgICAgICAgICAgICAgICAgICAgdmFyIGZ1bGxFcnJvciA9IEpTT04uc3RyaW5naWZ5KGQpO1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGVycm9yVGl0bGUpO1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGZ1bGxFcnJvcik7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHB1YmxpYyBnZXQgPSAodXJsOiBzdHJpbmcsIHN1Y2Nlc3NDYWxsYmFjazogRnVuY3Rpb24sIGVycm9yQ2FsbGJhY2s/OiBGdW5jdGlvbik6IHZvaWQgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLnJlcXVlc3QobmV3IE9wdGlvbnModXJsKSwgc3VjY2Vzc0NhbGxiYWNrLCBlcnJvckNhbGxiYWNrKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHB1YmxpYyBwb3N0ID0gKHVybDogc3RyaW5nLCBkYXRhOiBhbnksIHN1Y2Nlc3NDYWxsYmFjazogRnVuY3Rpb24sIGVycm9yQ2FsbGJhY2s/OiBGdW5jdGlvbik6IHZvaWQgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLnJlcXVlc3QobmV3IE9wdGlvbnModXJsLCBcInBvc3RcIiwgZGF0YSksIHN1Y2Nlc3NDYWxsYmFjaywgZXJyb3JDYWxsYmFjayk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBwdWJsaWMgcG9zdEZvcm1kYXRhID0gZnVuY3Rpb24gKHVybDogc3RyaW5nLCBkYXRhOiBhbnksIHN1Y2Nlc3NDYWxsYmFjazogRnVuY3Rpb24sIHByb2dyZXNzQ2FsbGJhY2s6IEZ1bmN0aW9uLCBlcnJvckNhbGxiYWNrPzogRnVuY3Rpb24pIHtcclxuICAgICAgICAgICAgdGhpcy5yZXF1ZXN0Rm9ybWRhdGEobmV3IE9wdGlvbnModXJsLCBcInBvc3RcIiwgZGF0YSksIHN1Y2Nlc3NDYWxsYmFjaywgcHJvZ3Jlc3NDYWxsYmFjaywgZXJyb3JDYWxsYmFjayk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgIH1cclxufVxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuLi9UeXBlU2NyaXB0L0NvbW1vbi9BamF4LnRzIiwiZXhwb3J0IG1vZHVsZSBNZXNzYWdlTW9kdWxlIHtcclxuICAgIGRlY2xhcmUgdmFyICQ6IGFueTtcclxuICAgIGRlY2xhcmUgdmFyIFBOb3RpZnk6IGFueTtcclxuICAgIHZhciBzdGFja19iYXJfdG9wID0geyBcImRpcjFcIjogXCJkb3duXCIsIFwiZGlyMlwiOiBcInJpZ2h0XCIsIFwicHVzaFwiOiBcInRvcFwiLCBcInNwYWNpbmcxXCI6IDAsIFwic3BhY2luZzJcIjogMCB9O1xyXG4gICAgLy9UeXBlIDogZXJyb3Isc3VjY2Vzc1xyXG5cclxuICAgIGV4cG9ydCBjbGFzcyBNZXNzYWdlIHtcclxuICAgICAgICBzaG93KHR5cGUgOiBzdHJpbmcsdGl0bGUgOiBzdHJpbmcsIG1lc3NhZ2U6IHN0cmluZykge1xyXG4gICAgICAgICAgJChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICB2YXIgbm90aWNlID0gbmV3IFBOb3RpZnkoe1xyXG5cdFx0XHQgICAgdGl0bGU6IHRpdGxlLFxyXG5cdFx0XHQgICAgdGV4dDogbWVzc2FnZSxcclxuXHRcdFx0ICAgIHR5cGU6IHR5cGUsXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxufVxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuLi9UeXBlU2NyaXB0L0NvbW1vbi9NZXNzYWdlLnRzIiwiZXhwb3J0IGNsYXNzIENvbmZpZ1xyXG4ge1xyXG4gICAgcHVibGljIHN0YXRpYyBCYXNlVXJsOiBzdHJpbmcgPSAnLyc7XHJcbiB9XG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4uL1R5cGVTY3JpcHQvQ29tbW9uL0NvbmZpZ1ZhcmlhYmxlLnRzIiwiZXhwb3J0IG1vZHVsZSBIZWxwZXJNb2R1bGUge1xyXG5cclxuICAgIGV4cG9ydCBjbGFzcyBIZWxwZXIge1xyXG4gICAgICAgIGdldFBhcmFtZXRlckJ5TmFtZShuYW1lOiBzdHJpbmcsIHVybDogc3RyaW5nKSB7XHJcbiAgICAgICAgICAgIGlmICghdXJsKSB1cmwgPSB3aW5kb3cubG9jYXRpb24uaHJlZjtcclxuICAgICAgICAgICAgbmFtZSA9IG5hbWUucmVwbGFjZSgvW1xcW1xcXV0vZywgXCJcXFxcJCZcIik7XHJcbiAgICAgICAgICAgIHZhciByZWdleCA9IG5ldyBSZWdFeHAoXCJbPyZdXCIgKyBuYW1lICsgXCIoPShbXiYjXSopfCZ8I3wkKVwiKSxcclxuICAgICAgICAgICAgICAgIHJlc3VsdHMgPSByZWdleC5leGVjKHVybCk7XHJcbiAgICAgICAgICAgIGlmICghcmVzdWx0cykgcmV0dXJuIG51bGw7XHJcbiAgICAgICAgICAgIGlmICghcmVzdWx0c1syXSkgcmV0dXJuICcnO1xyXG4gICAgICAgICAgICByZXR1cm4gZGVjb2RlVVJJQ29tcG9uZW50KHJlc3VsdHNbMl0ucmVwbGFjZSgvXFwrL2csIFwiIFwiKSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxufVxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuLi9UeXBlU2NyaXB0L0NvbW1vbi9IZWxwZXIudHMiLCJleHBvcnQgbW9kdWxlIE1vZGFsTW9kdWxlIHtcclxuICAgIGRlY2xhcmUgdmFyICQ6IGFueTtcclxuICAgIGRlY2xhcmUgdmFyIG1hZ25pZmljUG9wdXA6IGFueTtcclxuICAgIGV4cG9ydCBkZWNsYXJlIHZhciBtb2RhbEZ1bmM6IEZ1bmN0aW9uO1xyXG4gICAgZXhwb3J0IGRlY2xhcmUgdmFyIG1vZGFsQXJnczogYW55W107XHJcblxyXG4gICAgZXhwb3J0IGNsYXNzIE1vZGFsIHtcclxuXHJcbiAgICAgICAgLy9zaG93KGZ1bmM6IEZ1bmN0aW9uLCBhcmdzOiBhbnlbXSwgaHRtbDogc3RyaW5nKSB7IGJ1IHlhcMSxIGRlxJ9pxZ90aXJpbGRpIMOnw7xua8O8IHNheWZhIGZhcmtsxLEgYmlyZGVuIGZhemxhIHBvcHVwIMOnYcSfxLFyxLFsZMSxxJ/EsW5kYSBwYXJhbWV0cmVsZXIgZXNraSBnZWxpeW9yXHJcbiAgICAgICAgc2hvdyhodG1sOiBzdHJpbmcpIHtcclxuXHJcbiAgICAgICAgICAgICQoJyNNb2RhbEJhc2ljJykuZmluZCgnLm1vZGFsLXRleHQnKS5odG1sKGh0bWwpO1xyXG5cclxuICAgICAgICAgICAgLy9CdSBrb250cm9sIHBvcHVwIGJpcmRlbiBmYXpsYSB0xLFrbGFtYSBvbHV5b3JkdSBvbmRhbiBkb2xhecSxIHlhcMSxbGTEsSB0ZWtyYXIgYnV0b25hIGJhc8SxbGTEscSfxLEgZHVydW1sYXJkYVxyXG4gICAgICAgICAgICBpZiAoJC5tYWduaWZpY1BvcHVwLmluc3RhbmNlLml0ZW1zID09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgICAgICAgICAgJChcIiNNb2RhbEJhc2ljXCIpLm9uKCdjbGljaycsICcubW9kYWwtY29uZmlybScsIGZ1bmN0aW9uIChldmVudDogYW55KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgbW9kYWxGdW5jLmFwcGx5KG51bGwsIG1vZGFsQXJncyk7XHJcbiAgICAgICAgICAgICAgICAgICAgJC5tYWduaWZpY1BvcHVwLnByb3RvLmNsb3NlLmNhbGwodGhpcyk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgICAgICAkKCcubW9kYWwtZGlzbWlzcycpLm9uKCdjbGljaycsIGZ1bmN0aW9uIChldmVudCkge1xyXG4gICAgICAgICAgICAgICAgICAgICQubWFnbmlmaWNQb3B1cC5wcm90by5jbG9zZS5jYWxsKHRoaXMpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICQubWFnbmlmaWNQb3B1cC5vcGVuKHtcclxuICAgICAgICAgICAgICAgIGl0ZW1zOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgc3JjOiAnI01vZGFsQmFzaWMnXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgdHlwZTogJ2lubGluZScsXHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuXHJcblxyXG4gICAgICAgIH1cclxuXHJcbiAgICB9XHJcbn1cclxuXHJcblxyXG5cclxuXHJcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuLi9UeXBlU2NyaXB0L0NvbW1vbi9Nb2RhbC50cyJdLCJzb3VyY2VSb290IjoiIn0=