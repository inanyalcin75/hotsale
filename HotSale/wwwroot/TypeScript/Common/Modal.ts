﻿export module ModalModule {
    declare var $: any;
    declare var magnificPopup: any;
    export declare var modalFunc: Function;
    export declare var modalArgs: any[];

    export class Modal {

        //show(func: Function, args: any[], html: string) { bu yapı değiştirildi çünkü sayfa farklı birden fazla popup çağırıldığında parametreler eski geliyor
        show(html: string) {

            $('#ModalBasic').find('.modal-text').html(html);

            //Bu kontrol popup birden fazla tıklama oluyordu ondan dolayı yapıldı tekrar butona basıldığı durumlarda
            if ($.magnificPopup.instance.items == undefined) {
                $("#ModalBasic").on('click', '.modal-confirm', function (event: any) {
                    modalFunc.apply(null, modalArgs);
                    $.magnificPopup.proto.close.call(this);
                });

                $('.modal-dismiss').on('click', function (event) {
                    $.magnificPopup.proto.close.call(this);
                });
            }

            $.magnificPopup.open({
                items: {
                    src: '#ModalBasic'
                },
                type: 'inline',
            });



        }

    }
}




