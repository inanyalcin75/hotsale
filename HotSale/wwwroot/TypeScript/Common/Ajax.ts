﻿export module Ajax {

    declare var $: any;

    export class Options {
        url: string;
        method: string;
        data: Object;
        constructor(url: string, method?: string, data?: Object) {
            this.url = url;
            this.method = method || "get";
            this.data = data || {};
        }
    }

    export class Service {

        public request = (options: Options, successCallback: Function, errorCallback?: Function): void => {
            var that = this;
            $.ajax({
                url: options.url,
                type: options.method,
                data: options.data,
                traditional: true,
                cache: false,
                success: function (d) {
                    successCallback(d);
                },
                error: function (d) {
                    if (errorCallback) {
                        errorCallback(d);
                        return;
                    }
                    var errorTitle = "Error in (" + options.url + ")";
                    var fullError = JSON.stringify(d);
                    console.log(errorTitle);
                    console.log(fullError);
                }
            });
        }

        public requestFormdata = function (options: Options, successCallback: Function, progressCallback: Function, errorCallback?: Function) {
            var that = this;
            $.ajax({
                url: options.url,
                type: options.method,
                data: options.data,
                processData: false,
                contentType: false,
                timeout: 10000000,
                success: function (xhr: any) {
                    successCallback(xhr);

                },
                xhr: function () {
                    var xhr = new XMLHttpRequest();
                    //Upload progress
                    xhr.upload.addEventListener("progress", function (evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = evt.loaded / evt.total;
                            //Do something with upload progress
                            progressCallback(evt);
                        }
                    }, false);
                    //Download progress
                    xhr.addEventListener("progress", function (evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = evt.loaded / evt.total;
                            //Do something with download progress
                            progressCallback(evt);
                        }
                    }, false);
                    return xhr;
                },
                error: function (d: any) {
                    if (errorCallback) {
                        errorCallback(d);
                        return;
                    }
                    var errorTitle = "Error in (" + options.url + ")";
                    var fullError = JSON.stringify(d);
                    console.log(errorTitle);
                    console.log(fullError);
                }

            });
        }

        public get = (url: string, successCallback: Function, errorCallback?: Function): void => {
            this.request(new Options(url), successCallback, errorCallback);
        }

        public post = (url: string, data: any, successCallback: Function, errorCallback?: Function): void => {
            this.request(new Options(url, "post", data), successCallback, errorCallback);
        }

        public postFormdata = function (url: string, data: any, successCallback: Function, progressCallback: Function, errorCallback?: Function) {
            this.requestFormdata(new Options(url, "post", data), successCallback, progressCallback, errorCallback);
        }

    }
}