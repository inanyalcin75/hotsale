﻿export module MessageModule {
    declare var $: any;
    declare var PNotify: any;
    var stack_bar_top = { "dir1": "down", "dir2": "right", "push": "top", "spacing1": 0, "spacing2": 0 };
    //Type : error,success

    export class Message {
        show(type : string,title : string, message: string) {
          $(function () {
                var notice = new PNotify({
			    title: title,
			    text: message,
			    type: type,
                });
         });
        }
    }

}