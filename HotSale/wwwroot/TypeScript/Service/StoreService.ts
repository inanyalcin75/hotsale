﻿import { ModalModule } from "../Common/Modal";
import { Ajax } from "../Common/Ajax";
import { Config } from "../Common/ConfigVariable";
import { MessageModule } from "../Common/Message";

//Business
export module StoreModule {
    declare var $: any;
    export class Store {

        storeadd(ProductId: string, ProductCode: string, ProductName: string, Price: string, Quantity: string, Tax: string, ProductUnit: string, ProductUnitRate: string) {

            var message = new MessageModule.Message();

            if (Quantity == "") {
                message.show("error", "Bildirim", "Miktar veya adet giriniz.");
                return false;
            }

            $(".row-products").trigger('loading-overlay:show');

            var ajaxPost = new Ajax.Service();

            var object = { productId: ProductId, productCode: ProductCode, productName: ProductName, price: Price, quantity: Quantity, tax: Tax, productUnit: ProductUnit, productUnitRate: ProductUnitRate };

            var url = "/Store/AddStores";
            ajaxPost.post(url, object, function Success(data: any) {
                $(".row-products").trigger('loading-overlay:hide');
                message.show("success", "Notify", "Ürün eklendi.");
                $(".number-store-" + ProductId).val("");
            }, function Error(data: any) {
                message.show("error", "Notify", "is problems");
                $(".row-products").trigger('loading-overlay:hide');
            });
        }

        storeCacheList() {

            var message = new MessageModule.Message();
            $(".row-store-cache").trigger('loading-overlay:show');

            var ajaxPost = new Ajax.Service();

            var object = {};

            var url = "/Store/GetCacheStores";
            ajaxPost.post(url, object, function Success(data: any) {

                $(".row-store-cache").trigger('loading-overlay:hide');
                $(".row-store-cache").html(data);

            }, function Error(data: any) {
                message.show("error", "Bildirim", "is problems");
                $(".row-store-cache").trigger('loading-overlay:hide');
            });


        }

        storeList(Term: string, PageNo: number, ListCount: number, startdate: string, finishdate: string) {
            var message = new MessageModule.Message();
            $(".row-stores").trigger('loading-overlay:show');

            var ajaxPost = new Ajax.Service();

            var object = { term: Term, pageNo: PageNo, listCount: ListCount, startDate: startdate, finishDate: finishdate };

            var url = "/Store/GetStores";
            ajaxPost.post(url, object, function Success(data: any) {

                $(".row-stores").trigger('loading-overlay:hide');
                $(".row-stores").html(data);
                new StoreModule.Store().calandertrigger();

            }, function Error(data: any) {
                message.show("error", "Notify", "is problems");
                $(".row-stores").trigger('loading-overlay:hide');
            });

        }

        productlist(Term: string, PageNo: number, ListCount: number) {
            var message = new MessageModule.Message();
            $(".row-products").trigger('loading-overlay:show');

            var ajaxPost = new Ajax.Service();

            var object = {term: Term, pageNo: PageNo, listCount: ListCount };

            var url = "/Store/GetStoreProducts";
            ajaxPost.post(url, object, function Success(data: any) {

                $(".row-products").trigger('loading-overlay:hide');
                $(".row-products").html(data);
                $(".order-product-search").focus();
                var strLength = $(".order-product-search").val().length * 2;
                $(".order-product-search")[0].setSelectionRange(strLength, strLength);


            }, function Error(data: any) {
                message.show("error", "Notify", "is problems");
                $(".row-products").trigger('loading-overlay:hide');
            });

        }

        storeCacheProductDelete(ProductId) {

            var message = new MessageModule.Message();
            $(".row-store-cache").trigger('loading-overlay:show');

            var ajaxPost = new Ajax.Service();

            var object = { productId: ProductId };

            var url = "/Store/DeleteCacheStoresProduct";
            ajaxPost.post(url, object, function Success(data: any) {

                $(".row-store-cache").trigger('loading-overlay:hide');
                $(".row-store-cache").html(data);

                new StoreModule.Store().storeCacheList();
            }, function Error(data: any) {
                message.show("error", "Notify", "is problems");
                $(".row-store-cache").trigger('loading-overlay:hide');
            });
        }

        storeCacheProductAllDelete() {

            var message = new MessageModule.Message();
            $(".row-store-cache").trigger('loading-overlay:show');

            var ajaxPost = new Ajax.Service();

            var object = {};

            var url = "/Store/DeleteCacheStoresProductAll";
            ajaxPost.post(url, object, function Success(data: any) {

                $(".row-store-cache").trigger('loading-overlay:hide');
                $(".row-store-cache").html(data);

                new StoreModule.Store().storeCacheList();
            }, function Error(data: any) {
                message.show("error", "Bildirim", "is problems");
                $(".row-store-cache").trigger('loading-overlay:hide');
            });
        }

        storeSave() {

            var message = new MessageModule.Message();
            $(".row-store-cache").trigger('loading-overlay:show');

            var storecode = $(".slct-store").val();

            var ajaxPost = new Ajax.Service();

            var object = { storeCode: storecode };

            var url = "/Store/SaveStores";
            ajaxPost.post(url, object, function Success(data: any) {
                $(".row-store-cache").trigger('loading-overlay:hide');

                if (data.data) {
                    message.show("success", "Bildirim", "Depo fişi oluşturuldu.");
                    $(".store-list").click();
                }
                else {
                    message.show("error", "Bildirim", "Depo fişiniz oluşturulamadı");

                }

            }, function Error(data: any) {
                message.show("error", "Bildirim", "Depo fişiniz oluştururken sorun oluştu");
                $(".row-store-cache").trigger('loading-overlay:hide');
            });
        }

        calandertrigger() {

            $('[data-plugin-datepicker]').each(function () {
                var $this = $(this),
                    opts = {};

                var pluginOptions = $this.data('plugin-options');
                if (pluginOptions)
                    opts = pluginOptions;

                $this.themePluginDatePicker(opts);
            });

        }
    }
}
//Business




//Frontend
declare var $: any;
$(function () {

    /*
     Page Load
    */
    $(document).ready(function () {

        $('.row-products').on('click', '.btn-store-add', function () {
            var quantity = $(this).parents("tr").find(".number-store").val();
            var productId = $(this).parents("tr").find(".number-store").attr("ProductId");
            var productName = $(this).parents("tr").find(".number-store").attr("ProductName");
            var productPrice = $(this).parents("tr").find(".number-store").attr("ProductPrice");
            var productTax = $(this).parents("tr").find(".number-store").attr("ProductTax");

            var productCode = $(this).parents("tr").find(".number-store").attr("ProductCode");
            var productUnit = $(this).parents("tr").find(".number-store").attr("ProductUnit");
            var productUnitRate = $(this).parents("tr").find(".number-store").attr("ProductUnitRate");

            new StoreModule.Store().storeadd(productId, productCode, productName, productPrice, quantity, productTax, productUnit, productUnitRate);
        });

        $('.row-products').on('click', '.btn-store-create', function () {
            new StoreModule.Store().storeCacheList();
        });

        $('.store-list').on('click', function (event: any) {
            var term = $(".store-search").val();
            var startDate = "01.01.1990";
            var finishDate = new Date().toLocaleDateString();
            var listCount = 8;
            new StoreModule.Store().storeList(term, 1, listCount, startDate, finishDate);
        });

        //Store page List
        $('body').on('click', '.btn-order-list', function (event: any) {
            var term = $(".order-search").val();
            var startDate = $('#txt-company-startDate').val();
            var finishDate = $('#txt-company-finishDate').val();
            var pageNo = 1;
            var listCount = 8;
            new StoreModule.Store().storeList(term, pageNo, listCount, startDate, finishDate);
        });

        //Store page change List
        $('body').on('click', '.btn-order-page-change', function (event: any) {
            var term = $(".order-search").val();
            var startDate = $('#txt-company-startDate').val();
            var finishDate = $('#txt-company-finishDate').val();
            var pageNo = $(this).attr("pageNo");
            var listCount = 8;
            new StoreModule.Store().storeList(term, pageNo, listCount, startDate, finishDate);
        });

        $('.row-stores').on('click', '.fa-selected', function () {
            if ($(this).hasClass("fa-minus-square-o")) {
                $(this).removeClass('fa-minus-square-o').addClass('fa-plus-square-o');
                $(this).parents(".selectedtr").next('.details ').fadeOut().addClass("hide");
            }
            else {
                $(this).removeClass('fa-plus-square-o').addClass('fa-minus-square-o');
                $(this).parents(".selectedtr").next('.details ').fadeIn().removeClass("hide");

            }
        });

        $('.row-store-cache').on('click', '.btn-store-product-delete', function () {

            var productId = $(this).attr("productId")
            var arr = [];
            arr.push(productId);
            ModalModule.modalFunc = new StoreModule.Store().storeCacheProductDelete;
            ModalModule.modalArgs = arr;
            new ModalModule.Modal().show("Ürünü silmek istediğinize eminmisiniz ?");
        });

        $('.row-store-cache').on('click', '.btn-store-clear', function () {
            ModalModule.modalFunc = new StoreModule.Store().storeCacheProductAllDelete;
            ModalModule.modalArgs = null;
            new ModalModule.Modal().show("Ürünler tamamen silinmesi istediğinize eminmisiniz ?");
        });

        $('.row-store-cache').on('click', '.btn-store-save', function () {
            ModalModule.modalFunc = new StoreModule.Store().storeSave;
            ModalModule.modalArgs = null;
            new ModalModule.Modal().show("Bu Depo kaydı gerçekleştirilmesini istiyormusunuz ?");
        });
        

        //search event
        $('.row-products').on('keydown', '.order-product-search', function (e) {
            if (e.which == 13) {
                var term = $(".order-product-search").val();
                var listCount = 8;
                new StoreModule.Store().productlist(term, 1, listCount);
            }
        });

    });




});
//Frontend
