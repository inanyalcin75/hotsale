﻿import { ModalModule } from "../Common/Modal";
import { Ajax } from "../Common/Ajax";
import { Config } from "../Common/ConfigVariable";
import { MessageModule } from "../Common/Message";

//Business
export module OrderSpecialModule {
    declare var $: any;
    export class OrderSpecial {

        public OrderSave(data: any, successCall: Function, progressCall: Function, errorCall: Function) {
            var sx = new Ajax.Service();
            sx.postFormdata(Config.BaseUrl + "SpecialOrder/SaveSpecialOrders", data, successCall, progressCall, errorCall);
        }

    }
}
//Business



//Frontend
declare var $: any;
$(function () {

    /*
     Page Load
    */
    $(document).ready(function () {

        //Special Order Action Save
        $('body').on('submit', '.orderspecial-save-form', function (e: any) {

            e.preventDefault();
            $(".panel-loading").trigger('loading-overlay:show');
            $(".btn-orderspecial-save-form").hide();

            var form = $(this);
            var formdata = new FormData(form[0]);

            new OrderSpecialModule.OrderSpecial().OrderSave(formdata, function Success(data: any) {
                $(".panel-loading").trigger('loading-overlay:hide');

                if (data.data == true) {
                    $('.orderspecial-save-form').trigger('reset');
                    new MessageModule.Message().show("success", "Notification", "kayıt yapıldı.");
                }
                else {
                    $(".btn-orderspecial-save-form").show();
                    new MessageModule.Message().show("error", "Notification", "işlem yapılamadı");
                }

            }, function Progress(e: any) { }, function Error(data: any) {
                $(".btn-orderspecial-save-form").show();
                $(".panel-loading").trigger('loading-overlay:hide');
                new MessageModule.Message().show("error", "Notification", "ajax is problems");
            });


        });

        //Special Order Stock
        $('#select-stock').select2({
            minimumInputLength: 2,
            language: {
                inputTooShort: function () {
                    return 'Arama için en az 2 karater giriniz';
                }
            },
            ajax: {
                url: '/SpecialOrder/GetStock',
                data: function (params) {
                    var query = {
                        search: params.term
                    }

                    // Query parameters will be ?search=[term]&type=public
                    return query;
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            console.log(item.stockCode);
                            return {
                                text: item.stockName,
                                id: item.stockCode
                            }
                        })
                    };
                }
            }

        });





    });

});
//Frontend