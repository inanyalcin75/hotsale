﻿import { ModalModule } from "../Common/Modal";
import { Ajax } from "../Common/Ajax";
import { Config } from "../Common/ConfigVariable";
import { MessageModule } from "../Common/Message";

//Business
export module OrderModule {
    declare var $: any;
    export class Order {

        productlist(ProductGroupId: string, Term: string, PageNo: number, ListCount: number) {
            var message = new MessageModule.Message();
            $(".row-products").trigger('loading-overlay:show');

            var ajaxPost = new Ajax.Service();

            var object = { productGroupId: ProductGroupId, term: Term, pageNo: PageNo, listCount: ListCount };

            var url = "/Order/GetOrderProducts";
            ajaxPost.post(url, object, function Success(data: any) {

                $(".row-products").trigger('loading-overlay:hide');
                $(".row-products").html(data);
                $('.number-Order').keyboard({ type: 'numpad', placement: 'top' });
                $(".order-product-search").focus();
                var strLength = $(".order-product-search").val().length * 2;
                $(".order-product-search")[0].setSelectionRange(strLength, strLength);

            }, function Error(data: any) {
                message.show("error", "Bildirim", "is problems");
                $(".row-products").trigger('loading-overlay:hide');
            });

        }

        productItemlist(ProductGroupId: string, Term: string, PageNo: number, ListCount: number) {
            var message = new MessageModule.Message();
            $(".row-products").trigger('loading-overlay:show');

            var ajaxPost = new Ajax.Service();

            var object = { productGroupId: ProductGroupId, term: Term, pageNo: PageNo, listCount: ListCount };

            var url = "/Order/GetOrderProductsItem";
            ajaxPost.post(url, object, function Success(data: any) {
                $(".row-products .loading-order-products").remove();
                $(".row-products").trigger('loading-overlay:hide');
                var allData = $(".row-productsItem").html() + data;
                $(".row-productsItem").html(allData);
                $('.number-Order').keyboard({ type: 'numpad', placement: 'top' });
            }, function Error(data: any) {
                message.show("error", "Bildirim", "is problems");
                $(".row-products").trigger('loading-overlay:hide');
            });

        }

        productImageSave(data: any, successCall: Function, progressCall: Function, errorCall: Function) {

            var ajaxPost = new Ajax.Service();
            ajaxPost.postFormdata("/Order/SaveImage", data, successCall, progressCall, errorCall);
        }

        orderlist(Term: string, PageNo: number, ListCount: number, startdate: string, finishdate: string) {
            var message = new MessageModule.Message();
            $(".row-orders").trigger('loading-overlay:show');

            var ajaxPost = new Ajax.Service();

            var object = { term: Term, pageNo: PageNo, listCount: ListCount, startDate: startdate, finishDate: finishdate };

            var url = "/Order/GetOrders";
            ajaxPost.post(url, object, function Success(data: any) {

                $(".row-orders").trigger('loading-overlay:hide');
                $(".row-orders").html(data);
                new OrderModule.Order().calandertrigger();

            }, function Error(data: any) {
                message.show("error", "Bildirim", "is problems");
                $(".row-orders").trigger('loading-overlay:hide');
            });

        }

        orderadd(ProductId: string, ProductCode: string, ProductName: string, Price: string, Quantity: string, Tax: string, ProductUnit: string, ProductUnitRate: string) {

            var message = new MessageModule.Message();

            if (Quantity == "") {
                message.show("error", "Bildirim", "Miktar giriniz.");
                return false;
            }

            $(".row-products").trigger('loading-overlay:show');

            var ajaxPost = new Ajax.Service();

            var object = { productId: ProductId, productCode: ProductCode, productName: ProductName, price: Price, quantity: Quantity, tax: Tax, productUnit: ProductUnit, productUnitRate: ProductUnitRate };

            var url = "/Order/AddOrders";
            ajaxPost.post(url, object, function Success(data: any) {
                $(".row-products").trigger('loading-overlay:hide');
                message.show("success", "Bildirim", "Ürün eklendi.");
                $(".number-Order-" + ProductId).val("");
            }, function Error(data: any) {
                message.show("error", "Bildirim", "is problems");
                $(".row-products").trigger('loading-overlay:hide');
            });
        }

        orderCacheList() {

            var message = new MessageModule.Message();
            $(".row-orders-cache").trigger('loading-overlay:show');

            var ajaxPost = new Ajax.Service();

            var object = {};

            var url = "/Order/GetCacheOrders";
            ajaxPost.post(url, object, function Success(data: any) {

                $(".row-orders-cache").trigger('loading-overlay:hide');
                $(".row-orders-cache").html(data);
                new OrderModule.Order().select2trigger();

            }, function Error(data: any) {
                message.show("error", "Bildirim", "is problems");
                $(".row-orders-cache").trigger('loading-overlay:hide');
            });


        }

        orderCacheProductDelete(ProductId) {

            var message = new MessageModule.Message();
            $(".row-orders-cache").trigger('loading-overlay:show');

            var ajaxPost = new Ajax.Service();

            var object = { productId: ProductId };

            var url = "/Order/DeleteCacheOrdersProduct";
            ajaxPost.post(url, object, function Success(data: any) {

                $(".row-orders-cache").trigger('loading-overlay:hide');
                $(".row-orders-cache").html(data);

                new OrderModule.Order().orderCacheList();
            }, function Error(data: any) {
                message.show("error", "Bildirim", "is problems");
                $(".row-orders-cache").trigger('loading-overlay:hide');
            });
        }

        orderCacheProductAllDelete() {

            var message = new MessageModule.Message();
            $(".row-orders-cache").trigger('loading-overlay:show');

            var ajaxPost = new Ajax.Service();

            var object = {};

            var url = "/Order/DeleteCacheOrdersProductAll";
            ajaxPost.post(url, object, function Success(data: any) {

                $(".row-orders-cache").trigger('loading-overlay:hide');
                $(".row-orders-cache").html(data);

                new OrderModule.Order().orderCacheList();
            }, function Error(data: any) {
                message.show("error", "Bildirim", "is problems");
                $(".row-orders-cache").trigger('loading-overlay:hide');
            });
        }

        orderSave() {

            var message = new MessageModule.Message();
            $(".row-orders-cache").trigger('loading-overlay:show');

            var storecode = $(".slct-store").val();
            var accountno = $(".slct-account").val();

            var ajaxPost = new Ajax.Service();

            var object = { storeCode: storecode, accountNo: accountno };

            var url = "/Order/SaveOrders";
            ajaxPost.post(url, object, function Success(data: any) {
                $(".row-orders-cache").trigger('loading-overlay:hide');

                if (data.data) {
                    message.show("success", "Bildirim", "Siparişiniz oluşturuldu.");
                    $(".order-list").click();
                }
                else {
                    message.show("error", "Bildirim", "Siparişiniz oluşturulamadı");

                }

            }, function Error(data: any) {
                message.show("error", "Bildirim", "Sipariş oluştururken sorun oluştu");
                $(".row-orders-cache").trigger('loading-overlay:hide');
            });
        }

        calandertrigger() {

            $('[data-plugin-datepicker]').each(function () {
                var $this = $(this),
                    opts = {};

                var pluginOptions = $this.data('plugin-options');
                if (pluginOptions)
                    opts = pluginOptions;

                $this.themePluginDatePicker(opts);
            });

        }

        select2trigger() {

            // Select2
            if ($.isFunction($.fn['select2'])) {

                $(function () {
                    $('[data-plugin-selectTwo]').each(function () {
                        var $this = $(this),
                            opts = {};

                        var pluginOptions = $this.data('plugin-options');
                        if (pluginOptions)
                            opts = pluginOptions;

                        $this.adminPluginSelect2(opts);
                    });
                });

            }


            // Select2
            var admin = admin || {};
            var PluginSelect2: any;

            var instanceName = '__select2';

            PluginSelect2 = function ($el, opts) {
                return this.initialize($el, opts);
            };

            PluginSelect2.defaults = {
                theme: 'bootstrap'
            };

            PluginSelect2.prototype = {
                initialize: function ($el, opts) {
                    if ($el.data(instanceName)) {
                        return this;
                    }

                    this.$el = $el;

                    this
                        .setData()
                        .setOptions(opts)
                        .build();

                    return this;
                },

                setData: function () {
                    this.$el.data(instanceName, this);

                    return this;
                },

                setOptions: function (opts) {
                    this.options = $.extend(true, {}, PluginSelect2.defaults, opts);

                    return this;
                },

                build: function () {
                    this.$el.select2(this.options);

                    return this;
                }
            };

            // expose to scope
            $.extend(admin, {
                PluginSelect2: PluginSelect2
            });

            // jquery plugin
            $.fn.adminPluginSelect2 = function (opts) {
                return this.each(function () {
                    var $this = $(this);

                    if ($this.data(instanceName)) {
                        return $this.data(instanceName);
                    } else {
                        return new PluginSelect2($this, opts);
                    }

                });
            }



        }

    }
}
//Business



//Frontend
declare var $: any;
$(function () {

    /*
     Page Load
    */
    $(document).ready(function () {

        $('.row-products').on('click', '.order-text', function (event: any) {
            var productGroupId = $(this).attr("ProductGroupId");
            var term = $(".order-product-search").val();
            var listCount = 40;
            new OrderModule.Order().productlist(productGroupId, term, 1, listCount);
        });

        //search product event
        $('.row-products').on('keydown', '.order-product-search', function (e) {
            if (e.which == 13) {
                var productGroupId = $(".simple-post-list").find(".btn-primary").attr("ProductGroupId");
                var term = $(".order-product-search").val();
                var listCount = 40;
                new OrderModule.Order().productlist(productGroupId, term, 1, listCount);
            }
        });

        //fileuplaod product event
        $('.row-products').on('change', '.file-product', function (e) {

            var objectFile = this;

            var file = objectFile.files[0];
            var stockNo = $(objectFile).attr('stokNo');

            var formData = new FormData();
            formData.append('file', file);
            formData.append('stockNo', stockNo);

            new OrderModule.Order().productImageSave(formData, function Success(data) {
                console.log(data);
                var d = new Date();
                $(objectFile).parents(".thumb-image").find(".img-responsive").attr("src", "/images/stock/" + stockNo + "/" + stockNo + ".png?" + d.getTime());

            }, function Progress(e) { }, function Error(data) {
                new MessageModule.Message().show("error", "Hata", "işlem sırasında bir hata oluştu.");
            });
        });

        //Order List
        $('.order-list').on('click', function (event: any) {
            var term = $(".order-search").val();
            var startDate = "01.01.1990";
            var finishDate = new Date().toLocaleDateString();
            var listCount = 8;
            new OrderModule.Order().orderlist(term, 1, listCount, startDate, finishDate);
        });

        //Order page List
        $('body').on('click', '.btn-order-list', function (event: any) {
            var term = $(".order-search").val();
            var startDate = $('#txt-company-startDate').val();
            var finishDate = $('#txt-company-finishDate').val();
            var pageNo = 1;
            var listCount = 8;
            new OrderModule.Order().orderlist(term, pageNo, listCount, startDate, finishDate);
        });

        //Order page change List
        $('body').on('click', '.btn-order-page-change', function (event: any) {
            var term = $(".order-search").val();
            var startDate = $('#txt-company-startDate').val();
            var finishDate = $('#txt-company-finishDate').val();
            var pageNo = $(this).attr("pageNo");
            var listCount = 8;
            new OrderModule.Order().orderlist(term, pageNo, listCount, startDate, finishDate);
        });

        $('.row-orders').on('click', '.fa-selected', function () {
            if ($(this).hasClass("fa-minus-square-o")) {
                $(this).removeClass('fa-minus-square-o').addClass('fa-plus-square-o');
                $(this).parents(".selectedtr").next('.details ').fadeOut().addClass("hide");
            }
            else {
                $(this).removeClass('fa-plus-square-o').addClass('fa-minus-square-o');
                $(this).parents(".selectedtr").next('.details ').fadeIn().removeClass("hide");

            }
        });

        $('.row-products').on('click', '.btn-order-add', function () {
            var quantity = $(this).parents(".input-group").find(".number-Order").val();
            var productId = $(this).parents(".input-group").find(".number-Order").attr("ProductId");
            var productName = $(this).parents(".input-group").find(".number-Order").attr("ProductName");
            var productPrice = $(this).parents(".input-group").find(".number-Order").attr("ProductPrice");
            var productTax = $(this).parents(".input-group").find(".number-Order").attr("ProductTax");

            var productCode = $(this).parents(".input-group").find(".number-Order").attr("ProductCode");
            var productUnit = $(this).parents(".input-group").find(".number-Order").attr("ProductUnit");
            var productUnitRate = $(this).parents(".input-group").find(".number-Order").attr("ProductUnitRate");
            new OrderModule.Order().orderadd(productId, productCode, productName, productPrice, quantity, productTax, productUnit, productUnitRate);
        });

        $('.row-products').on('click', '.btn-order-create', function () {
            new OrderModule.Order().orderCacheList();
        });

        $('.row-orders-cache').on('click', '.btn-order-product-delete', function () {
            var productId = $(this).attr("productId")
            var arr = [];
            arr.push(productId);
            ModalModule.modalFunc = new OrderModule.Order().orderCacheProductDelete;
            ModalModule.modalArgs = arr;
            new ModalModule.Modal().show("Ürünü silmek istediğinize eminmisiniz ?");
        });


        $('.row-orders-cache').on('click', '.btn-order-clear', function () {
            ModalModule.modalFunc = new OrderModule.Order().orderCacheProductAllDelete;
            ModalModule.modalArgs = null;
            new ModalModule.Modal().show("Ürünler tamamen silinmesi istediğinize eminmisiniz ?");
        });

        $('.row-orders-cache').on('click', '.btn-order-save', function () {
            ModalModule.modalFunc = new OrderModule.Order().orderSave;
            ModalModule.modalArgs = null;
            new ModalModule.Modal().show("Bu sipariş kaydı gerçekleştirilmesini istiyormusunuz ?");
        });


        var scrollcheck = false;
        $(window).scroll(function () {
            if ($(window).scrollTop() + $(window).height() > $(document).height() - 100) {

                if (scrollcheck == false) {

                    $(".loading-order-products").removeClass("hide");
                    scrollcheck = true;
                    setTimeout(function () {
                        scrollcheck = false;


                        var productGroupId = $(".order-text.btn-primary").attr("ProductGroupId");
                        var term = $(".order-product-search").val();
                        var listCount = 40;
                        var pageNo = $(".row-products .loading-order-products").attr("pageNo");
                        new OrderModule.Order().productItemlist(productGroupId, term, pageNo, listCount);



                    }, 3000);
                }



            }
        });

    });




});
//Frontend