﻿import { MessageModule } from "../Common/Message";
import { HelperModule } from "../Common/Helper";
import { Ajax } from "../Common/Ajax";
import { Config } from "../Common/ConfigVariable";

//Business
export module HomeModule {
    declare var $: any;
    export class Home {

        change(ViewName: string, CompanyId: string, CompanysectionId: string, CompanyTypeId: string, startdate: Date, finishdate: Date) {
            var message = new MessageModule.Message();
            $(".content-body").trigger('loading-overlay:show');

            var ajaxPost = new Ajax.Service();

            var object = {
                viewName: ViewName,
                companyId: CompanyId,
                companySectionId: CompanysectionId,
                companyTypeId: CompanyTypeId,
                startDate: startdate,
                finishDate: finishdate
            };

            var url = "/Home/Getview";
            ajaxPost.post(url, object, function Success(data: any) {

                $(".content-body").trigger('loading-overlay:hide');
                $(".row-dashboard").html(data);
                new HomeModule.Home().calandertrigger();
                new HomeModule.Home().tabletrigger();
            }, function Error(data: any) {
                message.show("error", "Notify", "is problems");
                $(".content-body").trigger('loading-overlay:hide');
            });


        }

        tabletrigger() {

            var $table = $('.datatable-tabletools');

            $table.dataTable({
                "lengthMenu": [[100, 500, -1], [100, 500, "Hepsi"]],
                language: {
                    "lengthMenu": "_MENU_ sayfalanacak kayıt sayısı",
                    "zeroRecords": "Arama sonucunda bir kayıt bulunamadı",
                    "info": "Görüntülenen sayfa _PAGE_ - _PAGES_",
                    "infoEmpty": "",
                    "infoFiltered": ""
                },
                sDom: "<'text-right mb-md'T>" + $.fn.dataTable.defaults.sDom,
                oTableTools: {
                    aButtons: [
                        {
                            sExtends: 'print',
                            sButtonText: 'Yazdır',
                            sInfo: 'CTR+P ile yazdırabilirsiniz veya ESC tuşu ile çıkış yapabilirsiniz.'
                        }
                    ]
                },
                "fnDrawCallback": function (oSettings) {
                    new HomeModule.Home().tooltiptrigger();
                }
            });

            $('div.dataTables_filter input').addClass('form-control');

            $('div.dataTables_filter input').attr('placeholder', 'Ara..').addClass('form-control input-lg col-xs-12');
            $('div.dataTables_filter input').parents('label').css('width', '250px');
            $('div.company-panel .table-responsive').css('overflow-x', 'hidden');
        }

        tooltiptrigger() {
            $('[data-toggle=tooltip],[rel=tooltip]').tooltip({ container: 'body' });
        }

        calandertrigger() {

            $('[data-plugin-datepicker]').each(function () {
                var $this = $(this),
                    opts = {};

                var pluginOptions = $this.data('plugin-options');
                if (pluginOptions)
                    opts = pluginOptions;

                $this.themePluginDatePicker(opts);
            });

        }

    }
}
//Business



//Frontend
declare var $: any;
$(function () {

    /*
     Page Load
    */
    $(document).ready(function () {

        var message = new HelperModule.Helper().getParameterByName("textmessage", null);
        if (message != null)
            new MessageModule.Message().show("error", "Notify", message);


        $('.row-dashboard').on('click', '.company-text', function (event: any) {
            var viewName = $(this).attr("ViewName");
            var companyId = $(this).attr("CompanyId");
            var companysectionId = $(this).attr("CompanySectionId");
            var companyTypeId = $(this).attr("CompanyTypeId");
            var startDate = $('#txt-company-startDate').val();
            var finishDate = $('#txt-company-finishDate').val();
            new HomeModule.Home().change(viewName, companyId, companysectionId, companyTypeId, startDate, finishDate);
        });

    });





});
//Frontend

