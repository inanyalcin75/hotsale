﻿import { Ajax } from "../Common/Ajax";
import { MessageModule } from "../Common/Message";
import { Config } from "../Common/ConfigVariable";
import { HelperModule } from "../Common/Helper";

//Business
export module LoginModule {
    declare var $: any;
    export class Login {
        LoginCheck(email: string, pass: string, successCall: Function, errorCall: Function) {
            var sx = new Ajax.Service();
            var req = { email: email, pass: pass };
            sx.post(Config.BaseUrl + "Login/LoginCheck", req, successCall, errorCall);
        }
    }
}
//Business

//Frontend
declare var $: any;
$(function () {

    //page load
    $(document).ready(function () {
        var message = new HelperModule.Helper().getParameterByName("textmessage", null);
        if (message != null)
            new MessageModule.Message().show("error", "Notify", message);

    });


    //Form Validation
    $("#form").validate({
        highlight: function (label) {
            $(label).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (label) {
            $(label).closest('.form-group').removeClass('has-error');
            label.remove();
        },
        errorPlacement: function (error, element) {
            var placement = element.closest('.input-group');
            if (!placement.get(0)) {
                placement = element;
            }
            if (error.text() !== '') {
                placement.after(error);
            }
        }
    });

    //Login Click
    $('.button-login').on('click', function () {
        var validCheck = $("#form").valid();
        if (!validCheck) return;

        $(".center-sign").trigger('loading-overlay:show');
        var email = $('.input-username').val();
        var pass = $('.input-pass').val();
        var message = new MessageModule.Message();
        new LoginModule.Login().LoginCheck(email, pass, function Success(data) {
            $(".center-sign").trigger('loading-overlay:hide');

            console.log(data);

            if (data.status) {
                location.href = "/";
            }
            else {
                message.show("error", "Notify", "Login not access");
            }
        }, function Error(data) {
            message.show("error", "Notify", "ajax is problems");
            $(".center-sign").trigger('loading-overlay:hide');
        });
    });


     //Login Enter
    $('.input-pass').on('keyup keypress', function (e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {

            var validCheck = $("#form").valid();
            if (!validCheck) return;

            $(".center-sign").trigger('loading-overlay:show');
            var email = $('.input-username').val();
            var pass = $('.input-pass').val();
            var message = new MessageModule.Message();
            new LoginModule.Login().LoginCheck(email, pass, function Success(data) {
                $(".center-sign").trigger('loading-overlay:hide');

                console.log(data);

                if (data.status) {
                    location.href = "/";
                }
                else {
                    message.show("error", "Notify", "Login not access");
                }
            }, function Error(data) {
                message.show("error", "Notify", "ajax is problems");
                $(".center-sign").trigger('loading-overlay:hide');
            });

        }
    });


    $('#form').on('keyup keypress', function (e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            e.preventDefault();
            return false;
        }
    });


});
//Frontend