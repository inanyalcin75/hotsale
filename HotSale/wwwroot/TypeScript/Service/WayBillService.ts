﻿import { ModalModule } from "../Common/Modal";
import { Ajax } from "../Common/Ajax";
import { Config } from "../Common/ConfigVariable";
import { MessageModule } from "../Common/Message";

//Business
export module WayBillModule {
    declare var $: any;
    export class WayBill {

        waybilllist(Term: string, PageNo: number, ListCount: number, startdate: string, finishdate: string) {
            var message = new MessageModule.Message();
            $(".row-orders").trigger('loading-overlay:show');

            var ajaxPost = new Ajax.Service();

            var object = { term: Term, pageNo: PageNo, listCount: ListCount, startDate: startdate, finishDate: finishdate };

            var url = "/Finance/WayBillAjax";
            ajaxPost.post(url, object, function Success(data: any) {

                $(".row-orders").trigger('loading-overlay:hide');
                $(".panel-waybill").html(data);
                $("#waybill-sub-total").val(("Toplam Tutar :" + $('body').find('.waybill-footer-total').html()));
                //new OrderModule.Order().calandertrigger();

            }, function Error(data: any) {
                message.show("error", "Bildirim", "is problems");
                $(".row-orders").trigger('loading-overlay:hide');
            });

        }


    }
}
//Business



//Frontend
declare var $: any;
$(function () {

    /*
     Page Load
    */
    $(document).ready(function () {


        $('.row-orders').on('click', '.fa-selected', function () {
            if ($(this).hasClass("fa-minus-square-o")) {
                $(this).removeClass('fa-minus-square-o').addClass('fa-plus-square-o');
                $(this).parents(".selectedtr").next('.details ').fadeOut().addClass("hide");
            }
            else {
                $(this).removeClass('fa-plus-square-o').addClass('fa-minus-square-o');
                $(this).parents(".selectedtr").next('.details ').fadeIn().removeClass("hide");
            }
        });

        //WayBill Page List
        $('body').on('click', '.btn-waybill-list', function (event: any) {
            var term = $(".order-search").val();
            var startDate = $('#txt-company-startDate').val();
            var finishDate = $('#txt-company-finishDate').val();
            var pageNo = 1;
            var listCount = 50;
            new WayBillModule.WayBill().waybilllist(term, pageNo, listCount, startDate, finishDate);
        });



        //Order page change List
        $('body').on('click', '.btn-waybill-page-change', function (event: any) {
            var term = $(".order-search").val();
            var startDate = $('#txt-company-startDate').val();
            var finishDate = $('#txt-company-finishDate').val();
            var pageNo = $(this).attr("pageNo");
            var listCount = 50;
            new WayBillModule.WayBill().waybilllist(term, pageNo, listCount, startDate, finishDate);
        });


    });




});
//Frontend