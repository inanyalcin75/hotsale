export var Ajax;
(function (Ajax) {
    class Options {
        constructor(url, method, data) {
            this.url = url;
            this.method = method || "get";
            this.data = data || {};
        }
    }
    Ajax.Options = Options;
    class Service {
        constructor() {
            this.request = (options, successCallback, errorCallback) => {
                var that = this;
                $.ajax({
                    url: options.url,
                    type: options.method,
                    data: options.data,
                    traditional: true,
                    cache: false,
                    success: function (d) {
                        successCallback(d);
                    },
                    error: function (d) {
                        if (errorCallback) {
                            errorCallback(d);
                            return;
                        }
                        var errorTitle = "Error in (" + options.url + ")";
                        var fullError = JSON.stringify(d);
                        console.log(errorTitle);
                        console.log(fullError);
                    }
                });
            };
            this.requestFormdata = function (options, successCallback, progressCallback, errorCallback) {
                var that = this;
                $.ajax({
                    url: options.url,
                    type: options.method,
                    data: options.data,
                    processData: false,
                    contentType: false,
                    timeout: 10000000,
                    success: function (xhr) {
                        successCallback(xhr);
                    },
                    xhr: function () {
                        var xhr = new XMLHttpRequest();
                        //Upload progress
                        xhr.upload.addEventListener("progress", function (evt) {
                            if (evt.lengthComputable) {
                                var percentComplete = evt.loaded / evt.total;
                                //Do something with upload progress
                                progressCallback(evt);
                            }
                        }, false);
                        //Download progress
                        xhr.addEventListener("progress", function (evt) {
                            if (evt.lengthComputable) {
                                var percentComplete = evt.loaded / evt.total;
                                //Do something with download progress
                                progressCallback(evt);
                            }
                        }, false);
                        return xhr;
                    },
                    error: function (d) {
                        if (errorCallback) {
                            errorCallback(d);
                            return;
                        }
                        var errorTitle = "Error in (" + options.url + ")";
                        var fullError = JSON.stringify(d);
                        console.log(errorTitle);
                        console.log(fullError);
                    }
                });
            };
            this.get = (url, successCallback, errorCallback) => {
                this.request(new Options(url), successCallback, errorCallback);
            };
            this.post = (url, data, successCallback, errorCallback) => {
                this.request(new Options(url, "post", data), successCallback, errorCallback);
            };
            this.postFormdata = function (url, data, successCallback, progressCallback, errorCallback) {
                this.requestFormdata(new Options(url, "post", data), successCallback, progressCallback, errorCallback);
            };
        }
    }
    Ajax.Service = Service;
})(Ajax || (Ajax = {}));
//# sourceMappingURL=Ajax.js.map