export var MessageModule;
(function (MessageModule) {
    var stack_bar_top = { "dir1": "down", "dir2": "right", "push": "top", "spacing1": 0, "spacing2": 0 };
    //Type : error,success
    class Message {
        show(type, title, message) {
            $(function () {
                var notice = new PNotify({
                    title: title,
                    text: message,
                    type: type,
                });
            });
        }
    }
    MessageModule.Message = Message;
})(MessageModule || (MessageModule = {}));
//# sourceMappingURL=Message.js.map