﻿using HotSale.Cache;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotSale.Helper
{
    public static class CacheConfig
    {

        public static void Configration(IConfiguration Configuration, IServiceCollection services)
        {
            //var ServiceProvider = services.BuildServiceProvider();
            //var x = ServiceProvider.GetService<IOptions<AppParamConfig>>();
            string cacheName = Configuration.GetSection("AppParamConfig")["CacheType"];

            switch (cacheName)
            {
                case "MicrosoftMemcache":
                    services.AddMemoryCache();
                    services.AddSingleton<ICacheProvider, MicrosoftMemcache>();
                 break;
            }

        }

    }
}
