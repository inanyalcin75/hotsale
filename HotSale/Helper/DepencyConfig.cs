﻿using HotSale.Businesss.DashboardWork;
using HotSale.Businesss.FinancialWork;
using HotSale.Businesss.LoginWork;
using HotSale.Businesss.OrderWork;
using HotSale.Businesss.StoreWork;
using HotSale.Controllers;
using HotSale.Service.AccountService;
using HotSale.Service.CompanyService;
using HotSale.Service.FinancialService;
using HotSale.Service.LoginService;
using HotSale.Service.OrderService;
using HotSale.Service.ProductService;
using HotSale.Service.ReportService;
using HotSale.Service.SpecialOrderService;
using HotSale.Service.SqlDataService;
using HotSale.Service.StoreService;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotSale.Helper
{
    public static class DepencyConfig
    {

        public static void Configration(IServiceCollection services)
        {
            services.AddSingleton<CustomAuthorizationAttribute>();

            #region Business
            services.AddSingleton<ILoginWork, LoginWork>();
            services.AddSingleton<IDashboardWork, DashboardWork>();
            services.AddSingleton<IOrderWork, OrderWork>();
            services.AddSingleton<IStoreWork, StoreWork>();
            services.AddSingleton<IFinancialWork, FinancialWork>();
            services.AddSingleton<ISpecialOrderWork, SpecialOrderWork>();
            #endregion Business

            #region Service
            services.AddSingleton<ICompanyService, CompanyService>();
            services.AddSingleton<IAccountService, AccountService>();
            services.AddSingleton<IDashboardService, DashboardService>();
            services.AddSingleton<ILoginService, LoginService>();
            services.AddSingleton<IOrderService, OrderService>();
            services.AddSingleton<IProductService, ProductService>();
            services.AddSingleton<IStoreService, StoreService>();
            services.AddSingleton<ISqlDataService, SqlDataService>();
            services.AddSingleton<IFinancialService, FinancialService>();
            services.AddSingleton<ISpecialOrderService, SpecialOrderService>();
            #endregion Service
        }
    }
}
