﻿using HotSale.Dto.Operations;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace HotSale.Helper
{
    public static class WebSession 
    {

        public static LoginCheckRes LoginControl(ISession session)
        {
            var loginCheckRes = SessionExtensions.GetObject<LoginCheckRes>(session, "WebUser");
            return loginCheckRes;
        }

        public static void LoginOut(ISession session)
        {
            SessionExtensions.SetObject<LoginCheckRes>(session, "WebUser", null);

        }

        public static void LoginAdd(ISession session, LoginCheckRes loginCheckRes)
        {
            SessionExtensions.SetObject<LoginCheckRes>(session, "WebUser", loginCheckRes);
        }

    }


    public static class SessionExtensions
    {
        public static void SetObject<T>(this ISession session, string key, T value)
        {
            session.SetString(key, JsonConvert.SerializeObject(value));
        }

        public static T GetObject<T>(this ISession session, string key)
        {
            var value = session.GetString(key);
            return value == null ? default(T) :
                                  JsonConvert.DeserializeObject<T>(value);
        }
    }
}
