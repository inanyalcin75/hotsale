﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HotSale.Controllers;
using HotSale.Helper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace HotSale
{
    public class Startup
    {
        private readonly IHostingEnvironment _currentEnv;
        public IConfiguration Configuration { get;}

        public Startup(IConfiguration configuration, IHostingEnvironment env)
        {
            _currentEnv = env;
            Configuration = configuration;
        }


        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            services.AddDistributedMemoryCache(); // Adds a default in-memory implementation of IDistributedCache
            services.AddSession();
            services.Configure<AppParamConfig>(Configuration.GetSection("AppParamConfig")); //Appsetting configration
            DepencyConfig.Configration(services); //Depency injecetion load
            CacheConfig.Configration(Configuration, services); //Cache configration

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            // IMPORTANT: This session call MUST go before UseMvc()
            app.UseSession();

            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });



           
        }
    }
}
