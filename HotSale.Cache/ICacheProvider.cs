﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HotSale.Cache
{
    public interface ICacheProvider
    {
        T Get<T>(enmCacheNames key);
        T Get<T>(enmCacheNames key, string uniqId);
        void Set<T>(enmCacheNames key, T entry, int minute = 10);
        void Set<T>(enmCacheNames key, string uniqId, T entry, int minute = 10);
        void Remove(enmCacheNames key);
        void Remove(enmCacheNames key, string uniqId);
        void Remove(string key);
    }
}
