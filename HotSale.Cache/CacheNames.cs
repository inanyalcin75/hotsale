﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HotSale.Cache
{
    public enum enmCacheNames
    {
        CompanyList,
        CompanyStoreList,
        ShowCaseProductList,
        CategoryList,
        GroupList,
        ProductList,
        OrderList,
        OrderDetailList,
        BasketList,
        StoreList,
        StoreDetailList,
        StoreProductList,
        StoreBasketList,
        AccountCardList,
        //Ram'e alınan bilgilerin isimlerinin tutulması uniq isimler içermekte 
        //başka fonksiyonlardan ulaşımı sağlamak için
        CachaNameList
    }
}
