﻿using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Text;

namespace HotSale.Cache
{
    public class MicrosoftMemcache : ICacheProvider
    {
        private IMemoryCache _cache;

        public MicrosoftMemcache(IMemoryCache cache)
        {
            _cache = cache;
        }

        public T Get<T>(enmCacheNames key)
        {
            return _cache.Get<T>(key.ToString());
        }

        public T Get<T>(enmCacheNames key, string uniqId)
        {
            return _cache.Get<T>(key.ToString() + "-" + uniqId);
        }

        public void Remove(enmCacheNames key)
        {
            _cache.Remove(key.ToString());
        }

        public void Remove(enmCacheNames key, string uniqId)
        {
            _cache.Remove(key.ToString() + "-" + uniqId);
        }

        public void Remove(string key)
        {
            _cache.Remove(key);
        }

        public void Set<T>(enmCacheNames key, T entry, int minute = 10)
        {
            var cacheEntryOptions = new MemoryCacheEntryOptions().SetSlidingExpiration(TimeSpan.FromMinutes(minute));
            _cache.Set<T>(key.ToString(), entry, cacheEntryOptions);
        }

        public void Set<T>(enmCacheNames key, string uniqId, T entry, int minute = 10)
        {
            var cacheEntryOptions = new MemoryCacheEntryOptions().SetSlidingExpiration(TimeSpan.FromMinutes(minute));
            _cache.Set<T>(key.ToString() + "-" + uniqId, entry, cacheEntryOptions);
        }
    }
}
