﻿using HotSale.Dto;
using HotSale.Dto.Helper;
using HotSale.Dto.Operations;
using HotSale.Dto.Operations.Order;
using System;
using System.Collections.Generic;
using System.Text;

namespace HotSale.Businesss.OrderWork
{
    public interface IOrderWork
    {
        ServiceResponse<ProductRes> GetProductGroupList(string productGroupId, int firmId, string term, int pageNo, int listCount);
        OrderRes GetOrderList(int companyId, int firmId, bool isCustomer, string term, int pageNo, int listCount, DateTime startDate, DateTime finishDate);
        ServiceResponse<bool> SetOrderAdd(string userId, int productId, string productCode, string productName, decimal price, decimal quantity, decimal tax, string productUnit, decimal productUnitRate);
        ServiceResponse<bool> SetOrderRemove(string userId, int productId);
        ServiceResponse<bool> SetOrderRemoveAll(string userId);
        ServiceResponse<bool> SetOrderSave(int storeCode, int? accountNo, LoginCheckRes loginCheckRes);
        ServiceResponse<List<BasketDto>> GetCacheOrderList(string userId);
    }
}
