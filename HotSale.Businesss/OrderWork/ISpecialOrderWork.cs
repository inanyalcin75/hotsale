﻿using HotSale.Dto.Helper;
using HotSale.Dto.Operations.SpecialOrder;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace HotSale.Businesss.OrderWork
{
    public interface ISpecialOrderWork
    {
        ServiceResponse<bool> SetOrderSave(int branchId, int depoKodu, string StockCode, string StockDetail, string Description, Stream Filer, string DeliveryAddress, string DeliveryDate, string DeliveryTime);
        ServiceResponse<SpacialOrderResDto> GetSpacialOrder(int branchId);
        ServiceResponse<List<SpecialOrderStockResDto>> GetStock(string term);
    }
}
