﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using HotSale.Dto.Helper;
using HotSale.Service.CompanyService;
using HotSale.Dto.Operations.SpecialOrder;
using HotSale.Service.SpecialOrderService;
using System.IO;

namespace HotSale.Businesss.OrderWork
{
    public class SpecialOrderWork : ISpecialOrderWork
    {
        private readonly ICompanyService _companyService;
        private readonly ISpecialOrderService _specialOrderService;

        //========================================================================================================================
        // Constructor
        //========================================================================================================================
        public SpecialOrderWork(ICompanyService companyService, ISpecialOrderService specialOrderService)
        {
            //Injection
            _companyService = companyService;
            _specialOrderService = specialOrderService;
        }


        public ServiceResponse<SpacialOrderResDto> GetSpacialOrder(int branchId)
        {
            var companyList = _companyService.GetCompanyList();

            return new ServiceResponse<SpacialOrderResDto> { Status = true, Data = new SpacialOrderResDto { BranchName = companyList.FirstOrDefault(x => x.Id == branchId).Name } };
        }

        public ServiceResponse<List<SpecialOrderStockResDto>> GetStock(string term)
        {
            return new ServiceResponse<List<SpecialOrderStockResDto>> { Status = true, Data = _specialOrderService.GetStock(term) };
        }

        public ServiceResponse<bool> SetOrderSave(int branchId, int depoKodu, string StockCode, string StockDetail, string Description, Stream Filer, string DeliveryAddress, string DeliveryDate, string DeliveryTime)
        {
            var returner = _specialOrderService.SaveOrder(branchId, depoKodu, StockCode, StockDetail, Description, DeliveryAddress, DeliveryDate, DeliveryTime);

            return new ServiceResponse<bool> { Status = true, Data = returner };

        }
    }
}
