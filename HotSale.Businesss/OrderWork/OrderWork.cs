﻿using System;
using System.Collections.Generic;
using System.Text;
using HotSale.Cache;
using HotSale.Dto;
using HotSale.Dto.Helper;
using HotSale.Dto.Operations.Order;
using HotSale.Service.OrderService;
using HotSale.Service.ProductService;
using System.Linq;
using HotSale.Dto.Operations;

namespace HotSale.Businesss.OrderWork
{
    public class OrderWork : IOrderWork
    {
        private readonly IProductService _productService;
        private readonly IOrderService _orderService;
        private readonly ICacheProvider _cacheProvider;

        //========================================================================================================================
        // Constructor
        //========================================================================================================================
        public OrderWork(IProductService productService, IOrderService orderService, ICacheProvider cacheProvider)
        {
            //Injection
            _productService = productService;
            _orderService = orderService;
            _cacheProvider = cacheProvider;
        }


        public ServiceResponse<List<BasketDto>> GetCacheOrderList(string userId)
        {
            var baskets = _cacheProvider.Get<List<BasketDto>>(enmCacheNames.BasketList, userId.ToString()) ?? new List<BasketDto>();
            return new ServiceResponse<List<BasketDto>> { Data = baskets, DataTotalCount = baskets.Count(), Status = true };
        }

        public OrderRes GetOrderList(int companyId, int firmId, bool isCustomer, string term, int pageNo, int listCount, DateTime startDate, DateTime finishDate)
        {
            //Bu son tarihi almak için gerekli
            finishDate = finishDate.Add(new TimeSpan(23, 59, 59));

            var cacheName = (term ?? "") + "-" + firmId + "-" + pageNo + startDate.ToShortDateString() + "-" + finishDate.ToShortDateString();
            var orders = _cacheProvider.Get<ServiceResponse<List<OrderDto>>>(enmCacheNames.OrderList, cacheName);

            if (orders == null)
            {
                orders = _orderService.GetOrderList(companyId, firmId, isCustomer, term, pageNo, listCount, startDate, finishDate);
                orders.Page = new Infrastructure.Extension.Pager(orders.DataTotalCount, pageNo, listCount);
                _cacheProvider.Set<ServiceResponse<List<OrderDto>>>(enmCacheNames.OrderList, cacheName, orders);


                #region CachaName Save Cache
                var listCacheNames = _cacheProvider.Get<List<string>>(enmCacheNames.CachaNameList);

                if (listCacheNames == null)
                    listCacheNames = new List<string>();

                listCacheNames.Add(enmCacheNames.OrderList.ToString() + "-" + cacheName);

                _cacheProvider.Set<List<string>>(enmCacheNames.CachaNameList, listCacheNames);

                #endregion CachaName Save Cache


            }

            var orderDetails = _cacheProvider.Get<List<OrderDetailDto>>(enmCacheNames.OrderDetailList, cacheName);

            if (orderDetails == null && orders.Data.Count > 0)
            {
                orderDetails = _orderService.GetOrderDetailList(orders.Data.Select(t => t.FisNo).ToArray());

                _cacheProvider.Set<List<OrderDetailDto>>(enmCacheNames.OrderDetailList, cacheName, orderDetails);

                #region CachaName Save Cache
                var listCacheNames = _cacheProvider.Get<List<string>>(enmCacheNames.CachaNameList);

                if (listCacheNames == null)
                    listCacheNames = new List<string>();

                listCacheNames.Add(enmCacheNames.OrderDetailList.ToString() + "-" + cacheName);

                _cacheProvider.Set<List<string>>(enmCacheNames.CachaNameList, listCacheNames);

                #endregion CachaName Save Cache
            }

            var data = new OrderRes { OrdersDto = orders, OrderDetailsDto = orderDetails };

            return data;
        }

        public ServiceResponse<ProductRes> GetProductGroupList(string productGroupId, int firmId, string term, int pageNo, int listCount)
        {
            var productGroups = _cacheProvider.Get<List<ProductGroupDto>>(enmCacheNames.GroupList);

            if (productGroups == null)
            {
                productGroups = _productService.GetProductGroupList(firmId);

                _cacheProvider.Set<List<ProductGroupDto>>(enmCacheNames.GroupList, productGroups);
            }

            var products = _cacheProvider.Get<ServiceResponse<List<ProductDto>>>(enmCacheNames.ProductList, (productGroupId?.ToString() ?? "") + "-" + (term ?? "") + "-" + pageNo);

            if (products == null)
            {
                products = _productService.GetProductList(productGroupId, firmId, term, pageNo, listCount);

                _cacheProvider.Set<ServiceResponse<List<ProductDto>>>(enmCacheNames.ProductList, (productGroupId?.ToString() ?? "") + "-" + (term ?? "") + "-" + pageNo, products);
            }

            var data = new ProductRes { ProductGroupId = productGroupId, Term = term, PageNo = pageNo, ProductGroupsDto = productGroups, ProductsDto = products };

            return new ServiceResponse<ProductRes> { Status = true, Data = data };
        }

        public ServiceResponse<bool> SetOrderAdd(string userId, int productId, string productCode, string productName, decimal price, decimal quantity, decimal tax, string productUnit, decimal productUnitRate)
        {
            var baskets = _cacheProvider.Get<List<BasketDto>>(enmCacheNames.BasketList, userId.ToString());

            if (baskets == null)
            {
                baskets = new List<BasketDto>() { new BasketDto { UserId = userId, ProductId = productId, ProductName = productName, Price = price, Quantity = quantity, Tax = tax, ProductCode = productCode, ProductUnit = productUnit, ProductUnitRate = productUnitRate } };
            }
            else
            {

                if (baskets.Any(x => x.ProductId == productId))
                {
                    baskets.FirstOrDefault(x => x.ProductId == productId).Quantity += quantity;
                }
                else
                    baskets.Add(new BasketDto { UserId = userId, ProductId = productId, ProductName = productName, Price = price, Quantity = quantity, Tax = tax, ProductCode = productCode, ProductUnit = productUnit, ProductUnitRate = productUnitRate });

            }


            _cacheProvider.Set<List<BasketDto>>(enmCacheNames.BasketList, userId.ToString(), baskets);
            return new ServiceResponse<bool> { Data = true, Status = true };
        }


        public ServiceResponse<bool> SetOrderRemove(string userId, int productId)
        {
            var baskets = _cacheProvider.Get<List<BasketDto>>(enmCacheNames.BasketList, userId.ToString());

            if (baskets != null)
            {
                baskets.Remove(baskets.FirstOrDefault(x => x.UserId == userId && x.ProductId == productId));
            }

            _cacheProvider.Set<List<BasketDto>>(enmCacheNames.BasketList, userId.ToString(), baskets);

            return new ServiceResponse<bool> { Data = true, Status = true };
        }

        public ServiceResponse<bool> SetOrderRemoveAll(string userId)
        {
            var baskets = _cacheProvider.Get<List<BasketDto>>(enmCacheNames.BasketList, userId.ToString());

            if (baskets != null)
            {
                baskets = new List<BasketDto>();
            }

            _cacheProvider.Set<List<BasketDto>>(enmCacheNames.BasketList, userId.ToString(), baskets);

            return new ServiceResponse<bool> { Data = true, Status = true };
        }

        public ServiceResponse<bool> SetOrderSave(int storeCode, int? accountNo, LoginCheckRes loginCheckRes)
        {
            var baskets = _cacheProvider.Get<List<BasketDto>>(enmCacheNames.BasketList, loginCheckRes.UserId);

            if (baskets == null || baskets.Count == 0)
            {
                return new ServiceResponse<bool>() { Data = false, Status = false, Message = "Sepette ürün bulunamadı." };
            }

            //Eğer dış müşteri ise cari no yollayamaz
            if (loginCheckRes.IsCustomer)
            {
                accountNo = null;
            }

            var order = _orderService.SaveOrder(storeCode, accountNo, baskets, loginCheckRes);

            _cacheProvider.Set<List<BasketDto>>(enmCacheNames.BasketList, loginCheckRes.UserId, new List<BasketDto>());


            #region CachaName Remove Cache
            var cachaNameList = _cacheProvider.Get<List<string>>(enmCacheNames.CachaNameList);

            if (cachaNameList != null)
            {
                var cachaNameItems = cachaNameList.Where(x => x.Contains(enmCacheNames.OrderList.ToString()));

                foreach (var item in cachaNameItems)
                {
                    _cacheProvider.Remove(item);
                }

                var cachaNameDetailItems = cachaNameList.Where(x => x.Contains(enmCacheNames.OrderDetailList.ToString()));

                if (cachaNameDetailItems != null)
                {
                    foreach (var item in cachaNameDetailItems)
                    {
                        _cacheProvider.Remove(item);

                    }
                }
            }

            #endregion CachaName Remove Cache

            return new ServiceResponse<bool>() { Data = order, Status = true };

        }
    }
}
