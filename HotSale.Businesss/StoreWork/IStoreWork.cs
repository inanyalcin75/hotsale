﻿using HotSale.Dto;
using HotSale.Dto.Helper;
using HotSale.Dto.Operations;
using HotSale.Dto.Operations.Store;
using System;
using System.Collections.Generic;
using System.Text;

namespace HotSale.Businesss.StoreWork
{
    public interface IStoreWork
    {
        ServiceResponse<List<ProductDto>> GetStoreProductList(string term, int pageNo, int listCount);
        StoreRes GetStoreList(int companyId, string firmCode, string term, int pageNo, int listCount, DateTime startDate, DateTime finishDate);
        ServiceResponse<bool> SetStoreAdd(string userId, int productId, string productCode, string productName, decimal price, decimal quantity, decimal tax, string productUnit, decimal productUnitRate);
        ServiceResponse<bool> SetStoreRemove(string userId, int productId);
        ServiceResponse<bool> SetStoreRemoveAll(string userId);
        ServiceResponse<bool> SetStoreSave(int storeCode, LoginCheckRes loginCheckRes);
        ServiceResponse<List<StoreBasketDto>> GetCacheStoreList(string userId);
    }
}
