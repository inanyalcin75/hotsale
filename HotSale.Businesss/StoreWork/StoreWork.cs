﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HotSale.Cache;
using HotSale.Dto;
using HotSale.Dto.Helper;
using HotSale.Dto.Operations;
using HotSale.Dto.Operations.Store;
using HotSale.Service.ProductService;
using HotSale.Service.StoreService;

namespace HotSale.Businesss.StoreWork
{
    public class StoreWork : IStoreWork
    {
        private readonly ICacheProvider _cacheProvider;
        private readonly IProductService _productService;
        private readonly IStoreService _storeService;



        //========================================================================================================================
        // Constructor
        //========================================================================================================================
        public StoreWork(ICacheProvider cacheProvider, IProductService productService, IStoreService storeService)
        {
            //Injection
            _cacheProvider = cacheProvider;
            _productService = productService;
            _storeService = storeService;
        }


        public ServiceResponse<List<StoreBasketDto>> GetCacheStoreList(string userId)
        {
            var baskets = _cacheProvider.Get<List<StoreBasketDto>>(enmCacheNames.StoreBasketList, userId.ToString()) ?? new List<StoreBasketDto>();
            return new ServiceResponse<List<StoreBasketDto>> { Data = baskets, DataTotalCount = baskets.Count, Status = true };
        }

        public StoreRes GetStoreList(int companyId, string firmCode, string term, int pageNo, int listCount, DateTime startDate, DateTime finishDate)
        {
            //Bu son tarihi almak için gerekli
            finishDate = finishDate.Add(new TimeSpan(23, 59, 59));

            var cacheName = companyId + firmCode + (term ?? "") + "-" + pageNo + startDate.ToShortDateString() + "-" + finishDate.ToShortDateString();
            var stores = _cacheProvider.Get<ServiceResponse<List<StoreDto>>>(enmCacheNames.StoreList, cacheName);

            if (stores == null)
            {
                stores = _storeService.GetStoreList(companyId, firmCode, term, pageNo, listCount, startDate, finishDate);
                stores.Page = new Infrastructure.Extension.Pager(stores.DataTotalCount, pageNo, listCount);
                _cacheProvider.Set<ServiceResponse<List<StoreDto>>>(enmCacheNames.StoreList, cacheName, stores);

                #region CachaName Save Cache
                var listCacheNames = _cacheProvider.Get<List<string>>(enmCacheNames.CachaNameList);

                if (listCacheNames == null)
                    listCacheNames = new List<string>();

                listCacheNames.Add(enmCacheNames.StoreList.ToString() + "-" + cacheName);

                _cacheProvider.Set<List<string>>(enmCacheNames.CachaNameList, listCacheNames);

                #endregion CachaName Save Cache

            }

            var storeDetails = _cacheProvider.Get<List<StoreDetailDto>>(enmCacheNames.StoreDetailList, cacheName);

            if (storeDetails == null && stores.Data.Count > 0)
            {
                storeDetails = _storeService.GetStoreDetailList(stores.Data.Select(t => t.FisNo).ToArray());

                _cacheProvider.Set<List<StoreDetailDto>>(enmCacheNames.StoreDetailList, cacheName, storeDetails);

                #region CachaName Save Cache
                var listCacheNames = _cacheProvider.Get<List<string>>(enmCacheNames.CachaNameList);

                if (listCacheNames == null)
                    listCacheNames = new List<string>();

                listCacheNames.Add(enmCacheNames.StoreDetailList.ToString() + "-" + cacheName);

                _cacheProvider.Set<List<string>>(enmCacheNames.CachaNameList, listCacheNames);

                #endregion CachaName Save Cache
            }

            var data = new StoreRes { StoreDto = stores, StoreDetailsDto = storeDetails };

            return data;
        }

        public ServiceResponse<List<ProductDto>> GetStoreProductList(string term, int pageNo, int listCount)
        {
            var cacheName = (term ?? "") + "-" + pageNo;
            var storeProductsList = _cacheProvider.Get<List<ProductDto>>(enmCacheNames.StoreProductList, cacheName);

            if (storeProductsList == null)
            {
                storeProductsList = _productService.GetStoreProductList(term, pageNo, listCount).Data;

                _cacheProvider.Set<List<ProductDto>>(enmCacheNames.StoreProductList, cacheName, storeProductsList);
            }

            return new ServiceResponse<List<ProductDto>> { Status = true, Data = storeProductsList };
        }

        public ServiceResponse<bool> SetStoreAdd(string userId, int productId, string productCode, string productName, decimal price, decimal quantity, decimal tax, string productUnit, decimal productUnitRate)
        {
            var baskets = _cacheProvider.Get<List<StoreBasketDto>>(enmCacheNames.StoreBasketList, userId.ToString());

            if (baskets == null)
            {
                baskets = new List<StoreBasketDto>() { new StoreBasketDto { UserId = userId, ProductId = productId, ProductName = productName, Price = price, Quantity = quantity, Tax = tax, ProductCode = productCode, ProductUnit = productUnit, ProductUnitRate = productUnitRate } };
            }
            else
            {

                if (baskets.Any(x => x.ProductId == productId))
                {
                    baskets.FirstOrDefault(x => x.ProductId == productId).Quantity += quantity;
                }
                else
                    baskets.Add(new StoreBasketDto { UserId = userId, ProductId = productId, ProductName = productName, Price = price, Quantity = quantity, Tax = tax, ProductCode = productCode, ProductUnit = productUnit, ProductUnitRate = productUnitRate });

            }


            _cacheProvider.Set<List<StoreBasketDto>>(enmCacheNames.StoreBasketList, userId.ToString(), baskets);
            return new ServiceResponse<bool> { Data = true, Status = true };
        }

        public ServiceResponse<bool> SetStoreRemove(string userId, int productId)
        {
            var baskets = _cacheProvider.Get<List<StoreBasketDto>>(enmCacheNames.StoreBasketList, userId.ToString());

            if (baskets != null)
            {
                baskets.Remove(baskets.FirstOrDefault(x => x.UserId == userId && x.ProductId == productId));
            }

            _cacheProvider.Set<List<StoreBasketDto>>(enmCacheNames.StoreBasketList, userId.ToString(), baskets);

            return new ServiceResponse<bool> { Data = true, Status = true };
        }

        public ServiceResponse<bool> SetStoreRemoveAll(string userId)
        {
            var baskets = _cacheProvider.Get<List<StoreBasketDto>>(enmCacheNames.StoreBasketList, userId.ToString());

            if (baskets != null)
            {
                baskets = new List<StoreBasketDto>();
            }

            _cacheProvider.Set<List<StoreBasketDto>>(enmCacheNames.StoreBasketList, userId.ToString(), baskets);

            return new ServiceResponse<bool> { Data = true, Status = true };
        }

        public ServiceResponse<bool> SetStoreSave(int storeCode, LoginCheckRes loginCheckRes)
        {
            var baskets = _cacheProvider.Get<List<StoreBasketDto>>(enmCacheNames.StoreBasketList, loginCheckRes.UserId);

            if (baskets == null || baskets.Count == 0)
            {
                return new ServiceResponse<bool>() { Data = false, Status = false, Message = "Sepette ürün bulunamadı." };
            }

            var order = _storeService.SaveOrder(storeCode, baskets, loginCheckRes);

            _cacheProvider.Set<List<StoreBasketDto>>(enmCacheNames.StoreBasketList, loginCheckRes.UserId, new List<StoreBasketDto>());

            #region CachaName Remove Cache
            var cachaNameList = _cacheProvider.Get<List<string>>(enmCacheNames.CachaNameList);

            if (cachaNameList == null)
            {

                var cachaNameItem = cachaNameList.Where(x => x.Contains(enmCacheNames.StoreList.ToString()));

                foreach (var item in cachaNameItem)
                {
                    _cacheProvider.Remove(item);
                }

                var cachaNameDetailItems = cachaNameList.Where(x => x.Contains(enmCacheNames.StoreDetailList.ToString()));

                if (cachaNameDetailItems != null)
                {
                    foreach (var item in cachaNameDetailItems)
                    {
                        _cacheProvider.Remove(item);
                    }
                }

            }



            #endregion CachaName Remove Cache

            return new ServiceResponse<bool>() { Data = order, Status = true };

        }
    }
}
