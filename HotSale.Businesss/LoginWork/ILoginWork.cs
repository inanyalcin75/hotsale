﻿using HotSale.Dto.Helper;
using HotSale.Dto.Operations;
using System;
using System.Collections.Generic;
using System.Text;

namespace HotSale.Businesss.LoginWork
{
    public interface ILoginWork
    {
        ServiceResponse<LoginCheckRes> LoginCheck(string userName, string password);

    }
}
