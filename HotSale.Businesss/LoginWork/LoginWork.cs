﻿using HotSale.Dto.Helper;
using HotSale.Dto.Operations;
using HotSale.Service.LoginService;
using System;
using System.Collections.Generic;
using System.Text;

namespace HotSale.Businesss.LoginWork
{
    public class LoginWork : ILoginWork
    {
        private readonly ILoginService _loginService;

        //========================================================================================================================
        // Constructor
        //========================================================================================================================
        public LoginWork(ILoginService loginService)
        {
            //Injection
            _loginService = loginService;
        }


        public ServiceResponse<LoginCheckRes> LoginCheck(string userName, string password)
        {
            var control = _loginService.LoginCheck(userName, password);
            return new ServiceResponse<LoginCheckRes> { Status = control == null ? false : true, Data = control };
        }
    }
}
