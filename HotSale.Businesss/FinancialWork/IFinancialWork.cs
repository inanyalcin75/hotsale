﻿using HotSale.Dto.Helper;
using HotSale.Dto.Operations.Finance;
using System;
using System.Collections.Generic;
using System.Text;

namespace HotSale.Businesss.FinancialWork
{
    public interface IFinancialWork
    {
        ServiceResponse<List<BillDto>> GetBillList(int companyId, string term, int pageNo, int listCount, DateTime startDate, DateTime finishDate);
        ServiceResponse<WayBillRes> GetWayBillList(int companyId, int firmId, bool isCustomer, string term, int pageNo, int listCount, DateTime startDate, DateTime finishDate);
        ServiceResponse<CurrentAccountRes> GetCurrentAccountList(int firmId, bool isCustomer, int pageNo, int listCount, DateTime startDate, DateTime finishDate);
    }
}
