﻿using HotSale.Cache;
using HotSale.Dto.Helper;
using HotSale.Dto.Operations.Finance;
using HotSale.Service.FinancialService;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace HotSale.Businesss.FinancialWork
{
    public class FinancialWork : IFinancialWork
    {

        private readonly IFinancialService _financialService;
        private readonly ICacheProvider _cacheProvider;

        //========================================================================================================================
        // Constructor
        //========================================================================================================================
        public FinancialWork(IFinancialService financialService, ICacheProvider cacheProvider)
        {
            //Injection
            _financialService = financialService;
            _cacheProvider = cacheProvider;
        }

        public ServiceResponse<List<BillDto>> GetBillList(int companyId, string term, int pageNo, int listCount, DateTime startDate, DateTime finishDate)
        {

            //Bu son tarihi almak için gerekli
            finishDate = finishDate.Add(new TimeSpan(23, 59, 59));

            return _financialService.GetBillList(companyId, term, pageNo, listCount, startDate, finishDate);
        }


        public ServiceResponse<WayBillRes> GetWayBillList(int companyId, int firmId, bool isCustomer, string term, int pageNo, int listCount, DateTime startDate, DateTime finishDate)
        {
            var returnData = new ServiceResponse<WayBillRes>() { Data = new WayBillRes { } };

            //Bu son tarihi almak için gerekli
            finishDate = finishDate.Add(new TimeSpan(23, 59, 59));

            var res = _financialService.GetWayBillList(companyId, firmId, isCustomer, term, pageNo, listCount, startDate, finishDate);
            var data = res.Data;
            var dataDetail = _financialService.GetWayBillDetailList(data.WayBillDtos.Select(x => x.FisNo).ToArray());

            returnData.Data.WayBillDtos = data.WayBillDtos;
            returnData.Data.WayBillDetailsDto = dataDetail;
            returnData.DataTotalCount = res.DataTotalCount;
            returnData.Term = term;
            returnData.Data.StartDate = startDate;
            returnData.Data.FinishDate = finishDate;
            returnData.Data.Total = res.Data.Total;
            returnData.Page = new Infrastructure.Extension.Pager(res.DataTotalCount, pageNo, listCount);

            return returnData;
        }

        public ServiceResponse<CurrentAccountRes> GetCurrentAccountList(int firmId, bool isCustomer, int pageNo, int listCount, DateTime startDate, DateTime finishDate)
        {
            var returnData = new CurrentAccountRes();

            //Bu son tarihi almak için gerekli
            finishDate = finishDate.Add(new TimeSpan(23, 59, 59));

            var data = _financialService.GetCurrentAccountList(firmId, isCustomer, pageNo, listCount, startDate, finishDate);

            returnData.CurrentAccountsDto = data.Data.CurrentAccountsDto;
            returnData.TotalAlckTutari = data.Data.TotalAlckTutari;
            returnData.TotalBorcTutari = data.Data.TotalBorcTutari;
            returnData.StartDate = startDate;
            returnData.FinishDate = finishDate;
            returnData.CurrentAccountsDto.Page = new Infrastructure.Extension.Pager(returnData.CurrentAccountsDto.DataTotalCount, pageNo, listCount);

            return new ServiceResponse<CurrentAccountRes> { Data = returnData };
        }
    }
}
