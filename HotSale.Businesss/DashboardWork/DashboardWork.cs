﻿using HotSale.Cache;
using HotSale.Dto.Helper;
using HotSale.Dto.Operations;
using HotSale.Infrastructure.Extension;
using HotSale.Service.CompanyService;
using HotSale.Service.ReportService;
using System;
using System.Collections.Generic;
//using Microsoft.Extensions.Caching.Memory;

namespace HotSale.Businesss.DashboardWork
{
    public class DashboardWork : IDashboardWork
    {
        private readonly ICompanyService _companyService;
        private readonly IDashboardService _dashboardService;
        private readonly ICacheProvider _cacheProvider;

        //========================================================================================================================
        // Constructor
        //========================================================================================================================
        public DashboardWork(ICompanyService companyService, IDashboardService dashboardService, ICacheProvider cacheProvider)
        {
            //Injection
            _companyService = companyService;
            _dashboardService = dashboardService;
            _cacheProvider = cacheProvider;
        }


        public ServiceResponse<DashboardRes> Get(int? companyId, int? companysectionId, string companyTypeId, string firmCode, DateTime startDate, DateTime finishDate)
        {
            //Bu son tarihi almak için gerekli
            finishDate = finishDate.Add(new TimeSpan(23, 59, 59));

            var CacheName = ((companyId.ToString() ?? "empty") + "-" + (companysectionId.ToString() ?? "empty") + "-" + (companyTypeId ?? "empty") + startDate.ToShortDateString() + "-" + finishDate.ToShortDateString());

            var dashboardRes = _cacheProvider.Get<DashboardRes>(enmCacheNames.CategoryList, CacheName);

            if (dashboardRes == null)
            {
                dashboardRes = new DashboardRes();
                dashboardRes.StartDate = startDate;
                dashboardRes.FinishDate = finishDate;
                dashboardRes.CompanyId = companyId;
                dashboardRes.CompanySectionsId = companysectionId;
                dashboardRes.CompanyTypeId = companyTypeId;

                dashboardRes.CompanyDto = _companyService.GetCompanyList();
                dashboardRes.CompanyDto.Insert(0, new Dto.CompanyDto { Id = null, Name = "Tümü" });

                dashboardRes.CompanySectionsDto = _companyService.GetCompanySectionList();

                if (companysectionId != null)
                    dashboardRes.CompanyTypeDto = _companyService.GetCompanyTypeList(companysectionId.To<int>());

                if (companysectionId == 4 && companyTypeId != null) //Muhasebe ise
                {
                    dashboardRes.AccountReportDto = _dashboardService.GetAccountsReport(companyId, companysectionId, companyTypeId, startDate, finishDate);
                }
                else
                {
                    dashboardRes.ProductGroupsReportDto = _dashboardService.GetProductsReport(companyId, companysectionId, companyTypeId, firmCode, startDate, finishDate);
                    dashboardRes.ProductsSaleReportDto = _dashboardService.GetProductsSaleReportDto(companyId, companysectionId, companyTypeId, firmCode, startDate, finishDate);
                }

                _cacheProvider.Set<DashboardRes>(enmCacheNames.CategoryList, CacheName, dashboardRes);
            }

            return new ServiceResponse<DashboardRes> { Status = true, Data = dashboardRes };
        }
    }
}
