﻿using HotSale.Dto.Helper;
using HotSale.Dto.Operations;
using System;
using System.Collections.Generic;
using System.Text;

namespace HotSale.Businesss.DashboardWork
{
    public interface IDashboardWork
    {
        ServiceResponse<DashboardRes> Get(int? companyId, int? companysectionId, string companyTypeId, string firmCode, DateTime startDate, DateTime finishDate);
    }
}
